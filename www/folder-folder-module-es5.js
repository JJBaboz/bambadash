(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["folder-folder-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/app-adds/app-adds.component.html":
    /*!****************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app-adds/app-adds.component.html ***!
      \****************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppAppAddsAppAddsComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header [translucent]=\"true\">\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n\n    <ion-title size=\"large\">App Data</ion-title>\n \n   \n  \n  </ion-toolbar>\n \n\n</ion-header>\n\n<ion-content>\n\n\n\n\n\n  <ion-list>\n   \n    <ion-item>\n         \n      \n      <ion-button (click)=\"presentNewModal(null)\"   slot=\"end\">\n        Add Add\n      </ion-button>\n\n\n    </ion-item>\n\n  </ion-list>\n\n<ion-list *ngFor=\"let data of products\">\n\n\n    <ion-card >\n\n      <ion-card-header >\n\n        <ion-item lines=\"none\" >\n          <ion-thumbnail slot=\"start\">\n            <img [src]=\"data.image? data.image: 'assets/img/water-drop1.png' \">\n          </ion-thumbnail>\n\n\n\n          <ion-label>\n            <ion-text>\n              Name: {{data.customName}}\n            </ion-text>\n\n\n            <div>\n              <p>\n                Description: {{data.description}}\n              </p>\n            </div>\n           \n\n          </ion-label >\n\n  \n\n        </ion-item>\n      \n      </ion-card-header>\n      \n\n\n      <ion-card-content class=\"ion-no-padding\">\n\n        <ion-item lines=\"none\">\n\n      \n          \n          <ion-button slot=\"end\"  (click)=\"presentEditModal(data)\" >\n            Edit Product\n          </ion-button>\n          </ion-item>\n\n      </ion-card-content>\n\n    </ion-card>\n\n\n\n  </ion-list>\n\n\n  \n\n\n</ion-content>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/charts/charts.component.html":
    /*!************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/charts/charts.component.html ***!
      \************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppChartsChartsComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header [translucent]=\"true\">\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n\n  </ion-toolbar>\n \n\n</ion-header>\n\n<ion-content >\n\n\n  \n<ion-grid>\n  <ion-row>\n\n    <ion-item>\n      <iframe style=\"background: #21313C;border: none;border-radius: 2px;box-shadow: 0 2px 10px 0 rgba(70, 76, 79, .2);\" width=\"640\" height=\"480\" src=\"https://charts.mongodb.com/charts-project-0-osdea/embed/charts?id=56f56524-486d-4a71-ac4b-8e88505b27db&autoRefresh=300&theme=dark\"></iframe>\n    </ion-item>\n\n\n    </ion-row>\n\n\n    <ion-row>\n\n      <ion-item>\n        <iframe style=\"background: #FFFFFF;border: none;border-radius: 2px;box-shadow: 0 2px 10px 0 rgba(70, 76, 79, .2);\" width=\"640\" height=\"480\" src=\"https://charts.mongodb.com/charts-project-0-osdea/embed/charts?id=ad11799e-4a0d-4072-bd8a-5fa26dd7a632&autoRefresh=14400&theme=light\"></iframe>  \n      </ion-item>\n      </ion-row>\n\n\n      <ion-row>\n\n        <ion-item>\n          <iframe style=\"background: #21313C;border: none;border-radius: 2px;box-shadow: 0 2px 10px 0 rgba(70, 76, 79, .2);\" width=\"640\" height=\"480\" src=\"https://charts.mongodb.com/charts-project-0-osdea/embed/charts?id=be886e8a-7e10-48f0-a8f3-912a7732a9f1&autoRefresh=28800&theme=dark\"></iframe>    \n        </ion-item>\n    \n    \n        </ion-row>\n\n\n        <ion-row>\n\n          <ion-item>\n            <iframe style=\"background: #FFFFFF;border: none;border-radius: 2px;box-shadow: 0 2px 10px 0 rgba(70, 76, 79, .2);\" width=\"640\" height=\"480\" src=\"https://charts.mongodb.com/charts-project-0-osdea/embed/charts?id=c24ddee6-7a6f-4096-bd24-921c8a79ef3b&autoRefresh=14400&theme=light\"></iframe>  \n          </ion-item>\n          </ion-row>\n\n        <ion-row>\n\n          <ion-item>\n\n<iframe style=\"background: #21313C;border: none;border-radius: 2px;box-shadow: 0 2px 10px 0 rgba(70, 76, 79, .2);\" width=\"1000\" height=\"480\" src=\"https://charts.mongodb.com/charts-project-0-osdea/embed/charts?id=05708b6f-88f3-4e9f-b12d-3b90555cac65&autoRefresh=1800&theme=dark\"></iframe>         \n         \n          </ion-item>\n      \n          </ion-row>\n\n\n    </ion-grid>\n\n  </ion-content>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/edit-app-data/edit-app-data.component.html":
    /*!**************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/edit-app-data/edit-app-data.component.html ***!
      \**************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppEditAppDataEditAppDataComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n\n\n<ion-content>\n\n\n  <ion-card  >\n    <ion-card-header class=\"ion-text-center\">\n\n      <ion-item>\n\n        <ion-button slot=\"end\" color=\"primary\" (click)=\"dismiss()\" >\n          Close\n          </ion-button>\n\n      </ion-item>\n\n      <ion-progress-bar ion-padding color=\"primary\" [value]=\"per\"></ion-progress-bar>\n\n      <div  style=\"  width:100%; height:100%\"> \n         <img height=50% width= 50%;  [src]=\"myUrl? myUrl:'assets/img/bambaswap.jpg'\"  />\n        \n         \n  <ion-button *ngIf=\"!isFileUploading && !isFileUploaded\" slot=\"start\" expand=\"full\" color=\"success\" >\n    \n    <span>Upload Image</span>\n    \n  \n    <input id=\"uploadBtn\" type=\"file\" class=\"upload\" (change)=\"uploadImage($event.target.files)\"/>\n  </ion-button>\n\n    \n    </div>\n\n    <ion-card-title >\n      Image Name: {{name}}\n      </ion-card-title>\n    \n    \n          <ion-text color=\"success\">\n            <p>Description: {{description}}</p>\n          </ion-text>\n      \n    </ion-card-header>\n\n    <ion-card-content>\n\n    \n      <ion-item >\n        <ion-label position=\"floating\" >Name</ion-label>\n\n        <ion-input [(ngModel)]=\"name\"></ion-input>\n      </ion-item>\n\n      <ion-item >\n        <ion-label position=\"floating\"  >Description</ion-label>\n\n        <ion-input [(ngModel)]=\"description\"></ion-input>\n      </ion-item>\n\n    \n\n\n      \n\n<ion-item lines=\"none\">\n\n  <ion-button *ngIf=\"!isFileUploading && isFileUploaded\" slot=\"start\" expand=\"full\" color=\"danger\"\n   (click)=\"delete()\">Re-upload Image</ion-button>\n\n  <ion-button *ngIf=\"mode === 'edit' \" [disabled]=\" !isFileUploaded || name === '' || description ==='' \" color=\"danger\"\n \n  slot=\"start\" (click)=\"deleteProduct()\" >\n    Delete Product\n  </ion-button>\n\n  <ion-button *ngIf=\"mode === 'edit' \" [disabled]=\" !isFileUploaded || name === '' || description ===''  \" slot = \"end\" expand=\"full\"\n  \n  color=\"success\" (click)=\"updateProduct()\" >Update App-Add</ion-button>\n    \n\n  <ion-button *ngIf=\"mode === 'new' \" [disabled]=\" !isFileUploaded || name === '' || description ==='' \" slot = \"end\" expand=\"full\"\n  \n  color=\"success\" (click)=\"storeImageData()\">Save App-Add</ion-button>\n\n</ion-item>\n\n\n    \n      \n     \n    </ion-card-content>\n  </ion-card>\n\n\n\n\n</ion-content>\n  \n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/finance/finance/finance.component.html":
    /*!**********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/finance/finance/finance.component.html ***!
      \**********************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppFinanceFinanceFinanceComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\r\n<ion-header [translucent]=\"true\">\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-menu-button></ion-menu-button>\r\n    </ion-buttons>\r\n\r\n    \r\n    <ion-searchbar [(ngModel)]=\"searchKey\" animated  (ionInput)=\"onInput($event)\" (ionCancel)=\"onCancel($event)\"></ion-searchbar>\r\n  \r\n\r\n  </ion-toolbar>\r\n \r\n\r\n</ion-header>\r\n\r\n\r\n<ion-content>\r\n\r\n  \r\n\r\n\r\n<ion-grid>\r\n  \r\n\r\n  <ion-list>\r\n \r\n\r\n    <ion-item-divider>\r\n      \r\n  \r\n      <ion-row >\r\n    \r\n     \r\n    \r\n      <ion-col size=\"auto\" class=\"ion-align-self-center\" >\r\n        <ion-button (click)=\"getMpesabalance()\" fill=\"outline\">MPESA Utility Balance: KES {{mpesaBalance3}}</ion-button>\r\n    \r\n      </ion-col>\r\n\r\n      <ion-col size=\"auto\" class=\"ion-align-self-center\" >\r\n        <ion-button (click)=\"getMpesabalance()\" fill=\"outline\">MPESA Working Balance : KES {{mpesaBalance2}}</ion-button>\r\n    \r\n      </ion-col>\r\n    \r\n\r\n      <ion-col size=\"auto\" class=\"ion-align-self-center\" >\r\n        <ion-button (click)=\"africabalance()\" fill=\"outline\">USSD/SMS Balance : KES {{smsBalance}}</ion-button>\r\n    \r\n      </ion-col>\r\n  \r\n    <!--\r\n <ion-col size=\"12\" class=\"ion-align-self-center\" >\r\n    <ion-button (click)=\"getMpesabalance()\" fill=\"outline\">Utility Account : {{mpesaBalance}}</ion-button>\r\n\r\n  </ion-col>\r\n\r\n\r\n-->\r\n     \r\n      </ion-row>\r\n    \r\n    </ion-item-divider>\r\n    \r\n    \r\n\r\n\r\n\r\n<ion-item-divider>\r\n      \r\n    \r\n  <ion-row *ngIf=\" user == 'babzgeo27@gmail.com'? false:true\" >\r\n\r\n  <ion-col size=\"auto\" *ngFor=\"let dev of devices\"  class=\"ion-align-self-start\" >\r\n\r\n    <!---->\r\n\r\n    <ion-button  fill=\"outline\" [color]=\"dev.state === 'online'? 'success':'danger'\">{{dev.name}} ({{dev.phone}}): KES {{dev.balance}} -- {{dev.state === 'online'? 'online':'offline'}} </ion-button>\r\n\r\n  </ion-col>\r\n\r\n  </ion-row>\r\n\r\n  \r\n  <ion-row *ngIf=\" user == 'babzgeo27@gmail.com'? true:false\" >\r\n\r\n    <ion-col size=\"auto\" *ngFor=\"let dev of devices\"  class=\"ion-align-self-start\" >\r\n  \r\n      <!--({{dev.phone}})   (click)=\"getCreditBalance(dev)\"  -->\r\n  \r\n      <ion-button  (click)=\"getCreditBalance(dev)\" fill=\"outline\" [color]=\"dev.state === 'online'? 'success':'danger'\">{{dev.name}} ({{dev.phone}}): KES {{dev.balance}} -- {{dev.state === 'online'? 'online':'offline'}} -- {{dev.usage}}</ion-button>\r\n  \r\n    </ion-col>\r\n    \r\n    </ion-row>\r\n\r\n\r\n</ion-item-divider>\r\n\r\n\r\n<ion-row >\r\n  <ion-col >\r\n\r\n    <ion-button (click)=\"getCreditBalance({name:null})\"  fill=\"outline\" color='success'>Data-Airtime Exchanger  {{dispersable}} : {{totalAirtime}} --ONLINE </ion-button>\r\n  </ion-col>\r\n</ion-row>\r\n\r\n\r\n\r\n<ion-item-divider >\r\n\r\n  <ion-row  >\r\n   \r\n    <ion-col size=\"12\" class=\"ion-align-self-center\" >\r\n\r\n      <ion-card >\r\n\r\n        <ion-card-header>\r\n          <ion-card-subtitle>This will prompt the buyer via mpesa pop-up menu</ion-card-subtitle>\r\n          <ion-card-title>Send Airtime to Distributor</ion-card-title>\r\n        </ion-card-header>\r\n        \r\n      \r\n        <ion-card-content>\r\n          <ion-item>\r\n            <ion-label>My Devices</ion-label>\r\n        \r\n          <ion-select [(ngModel)]=\"myDevice\" placeholder=\"Select One\" okText=\"Okay\" cancelText=\"Dismiss\">\r\n          \r\n              <ion-select-option *ngFor=\"let device of devices\" [value]=\"device.phone\">{{device.name}}--{{device.phone}}</ion-select-option>\r\n        \r\n            </ion-select>\r\n          </ion-item>\r\n\r\n          <ion-item>\r\n            <ion-label position=\"floating\">Buyer number </ion-label>\r\n            <ion-input [(ngModel)]=\"clientPhone\" clearInput placeholder=\"07...\" ></ion-input>\r\n          </ion-item>\r\n\r\n\r\n          <ion-item>\r\n            <ion-label position=\"floating\">Amount </ion-label>\r\n            <ion-input [(ngModel)]=\"Amount\" clearInput placeholder=\"Kes\" ></ion-input>\r\n          </ion-item>\r\n\r\n\r\n        </ion-card-content>\r\n\r\n\r\n        <ion-item *ngIf=\" user == 'babzgeo27@gmail.com'? true:user == 'mutie.michael@gmail.com'? true:user == 'gmngari1@gmail.com'? true:false  \">\r\n          <ion-button [disabled]=\" clientPhone != '' && Amount != null && myDevice != '' ? false:true   \" (click)=\"sendAirtime()\" expand=\"block\" fill=\"outline\" >Send</ion-button>\r\n        </ion-item>\r\n\r\n      </ion-card>\r\n      \r\n    </ion-col>\r\n  \r\n  \r\n    </ion-row>\r\n  </ion-item-divider>\r\n\r\n\r\n\r\n</ion-list>\r\n\r\n\r\n<ion-list *ngIf=\" user == 'babzgeo27@gmail.com'? true:user == 'mutie.michael@gmail.com'? true:user == 'gmngari1@gmail.com'? true:user == 'muli@bambaswap.com'? true:false  \" >\r\n\r\n\r\n  <ion-title size=\"large\">Breakdown</ion-title>\r\n\r\n<ion-item-divider>\r\n<ion-row>\r\n  <ion-col>\r\n  <mat-form-field appearance=\"fill\">\r\n    <mat-label>Enter a date range</mat-label>\r\n    <mat-date-range-input [formGroup]=\"range\" [rangePicker]=\"picker\">\r\n      <input [min]=\"minDate\" [max]=\"maxDate\"   matStartDate formControlName=\"start\" placeholder=\"Start date\">\r\n      <input  [min]=\"minDate\" [max]=\"maxDate\"  matEndDate formControlName=\"end\" placeholder=\"End date\">\r\n    </mat-date-range-input>\r\n    <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\r\n    <mat-date-range-picker #picker></mat-date-range-picker>\r\n  \r\n    <mat-error *ngIf=\"range.controls.start.hasError('matStartDateInvalid')\">Invalid start date</mat-error>\r\n    <mat-error *ngIf=\"range.controls.end.hasError('matEndDateInvalid')\">Invalid end date</mat-error>\r\n  </mat-form-field>\r\n</ion-col>\r\n\r\n  <ion-col>\r\n    <ion-button  (click)=\"calcRequests()\" expand=\"block\" fill=\"outline\" >Search</ion-button>\r\n  </ion-col>\r\n\r\n</ion-row>\r\n</ion-item-divider>\r\n\r\n<ion-text color=\"primary\">\r\n  <h4>Start: {{range.value.start | date:'medium' }}</h4>\r\n</ion-text>\r\n\r\n<ion-text color=\"primary\">\r\n  <h4>End: {{range.value.end | date:'medium' }}</h4>\r\n</ion-text>\r\n\r\n<ion-item>\r\n\r\n</ion-item>\r\n\r\n<ion-item-divider>\r\n\r\n  <ion-title size=\"medium\">Airtime Purchase</ion-title>\r\n  \r\n</ion-item-divider>\r\n\r\n\r\n<ion-row class=\"ion-align-items-center\">\r\n\r\n  <ion-col>\r\n    TYPE\r\n  </ion-col>\r\n  <ion-col>\r\n    CREDIT\r\n  </ion-col>\r\n  <ion-col>\r\n    AMOUNT\r\n  </ion-col>\r\n  <ion-col>\r\n    PROFIT\r\n  </ion-col>\r\n\r\n</ion-row>\r\n\r\n\r\n\r\n<ion-row class=\"ion-align-items-center\">\r\n\r\n  <ion-col>\r\n    Airtime \r\n  </ion-col>\r\n  <ion-col>\r\n    {{airtimeCredit}}\r\n  </ion-col>\r\n  <ion-col>\r\n    {{airtimeCash}}\r\n  </ion-col>\r\n  <ion-col>\r\n    {{airtimeProfit}}\r\n  </ion-col>\r\n\r\n</ion-row>\r\n\r\n\r\n<ion-row class=\"ion-align-items-center\">\r\n\r\n  <ion-col>\r\n    Data \r\n  </ion-col>\r\n  <ion-col>\r\n    {{dataCredit}}\r\n  </ion-col>\r\n  <ion-col>\r\n    {{dataCash}}\r\n  </ion-col>\r\n  <ion-col>\r\n    {{dataProfit}}\r\n  </ion-col>\r\n\r\n</ion-row>\r\n\r\n\r\n\r\n\r\n<ion-row class=\"ion-align-items-center\">\r\n\r\n  <ion-col>\r\n    TOTALS \r\n  </ion-col>\r\n  <ion-col>\r\n    {{dataCredit + airtimeCredit}}\r\n  </ion-col>\r\n  <ion-col>\r\n    {{dataCash + airtimeCash }}\r\n  </ion-col>\r\n  <ion-col>\r\n    {{airtimeProfit + dataProfit}}\r\n  </ion-col>\r\n\r\n</ion-row>\r\n\r\n\r\n\r\n\r\n<ion-row >\r\n\r\n\r\n\r\n\r\n</ion-row>\r\n\r\n<ion-item>\r\n\r\n</ion-item>\r\n\r\n\r\n\r\n<ion-item-divider>\r\n\r\n  <ion-title size=\"medium\">Conversion </ion-title>\r\n  \r\n</ion-item-divider>\r\n\r\n\r\n<ion-row class=\"ion-align-items-center\">\r\n  <ion-col>\r\n    AIRTIME RECEIVED\r\n  </ion-col>\r\n  <ion-col>\r\n    CASH SENT\r\n  </ion-col>\r\n  <ion-col>\r\n    COST\r\n  </ion-col>\r\n</ion-row>\r\n\r\n\r\n<ion-row>\r\n  <ion-col>\r\n    {{conversionAirtime}}\r\n  </ion-col>\r\n\r\n  <ion-col>\r\n    {{conversionCash}}\r\n  </ion-col>\r\n\r\n\r\n  <ion-col>\r\n    {{conversionCost}}\r\n  </ion-col>\r\n\r\n</ion-row>\r\n\r\n<ion-row>\r\n\r\n  <ion-col>\r\n  Mpesa-Transaction cost\r\n  </ion-col>\r\n\r\n\r\n  <ion-col>\r\n     ---\r\n    </ion-col>\r\n\r\n\r\n    <ion-col>\r\n      <ion-col>\r\n        {{conversionCost}}\r\n      </ion-col>\r\n      </ion-col>\r\n\r\n</ion-row>\r\n\r\n\r\n<ion-row>\r\n\r\n  <ion-col>\r\n  USSD- Dialing cost\r\n  </ion-col>\r\n\r\n\r\n  <ion-col>\r\n     ---\r\n    </ion-col>\r\n\r\n\r\n    <ion-col>\r\n      <ion-col>\r\n        {{conversionCost1}}\r\n      </ion-col>\r\n      </ion-col>\r\n\r\n</ion-row>\r\n\r\n\r\n<ion-row>\r\n\r\n  <ion-col>\r\n  TOTAL cost\r\n  </ion-col>\r\n\r\n\r\n  <ion-col>\r\n     ---\r\n    </ion-col>\r\n\r\n\r\n    <ion-col>\r\n      <ion-col>\r\n        {{conversionCost +  conversionCost1}}\r\n      </ion-col>\r\n      </ion-col>\r\n\r\n</ion-row>\r\n\r\n</ion-list>\r\n\r\n</ion-grid>\r\n\r\n</ion-content>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/folder/folder.page.html":
    /*!*******************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/folder/folder.page.html ***!
      \*******************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppFolderFolderPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header [translucent]=\"true\">\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-menu-button></ion-menu-button>\r\n    </ion-buttons>\r\n\r\n    <ion-title size=\"large\">{{ folder }}</ion-title>\r\n \r\n    <ion-searchbar [(ngModel)]=\"searchKey\" animated  (ionInput)=\"onInput($event)\" (ionCancel)=\"onCancel($event)\"></ion-searchbar>\r\n  \r\n \r\n    <ion-row>\r\n\r\n    <ion-col>\r\n      <mat-form-field appearance=\"fill\">\r\n        <mat-label>Enter a date range</mat-label>\r\n        <mat-date-range-input [formGroup]=\"range\" [rangePicker]=\"picker\">\r\n          <input [min]=\"minDate\" [max]=\"maxDate\"   matStartDate formControlName=\"start\" placeholder=\"Start date\">\r\n          <input  [min]=\"minDate\" [max]=\"maxDate\"  matEndDate formControlName=\"end\" placeholder=\"End date\">\r\n        </mat-date-range-input>\r\n        <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\r\n        <mat-date-range-picker #picker></mat-date-range-picker>\r\n      \r\n        <mat-error *ngIf=\"range.controls.start.hasError('matStartDateInvalid')\">Invalid start date</mat-error>\r\n        <mat-error *ngIf=\"range.controls.end.hasError('matEndDateInvalid')\">Invalid end date</mat-error>\r\n      </mat-form-field>\r\n<ion-row>\r\n  <ion-button  (click)=\"getByDate()\" expand=\"block\" fill=\"outline\" >Search</ion-button>\r\n</ion-row>\r\n     \r\n    </ion-col>\r\n    \r\n    \r\n\r\n\r\n     \r\n        <ion-col size=\"4\">\r\n          <ion-item>\r\n            <ion-label>Manual Transactions</ion-label>\r\n        \r\n          <ion-select [(ngModel)]=\"filterManual\" placeholder=\"Show Manual Requests\" okText=\"Okay\" cancelText=\"Dismiss\">\r\n          \r\n              <ion-select-option  [value]= true >Yes</ion-select-option>\r\n              <ion-select-option  [value]= false >No</ion-select-option>\r\n        \r\n            </ion-select>\r\n          </ion-item>\r\n\r\n<ion-item>\r\n\r\n  <ion-button col (click)=\"placeFilters()\" >\r\n    Filter\r\n    </ion-button>\r\n\r\n    <ion-button color= \"danger\" col (click)=\"clearFilters()\">\r\n      Clear\r\n      </ion-button>\r\n</ion-item>\r\n\r\n      </ion-col>\r\n\r\n \r\n     \r\n    </ion-row>\r\n\r\n\r\n  </ion-toolbar>\r\n \r\n\r\n</ion-header>\r\n\r\n<ion-content >\r\n \r\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"getUsersList($event)\" pullMin=\"100\" pullMax=\"200\">\r\n    <ion-refresher-content \r\n    pullingIcon=\"arrow-down-outline\" \r\n    pullingText=\"Pull to refresh\" \r\n    refreshingSpinner=\"crescent\"\r\n    refreshingText=\"Refreshing...\">\r\n  </ion-refresher-content>\r\n  </ion-refresher>\r\n\r\n  <ion-segment color=\"dark\" [(ngModel)]=\"states\"  >\r\n\r\n\r\n    <ion-segment-button value=\"A2M\">\r\n      AIRTIME TO MPESA\r\n    </ion-segment-button>\r\n  \r\n    <ion-segment-button  value=\"M2A\">\r\n      BUY AIRTIME\r\n    </ion-segment-button>\r\n\r\n    <ion-segment-button  value=\"Data\">\r\n     BUY DATA\r\n    </ion-segment-button>\r\n\r\n\r\n\r\n  </ion-segment>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n  <ion-grid  [ngSwitch]=\"states\">\r\n\r\n\r\n    <ion-row  *ngSwitchCase=\"'A2M'\" >\r\n\r\n      <ion-virtual-scroll [items]=\"A2M\">\r\n      \r\n\r\n<ion-item button (click)=\"presentModal(req)\" *virtualItem=\"let req\" >\r\n  <ion-thumbnail slot=\"start\">\r\n    <img src=\"assets/img/cash.png\">\r\n  </ion-thumbnail>\r\n  <ion-label class=\"ion-text-wrap\">\r\n    Phone: {{req.phone}}\r\n    <ion-text color=\"primary\">\r\n      <h3>Date: {{req.date | date:'medium' }}</h3>\r\n    </ion-text>\r\n    <ion-text *ngIf=\"req.transType== 'M2A'\" color=\"secondary\">\r\n      <h3>Type:  MPESA TO AIRTIME</h3>\r\n    </ion-text>\r\n\r\n    <ion-text *ngIf=\"req.transType== 'A2M'\" color=\"success\">\r\n      <h3>Type:  AIRTIME TO MPESA</h3>\r\n    </ion-text>\r\n\r\n    <ion-text *ngIf=\"req.transType== 'Data'\" color=\"success\">\r\n      <h3>Type:   DATA BUNDLES</h3>\r\n    </ion-text>\r\n\r\n    \r\n    <ion-text *ngIf=\"req.transType== 'M2A'\" >\r\n      Airtime to be received: <span style=\"color: green;\">Kes {{req.amount}}</span>\r\n    </ion-text>\r\n\r\n    <ion-text *ngIf=\"req.transType== 'A2M'\" >\r\n      Airtime to Convert: <span style=\"color: green;\"> Kes {{req.amount}}</span>\r\n    </ion-text>\r\n\r\n    <ion-text *ngIf=\"req.transType== 'Data' \" >\r\n      Data type: <span style=\"color: green;\"> {{req.Category}}</span>\r\n    </ion-text>\r\n\r\n    \r\n\r\n\r\n    <ion-text *ngIf=\"req.transType== 'M2A'\" >\r\n    Cash to Pay: <span style=\"color: green\">Kes {{req.mpesaAmount}}</span>\r\n    </ion-text>\r\n\r\n    <ion-text *ngIf=\"req.transType== 'Data'\" >\r\n      Cash to Pay: <span style=\"color: green\">Kes {{req.mpesaAmount}}</span>\r\n      </ion-text>\r\n\r\n    <ion-text *ngIf=\"req.transType== 'A2M'\" >\r\n      Cash to be received by client: <span style=\"color: green\">Kes {{req.mpesaAmount}}</span>\r\n    </ion-text>\r\n\r\n\r\n  <ion-text *ngIf=\"req.state === 'complete'\" color=\"success\" >\r\n    <p> {{req.state}}</p>\r\n  </ion-text>\r\n\r\n  <ion-text *ngIf=\"req.state === 'pendingMpesa' || req.state ==='pendingCredit' || req.state ==='pendingData' || req.state ==='pendingApproval' \" color=\"danger\" >\r\n    <p> {{req.state}}</p>\r\n  </ion-text>\r\n\r\n  <ion-text *ngIf=\"req.state === 'error404' || req.state === 'requestExpired' \" color=\"danger\">\r\n    <p> {{req.state}}</p>\r\n  </ion-text>\r\n  \r\n \r\n  \r\n\r\n  </ion-label>\r\n\r\n  \r\n\r\n\r\n  \r\n\r\n\r\n  \r\n</ion-item>\r\n\r\n\r\n</ion-virtual-scroll>\r\n\r\n\r\n\r\n  </ion-row>\r\n\r\n\r\n\r\n  <ion-row   *ngSwitchCase=\"'M2A'\" >\r\n\r\n    <ion-virtual-scroll [items]=\"M2A\">\r\n    \r\n\r\n<ion-item button (click)=\"presentModal(req)\" *virtualItem=\"let req\" >\r\n<ion-thumbnail slot=\"start\">\r\n  <img src=\"assets/img/cash.png\">\r\n</ion-thumbnail>\r\n<ion-label class=\"ion-text-wrap\">\r\n  Phone: {{req.phone}}\r\n  <ion-text color=\"primary\">\r\n    <h3>Date: {{req.date | date:'medium' }}</h3>\r\n  </ion-text>\r\n  <ion-text *ngIf=\"req.transType== 'M2A'\" color=\"secondary\">\r\n    <h3>Type:  MPESA TO AIRTIME</h3>\r\n  </ion-text>\r\n\r\n  <ion-text *ngIf=\"req.transType== 'A2M'\" color=\"success\">\r\n    <h3>Type:  AIRTIME TO MPESA</h3>\r\n  </ion-text>\r\n\r\n  <ion-text *ngIf=\"req.transType== 'Data'\" color=\"success\">\r\n    <h3>Type:   DATA BUNDLES</h3>\r\n  </ion-text>\r\n\r\n  \r\n  <ion-text *ngIf=\"req.transType== 'M2A'\" >\r\n    Airtime to be received: <span style=\"color: green;\">Kes {{req.amount}}</span>\r\n  </ion-text>\r\n\r\n  <ion-text *ngIf=\"req.transType== 'A2M'\" >\r\n    Airtime to Convert: <span style=\"color: green;\"> Kes {{req.amount}}</span>\r\n  </ion-text>\r\n\r\n  <ion-text *ngIf=\"req.transType== 'Data' \" >\r\n    Data type: <span style=\"color: green;\"> {{req.Category}}</span>\r\n  </ion-text>\r\n\r\n  \r\n\r\n\r\n  <ion-text *ngIf=\"req.transType== 'M2A'\" >\r\n  Cash to Pay: <span style=\"color: green\">Kes {{req.mpesaAmount}}</span>\r\n  </ion-text>\r\n\r\n  <ion-text *ngIf=\"req.transType== 'Data'\" >\r\n    Cash to Pay: <span style=\"color: green\">Kes {{req.mpesaAmount}}</span>\r\n    </ion-text>\r\n\r\n  <ion-text *ngIf=\"req.transType== 'A2M'\" >\r\n    Cash to be received by client: <span style=\"color: green\">Kes {{req.mpesaAmount}}</span>\r\n  </ion-text>\r\n\r\n\r\n<ion-text *ngIf=\"req.state === 'complete'\" color=\"success\" >\r\n  <p> {{req.state}}</p>\r\n</ion-text>\r\n\r\n<ion-text *ngIf=\"req.state === 'pendingMpesa' || req.state ==='pendingCredit' || req.state ==='pendingData' \" color=\"danger\" >\r\n  <p> {{req.state}}</p>\r\n</ion-text>\r\n\r\n<ion-text *ngIf=\"req.state === 'error404' \"  color=\"danger\">\r\n  <p> {{req.state}}</p>\r\n</ion-text>\r\n\r\n</ion-label>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</ion-item>\r\n\r\n\r\n</ion-virtual-scroll>\r\n\r\n\r\n\r\n</ion-row>\r\n\r\n\r\n\r\n\r\n<ion-row  *ngSwitchCase=\"'Data'\" >\r\n\r\n  <ion-virtual-scroll [items]=\"Data\">\r\n  \r\n\r\n<ion-item button (click)=\"presentModal(req)\" *virtualItem=\"let req\" >\r\n<ion-thumbnail slot=\"start\">\r\n<img src=\"assets/img/cash.png\">\r\n</ion-thumbnail>\r\n<ion-label class=\"ion-text-wrap\">\r\nPhone: {{req.phone}}\r\n<ion-text color=\"primary\">\r\n  <h3>Date: {{req.date | date:'medium' }}</h3>\r\n</ion-text>\r\n<ion-text *ngIf=\"req.transType== 'M2A'\" color=\"secondary\">\r\n  <h3>Type:  MPESA TO AIRTIME</h3>\r\n</ion-text>\r\n\r\n<ion-text *ngIf=\"req.transType== 'A2M'\" color=\"success\">\r\n  <h3>Type:  AIRTIME TO MPESA</h3>\r\n</ion-text>\r\n\r\n<ion-text *ngIf=\"req.transType== 'Data'\" color=\"success\">\r\n  <h3>Type:   DATA BUNDLES</h3>\r\n</ion-text>\r\n\r\n\r\n<ion-text *ngIf=\"req.transType== 'M2A'\" >\r\n  Airtime to be received: <span style=\"color: green;\">Kes {{req.amount}}</span>\r\n</ion-text>\r\n\r\n<ion-text *ngIf=\"req.transType== 'A2M'\" >\r\n  Airtime to Convert: <span style=\"color: green;\"> Kes {{req.amount}}</span>\r\n</ion-text>\r\n\r\n<ion-text *ngIf=\"req.transType== 'Data' \" >\r\n  Data type: <span style=\"color: green;\"> {{req.Category}}</span>\r\n</ion-text>\r\n\r\n\r\n\r\n\r\n<ion-text *ngIf=\"req.transType== 'M2A'\" >\r\nCash to Pay: <span style=\"color: green\">Kes {{req.mpesaAmount}}</span>\r\n</ion-text>\r\n\r\n<ion-text *ngIf=\"req.transType== 'Data'\" >\r\n  Cash to Pay: <span style=\"color: green\">Kes {{req.mpesaAmount}}</span>\r\n  </ion-text>\r\n\r\n<ion-text *ngIf=\"req.transType== 'A2M'\" >\r\n  Cash to be received by client: <span style=\"color: green\">Kes {{req.mpesaAmount}}</span>\r\n</ion-text>\r\n\r\n\r\n<ion-text *ngIf=\"req.state === 'complete'\" color=\"success\" >\r\n<p> {{req.state}}</p>\r\n</ion-text>\r\n\r\n<ion-text *ngIf=\"req.state === 'pendingMpesa' || req.state ==='pendingCredit' || req.state ==='pendingData' \" color=\"danger\" >\r\n<p> {{req.state}}</p>\r\n</ion-text>\r\n\r\n<ion-text *ngIf=\"req.state === 'error404' \"  color=\"danger\">\r\n  <p> {{req.state}}</p>\r\n</ion-text>\r\n\r\n</ion-label>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</ion-item>\r\n\r\n\r\n</ion-virtual-scroll>\r\n\r\n\r\n\r\n</ion-row>\r\n\r\n\r\n</ion-grid>\r\n\r\n\r\n</ion-content>\r\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/sms/sms/sms.component.html":
    /*!**********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/sms/sms/sms.component.html ***!
      \**********************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSmsSmsSmsComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header [translucent]=\"true\">\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-menu-button></ion-menu-button>\r\n    </ion-buttons>\r\n \r\n    <ion-searchbar [(ngModel)]=\"searchKey\" (ionCancel)=\"onCancel()\"></ion-searchbar>\r\n  \r\n\r\n  </ion-toolbar>\r\n \r\n\r\n</ion-header>\r\n\r\n\r\n<ion-content>\r\n\r\n  <ion-list>\r\n <ion-list-header>\r\n   \r\n  </ion-list-header>\r\n\r\n\r\n  <ion-item>\r\n    <ion-label>My Devices</ion-label>\r\n\r\n  <ion-select [(ngModel)]=\"device\" placeholder=\"Select One\" okText=\"Okay\" cancelText=\"Dismiss\">\r\n  \r\n      <ion-select-option *ngFor=\"let device of myDevices\" [value]=\"device.phone\">{{device.name}}--{{device.phone}}</ion-select-option>\r\n\r\n    </ion-select>\r\n  </ion-item>\r\n\r\n</ion-list>\r\n\r\n<!--\r\n<h3 *ngIf=\"sms.length === 0? true:false \">Server responce: {{title}}</h3>\r\n\r\n  <h3 *ngIf=\"sms.length === 0? true:false \"> No sms to display</h3>\r\n-->\r\n\r\n<ion-grid>\r\n  <ion-row>\r\n  <ion-col  class=\"ion-align-self-center\" size=\"12\" *ngFor=\"let sms of sms\" >\r\n    <ion-card  >\r\n\r\n      <ion-card-header>\r\n        <ion-avatar item-start>\r\n          <img src=\"assets/img/cash.png\">\r\n        </ion-avatar>\r\n        <ion-card-title>\r\n          Sambaza Sms Sent by Client\r\n        </ion-card-title>\r\n      </ion-card-header>\r\n  \r\n  \r\n    <ion-card-content>\r\n      <ion-list>\r\n        <ion-item>\r\n        \r\n          <ion-label>\r\n            Phone number: \r\n          </ion-label>\r\n          <h3>{{searchKey}}</h3>\r\n        </ion-item>\r\n\r\n        <ion-item >\r\n          <ion-label>\r\n            Date: \r\n          </ion-label>\r\n  \r\n          <p>{{sms.date | date:'medium'}}</p>\r\n        </ion-item>\r\n            \r\n              <ion-item >\r\n                <ion-label>\r\n                  Message: \r\n                </ion-label>\r\n        \r\n                <p>{{sms.body}}</p>\r\n              </ion-item>\r\n        \r\n        \r\n              <ion-item >\r\n                <ion-label>\r\n                  From: \r\n                </ion-label>\r\n               {{sms.address}}\r\n              </ion-item>\r\n        </ion-list>\r\n    </ion-card-content>\r\n\r\n  \r\n\r\n  </ion-card>\r\n</ion-col>\r\n</ion-row>\r\n</ion-grid>\r\n\r\n\r\n\r\n  <ion-button  (click)=\"sendReq()\" color=\"success\">Send request for Sms</ion-button>\r\n</ion-content>\r\n\r\n\r\n\r\n";
      /***/
    },

    /***/
    "./src/app/app-adds/app-adds.component.scss":
    /*!**************************************************!*\
      !*** ./src/app/app-adds/app-adds.component.scss ***!
      \**************************************************/

    /*! exports provided: default */

    /***/
    function srcAppAppAddsAppAddsComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC1hZGRzL2FwcC1hZGRzLmNvbXBvbmVudC5zY3NzIn0= */";
      /***/
    },

    /***/
    "./src/app/app-adds/app-adds.component.ts":
    /*!************************************************!*\
      !*** ./src/app/app-adds/app-adds.component.ts ***!
      \************************************************/

    /*! exports provided: AppAddsComponent */

    /***/
    function srcAppAppAddsAppAddsComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppAddsComponent", function () {
        return AppAddsComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _edit_app_data_edit_app_data_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../edit-app-data/edit-app-data.component */
      "./src/app/edit-app-data/edit-app-data.component.ts");
      /* harmony import */


      var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/fire/firestore */
      "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");

      var AppAddsComponent = /*#__PURE__*/function () {
        function AppAddsComponent(modalController, afFirestore) {
          _classCallCheck(this, AppAddsComponent);

          this.modalController = modalController;
          this.afFirestore = afFirestore;
          this.products = [];
        }

        _createClass(AppAddsComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.fetchData();
          }
        }, {
          key: "presentNewModal",
          value: function presentNewModal(data) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var modal;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      console.log(data);
                      _context.next = 3;
                      return this.modalController.create({
                        component: _edit_app_data_edit_app_data_component__WEBPACK_IMPORTED_MODULE_3__["EditAppDataComponent"],
                        cssClass: 'my-custom-class',
                        showBackdrop: true,
                        swipeToClose: true,
                        animated: true,
                        componentProps: {
                          req: data,
                          mode: "new"
                        }
                      });

                    case 3:
                      modal = _context.sent;
                      modal.onWillDismiss().then(function (data) {
                        console.log("about to dismiss modal"); // this.getProducts()
                      });
                      _context.next = 7;
                      return modal.present();

                    case 7:
                      return _context.abrupt("return", _context.sent);

                    case 8:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "presentEditModal",
          value: function presentEditModal(data) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var modal;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      console.log(data);
                      _context2.next = 3;
                      return this.modalController.create({
                        component: _edit_app_data_edit_app_data_component__WEBPACK_IMPORTED_MODULE_3__["EditAppDataComponent"],
                        cssClass: 'my-custom-class',
                        showBackdrop: true,
                        swipeToClose: true,
                        animated: true,
                        componentProps: {
                          req: data,
                          mode: "edit"
                        }
                      });

                    case 3:
                      modal = _context2.sent;
                      modal.onWillDismiss().then(function (data) {
                        console.log("about to dismiss modal"); // this.getProducts()
                      });
                      _context2.next = 7;
                      return modal.present();

                    case 7:
                      return _context2.abrupt("return", _context2.sent);

                    case 8:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "dismiss",
          value: function dismiss() {
            // using the injected ModalController this page
            // can "dismiss" itself and optionally pass back data
            this.modalController.dismiss({
              'dismissed': true
            });
          }
        }, {
          key: "getDataFromFirestore",
          value: function getDataFromFirestore() {
            return this.afFirestore.collection('appImages').snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (actions) {
              return actions.map(function (action) {
                var data = action.payload.doc.data();
                var id = action.payload.doc.id;
                return Object.assign({
                  id: id
                }, data);
              });
            }));
          }
        }, {
          key: "fetchData",
          value: function fetchData() {
            var _this = this;

            this.getDataFromFirestore().subscribe(function (data) {
              _this.products = data;
              console.log(data);
            }, function (error) {
              console.error('Error fetching data:', error);
            });
          }
        }]);

        return AppAddsComponent;
      }();

      AppAddsComponent.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
        }, {
          type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"]
        }];
      };

      AppAddsComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-app-adds',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./app-adds.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/app-adds/app-adds.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./app-adds.component.scss */
        "./src/app/app-adds/app-adds.component.scss"))["default"]]
      })], AppAddsComponent);
      /***/
    },

    /***/
    "./src/app/auth/auth.service.ts":
    /*!**************************************!*\
      !*** ./src/app/auth/auth.service.ts ***!
      \**************************************/

    /*! exports provided: AuthService */

    /***/
    function srcAppAuthAuthServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthService", function () {
        return AuthService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _auth0_angular_jwt__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @auth0/angular-jwt */
      "./node_modules/@auth0/angular-jwt/__ivy_ngcc__/fesm2015/auth0-angular-jwt.js");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/storage */
      "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");

      var helper = new _auth0_angular_jwt__WEBPACK_IMPORTED_MODULE_2__["JwtHelperService"]();

      var AuthService = /*#__PURE__*/function () {
        function AuthService(storage) {
          _classCallCheck(this, AuthService);

          this.storage = storage;
        }

        _createClass(AuthService, [{
          key: "getToken",
          value: function getToken() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      _context3.next = 2;
                      return this.storage.get('token');

                    case 2:
                      return _context3.abrupt("return", _context3.sent);

                    case 3:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }, {
          key: "isAuthenticated",
          value: function isAuthenticated(token) {
            // get the token
            return helper.isTokenExpired(token); // return a boolean reflecting 
            // whether or not the token is expired
          }
        }]);

        return AuthService;
      }();

      AuthService.ctorParameters = function () {
        return [{
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"]
        }];
      };

      AuthService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], AuthService);
      /***/
    },

    /***/
    "./src/app/charts/charts.component.scss":
    /*!**********************************************!*\
      !*** ./src/app/charts/charts.component.scss ***!
      \**********************************************/

    /*! exports provided: default */

    /***/
    function srcAppChartsChartsComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NoYXJ0cy9jaGFydHMuY29tcG9uZW50LnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/charts/charts.component.ts":
    /*!********************************************!*\
      !*** ./src/app/charts/charts.component.ts ***!
      \********************************************/

    /*! exports provided: ChartsComponent */

    /***/
    function srcAppChartsChartsComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ChartsComponent", function () {
        return ChartsComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var ChartsComponent = /*#__PURE__*/function () {
        function ChartsComponent() {
          _classCallCheck(this, ChartsComponent);
        }

        _createClass(ChartsComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return ChartsComponent;
      }();

      ChartsComponent.ctorParameters = function () {
        return [];
      };

      ChartsComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-charts',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./charts.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/charts/charts.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./charts.component.scss */
        "./src/app/charts/charts.component.scss"))["default"]]
      })], ChartsComponent);
      /***/
    },

    /***/
    "./src/app/edit-app-data/edit-app-data.component.scss":
    /*!************************************************************!*\
      !*** ./src/app/edit-app-data/edit-app-data.component.scss ***!
      \************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppEditAppDataEditAppDataComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2VkaXQtYXBwLWRhdGEvZWRpdC1hcHAtZGF0YS5jb21wb25lbnQuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/edit-app-data/edit-app-data.component.ts":
    /*!**********************************************************!*\
      !*** ./src/app/edit-app-data/edit-app-data.component.ts ***!
      \**********************************************************/

    /*! exports provided: EditAppDataComponent */

    /***/
    function srcAppEditAppDataEditAppDataComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EditAppDataComponent", function () {
        return EditAppDataComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/fire/storage */
      "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-storage.js");
      /* harmony import */


      var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/fire/firestore */
      "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var EditAppDataComponent = /*#__PURE__*/function () {
        function EditAppDataComponent(modalController, afs, afStorage) {
          _classCallCheck(this, EditAppDataComponent);

          this.modalController = modalController;
          this.afs = afs;
          this.afStorage = afStorage; // File uploading status

          this.isFileUploading = false;
          this.isFileUploaded = false;
          this.myUrl = "";
          this.fileStoragePath = "";
          this.per = 0;
          this.name = "";
          this.price = 0;
          this.description = "";
        }

        _createClass(EditAppDataComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            if (this.mode === "edit") {
              this.name = this.req.customName;
              this.description = this.req.description;
              this.myUrl = this.req.image;
              this.isFileUploading = false;
              this.isFileUploaded = true;
            } else if (this.mode === "new") {
              this.isFileUploading = false;
              this.isFileUploaded = false;
            } else {
              console.log("undefined action request");
            }
          }
        }, {
          key: "uploadImage",
          value: function uploadImage(event) {
            var _this2 = this;

            var file = event.item(0);
            console.log(file.name); // Image validation

            if (file.type.split('/')[0] !== 'image') {
              console.log('File type is not supported!');
              return;
            }

            this.isFileUploading = true;
            this.isFileUploaded = false;
            this.imgName = file.name;
            console.log(file.name);
            var storageRef = this.afStorage.ref("images");
            var fileExt = file.name.split('.').pop();
            var imageName = "".concat(file.name, ".").concat(fileExt); // Replace with your custom name or generate dynamically

            var imageRef = storageRef.child(imageName); // Upload the image file

            var uploadTask = imageRef.put(file);
            this.percentageVal = uploadTask.percentageChanges();
            this.percentageVal.subscribe(function (resp) {
              _this2.per = Math.floor(resp) / 100;
              console.log("PERCENTAGE..", _this2.per);

              if (resp === 100) {
                imageRef.getDownloadURL().subscribe(function (resp) {
                  console.log("FILE URL IS...", resp);
                  _this2.myUrl = resp;
                  _this2.isFileUploading = false;
                  _this2.isFileUploaded = true;
                });
              } else {}
            }); // Get the download URL of the uploaded image
            // Storage path
            // this.fileStoragePath = `waterApp/${new Date().getTime()}_${file.name}`;

            /*
            
            // Image reference
              const imageRef = this.afStorage.ref(this.fileStoragePath);
                      
                      // File upload task
              this.fileUploadTask = this.afStorage.upload(this.fileStoragePath, file);
                  
             // imageRef.put(file);
                  
              // Show uploading progress
              this.percentageVal =  this.fileUploadTask.percentageChanges()
                      
              this.UploadedImageURL = imageRef.getDownloadURL();
                     
              
              this.percentageVal.subscribe(resp=>{
                  
             
                        this.per = Math.floor(resp) /100
                        console.log( "PERCENTAGE..", this.per);
                        if (resp === 100) {
                  
                  this.UploadedImageURL.subscribe(resp=>{
                  
                    console.log( "FILE URL IS...", resp);
                            this.myUrl = resp
                            this.isFileUploading = false
                    this.isFileUploaded = true
                        })
                  
                } else {
                  
                }
                    })
            */
          }
        }, {
          key: "uploadImageToStorage",
          value: function uploadImageToStorage(imageFile) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var storageRef, fileExt, imageName, imageRef, imageUrl;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      storageRef = this.afStorage.ref('new_app_adds');
                      fileExt = imageFile.name.split('.').pop(); // Get the original file extension

                      imageName = "".concat(this.name, ".").concat(fileExt); // Generate a unique name with the original file extension

                      imageRef = storageRef.child(imageName);
                      _context4.next = 6;
                      return imageRef.put(imageFile);

                    case 6:
                      _context4.next = 8;
                      return imageRef.getDownloadURL().toPromise();

                    case 8:
                      imageUrl = _context4.sent;
                      return _context4.abrupt("return", imageUrl);

                    case 10:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }, {
          key: "storeImageData",
          value: function storeImageData() {
            var _this3 = this;

            var collectionRef = this.afs.collection('appImages');
            collectionRef.add({
              image: this.myUrl,
              customName: this.name,
              description: this.description
            }).then(function (data) {
              console.log(data);

              _this3.dismiss();
            })["catch"](function (error) {
              console.log(error);
            });
          }
        }, {
          key: "handleImageUpload",
          value: function handleImageUpload(imageFile, customName) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
              var imageUrl;
              return regeneratorRuntime.wrap(function _callee5$(_context5) {
                while (1) {
                  switch (_context5.prev = _context5.next) {
                    case 0:
                      _context5.prev = 0;
                      _context5.next = 3;
                      return this.uploadImageToStorage(imageFile);

                    case 3:
                      imageUrl = _context5.sent;
                      //    await this.storeImageData(imageUrl, customName);
                      console.log('Image uploaded and data stored successfully.');
                      _context5.next = 10;
                      break;

                    case 7:
                      _context5.prev = 7;
                      _context5.t0 = _context5["catch"](0);
                      console.error('Error uploading image and storing data:', _context5.t0);

                    case 10:
                    case "end":
                      return _context5.stop();
                  }
                }
              }, _callee5, this, [[0, 7]]);
            }));
          }
        }, {
          key: "delete",
          value: function _delete() {
            var _this4 = this;

            var originalImageRef = this.afStorage.refFromURL(this.myUrl);
            console.log(originalImageRef);
            originalImageRef["delete"]().subscribe(function (data) {
              console.log(data);
              _this4.isFileUploading = _this4.isFileUploaded = false;
              _this4.myUrl = "";
            }, function (error) {
              console.log(error);
            });
          }
        }, {
          key: "deleteProduct",
          value: function deleteProduct() {
            var _this5 = this;

            var originalImageRef = this.afStorage.refFromURL(this.myUrl);
            var docRef = this.afs.collection('appImages').doc(this.req.id);
            docRef["delete"]().then(function (d) {
              console.log("DELETED PRODUCT FROM DATABASE..", d);
              _this5.isFileUploading = _this5.isFileUploaded = false;
              _this5.myUrl = "";
              _this5.name = "";
              _this5.description = "";
              _this5.mode = "new";
            })["catch"](function (e) {
              console.log("failed to delete", e);
            });
            console.log(this.req.id);

            if (originalImageRef) {
              originalImageRef["delete"]().subscribe(function (data) {
                console.log(data);
                _this5.isFileUploading = _this5.isFileUploaded = false;
                _this5.myUrl = "";
              }, function (error) {
                console.log(error);
              });
            }
          }
        }, {
          key: "updateProduct",
          value: function updateProduct() {
            var _this6 = this;

            var docRef = this.afs.collection('appImages').doc(this.req.id);
            var newDoc = {
              customName: this.name,
              description: this.description,
              image: this.myUrl
            };
            docRef.update(newDoc).then(function (d) {
              console.log("DELETED PRODUCT FROM DATABASE..", d);
              _this6.isFileUploading = false;
              _this6.isFileUploaded = true;
            })["catch"](function (e) {
              console.log("failed to delete", e);
            });
            /*  this.requestUssd.updateProducts(
                {query:{_id:this.req._id},item:{name:this.name,description:this.description,price:this.price,
                  image:this.myUrl,fileStoragePath:this.fileStoragePath}}).subscribe(data=>{
                  console.log(data);
                  if (data.state === 'ok') {
            
            this.dismiss()
            } else {
            this.dismiss()
            }
                          }, error=>{
                            console.log(error);
                    this.dismiss()
                    
                  })
            */
          }
        }, {
          key: "dismiss",
          value: function dismiss() {
            // using the injected ModalController this page
            // can "dismiss" itself and optionally pass back data
            this.modalController.dismiss({
              'dismissed': true
            });
          }
        }]);

        return EditAppDataComponent;
      }();

      EditAppDataComponent.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]
        }, {
          type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"]
        }, {
          type: _angular_fire_storage__WEBPACK_IMPORTED_MODULE_2__["AngularFireStorage"]
        }];
      };

      EditAppDataComponent.propDecorators = {
        req: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        mode: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }]
      };
      EditAppDataComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-edit-app-data',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./edit-app-data.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/edit-app-data/edit-app-data.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./edit-app-data.component.scss */
        "./src/app/edit-app-data/edit-app-data.component.scss"))["default"]]
      })], EditAppDataComponent);
      /***/
    },

    /***/
    "./src/app/finance/finance/finance.component.scss":
    /*!********************************************************!*\
      !*** ./src/app/finance/finance/finance.component.scss ***!
      \********************************************************/

    /*! exports provided: default */

    /***/
    function srcAppFinanceFinanceFinanceComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZpbmFuY2UvZmluYW5jZS9maW5hbmNlLmNvbXBvbmVudC5zY3NzIn0= */";
      /***/
    },

    /***/
    "./src/app/finance/finance/finance.component.ts":
    /*!******************************************************!*\
      !*** ./src/app/finance/finance/finance.component.ts ***!
      \******************************************************/

    /*! exports provided: FinanceComponent */

    /***/
    function srcAppFinanceFinanceFinanceComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FinanceComponent", function () {
        return FinanceComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _get_requests_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../../get-requests.service */
      "./src/app/get-requests.service.ts");
      /* harmony import */


      var ngx_socket_io__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ngx-socket-io */
      "./node_modules/ngx-socket-io/__ivy_ngcc__/fesm2015/ngx-socket-io.js");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/storage */
      "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var FinanceComponent = /*#__PURE__*/function () {
        function FinanceComponent(storage, router, activatedRoute, requestUssd, socket, ref, alertController) {
          _classCallCheck(this, FinanceComponent);

          this.storage = storage;
          this.router = router;
          this.activatedRoute = activatedRoute;
          this.requestUssd = requestUssd;
          this.socket = socket;
          this.ref = ref;
          this.alertController = alertController;
          this.range = new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormGroup"]({
            start: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"](),
            end: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"]()
          });
          this.searchKey = "";
          this.mpesaBalance2 = ""; //working

          this.mpesaBalance3 = ""; //general

          this.devices = [];
          this.devices3 = [];
          this.myDevice = "";
          this.clientPhone = "";
          this.Amount = null;
          this.requests3 = [];
          this.requests = [];
          this.user = "";
          this.dataReq = [];
          this.airtimeReq = [];
          this.conversionReq = [];
          this.smsBalance = 0;
          this.airtimeCredit = 0;
          this.dataCredit = 0;
          this.airtimeCash = 0;
          this.dataCash = 0;
          this.dataProfit = 0;
          this.airtimeProfit = 0;
          this.totalProfit = 0;
          this.conversionAirtime = 0;
          this.conversionCash = 0;
          this.conversionCost = 0;
          this.conversionCost1 = 0;
          this.totalAirtime = 0;
          this.dispersable = 0;
          this.minDate = new Date(21, 9, 10);
        }

        _createClass(FinanceComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this7 = this;

            this.getMpesabalance();
            this.getCreditBalance({
              name: null
            });
            this.africabalance();
            this.storage.get('profile').then(function (data) {
              if (!data) {} else {
                _this7.user = data.profile.email;
                console.log(data.profile.email);
              }
            });
            this.storage.get('authenticated').then(function (data) {
              if (!data) {
                _this7.router.navigate(['/login']);
              }
            }); //received mpesa balance

            this.socket.on("b2cBalance", function (data) {
              console.log("balance from mpesa balance api...", data); //{"Key":"AccountBalance","Value":"Working Account|KES|0.00|0.00|0.00|0.00&Utility Account|KES|30544.18|30544.18|0.00|0.00&Charges Paid Account|KES|0.00|0.00|0.00|0.00"}

              if (data.Result.ResultCode === 0) {
                /// this.mpesaBalance = 
                var bal = data.Result.ResultParameters.ResultParameter[1].Value;
                console.log(data.Result.ResultCode); //"Working Account|KES|0.00|0.00|0.00|0.00&Utility Account|KES|5447.91|5447.91|0.00|0.00&Charges Paid Account|KES|0.00|0.00|0.00|0.00"

                _this7.mpesaBalance2 = bal.split(" ")[1].split("|")[2]; //working 

                _this7.mpesaBalance3 = bal.split(" ")[2].split("|")[2]; //utillity

                console.log(_this7.mpesaBalance3);

                _this7.ref.markForCheck();
              } else {}
            }); //USSD BALANCE 

            this.socket.on("ussdBalance", function (data) {
              _this7.smsBalance = data;

              _this7.ref.markForCheck();

              console.log("sms balance....", data);
              console.log("balance from mpesa balance api...", data);
            });
            /*
            this.socket.on("dashBoardCredit",data=>{
            
              console.log("balance from mpesa balance api...", data);
              
              
              
              
              //{"Key":"AccountBalance","Value":"Working Account|KES|0.00|0.00|0.00|0.00&Utility Account|KES|30544.18|30544.18|0.00|0.00&Charges Paid Account|KES|0.00|0.00|0.00|0.00"}
               
              this.devices = data
              this.ref.markForCheck();
              console.log("credit balance....", data);
              
              })
            
            */
          }
        }, {
          key: "sendAirtime",
          value: function sendAirtime() {
            var _this8 = this;

            //this.socket.emit("sendtoDist",{phone:this.clientPhone, amount:this.Amount, device:this.myDevice})

            /*
             {sessionId:"Manual Dash",  text:[""],
               phone:this.clientPhone,  amount:this.Amount,  otherTransNum:null,  scratchCard:null,
               mpesaAmount:null, deviceId:devices[0]  }
             */
            var req = {
              phone: this.clientPhone,
              amount: this.Amount,
              device: this.myDevice,
              initializer: this.user
            }; //&& user == 'babzgeo27@gmail.com'? false:true

            this.requestUssd.sendDist(req).subscribe(function (data) {
              if (data.msg === "invalid") {
                console.log("token expired");

                _this8.router.navigate(['/login']);

                _this8.storage.clear();
              } else {
                _this8.presentAlert("Sent to server, check in Sms menu if the client received credit.");

                console.log("credit results....", data);
              }
            });
          }
        }, {
          key: "presentAlert",
          value: function presentAlert(message) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
              var alert, _yield$alert$onDidDis, role;

              return regeneratorRuntime.wrap(function _callee6$(_context6) {
                while (1) {
                  switch (_context6.prev = _context6.next) {
                    case 0:
                      _context6.next = 2;
                      return this.alertController.create({
                        cssClass: 'my-custom-class',
                        header: 'Responce',
                        subHeader: '',
                        message: message,
                        buttons: ['OK']
                      });

                    case 2:
                      alert = _context6.sent;
                      _context6.next = 5;
                      return alert.present();

                    case 5:
                      _context6.next = 7;
                      return alert.onDidDismiss();

                    case 7:
                      _yield$alert$onDidDis = _context6.sent;
                      role = _yield$alert$onDidDis.role;
                      console.log('onDidDismiss resolved with role', role);

                    case 10:
                    case "end":
                      return _context6.stop();
                  }
                }
              }, _callee6, this);
            }));
          }
        }, {
          key: "getMpesabalance",
          value: function getMpesabalance() {
            this.requestUssd.mpesaBalance3("A2M").subscribe(function (data) {
              console.log("mpesa balance....", data); //  console.log("mpesa balance 2....", data.data.MpesaDetails[3].Value); //workin accc
              // console.log("mpesa balance....", data.data.MpesaDetails[2].Value); //utility acc

              if (data.result === "success") {//this.mpesaBalance = data.data.MpesaDetails[2].Value
                //  this.mpesaBalance2 = data.data.MpesaDetails[3].Value
                //  this.ref.markForCheck();
              } else {}
            });
            this.requestUssd.mpesaBalance3("A2M").subscribe(function (data) {
              console.log("mpesa balance btob balance api....", data);
            });
          }
        }, {
          key: "getCreditBalance",
          value: function getCreditBalance(device) {
            var _this9 = this;

            this.requestUssd.findDevices(device).subscribe(function (data) {
              _this9.totalAirtime = 0;
              _this9.dispersable = 0;
              _this9.devices = data.data;
              _this9.devices3 = _this9.devices;
              /*  this.devices3 = this.devices.filter((value: any, index: number, array: any[]) =>{
                              console.log(value.state);
                  
                             return value.state === "online"
                            })*/

              console.log("devvices online ....", _this9.devices3);

              _this9.devices.forEach(function (element) {// this.totalAirtime = this.totalAirtime + element.balance
                // this.dispersable = this.dispersable + element.usage
              });

              var usageDevice = _this9.devices.filter(function (v) {
                return v.state === "online";
              });

              usageDevice.forEach(function (element) {
                _this9.totalAirtime = _this9.totalAirtime + element.balance;
                _this9.dispersable = _this9.dispersable + element.usage;
              });

              _this9.ref.markForCheck();

              console.log("credit balance....", _this9.totalAirtime, _this9.dispersable);

              if (data.msg === "invalid") {
                console.log("token expired");

                _this9.router.navigate(['/login']);

                _this9.storage.clear();
              } else {}
            });
          }
        }, {
          key: "africabalance",
          value: function africabalance() {
            var _this10 = this;

            this.requestUssd.getUssdBal().subscribe(function (data) {
              _this10.smsBalance = data;

              _this10.ref.markForCheck();

              console.log("sms balance....", data);
            });
          }
        }, {
          key: "findAll",
          value: function findAll() {
            var _this11 = this;

            this.requestUssd.findAll().subscribe(function (data) {
              _this11.requests3 = data.data;
              _this11.requests = data.data;

              _this11.ref.markForCheck();

              console.log(data);
            });
          }
        }, {
          key: "calcRequests",
          value: function calcRequests() {
            var _this12 = this;

            console.log(this.range.value);
            this.requests = [];
            this.requests3 = [];
            this.airtimeCredit = 0;
            this.dataCredit = 0;
            this.airtimeCash = 0;
            this.dataCash = 0;
            this.dataProfit = 0;
            this.airtimeProfit = 0;
            this.totalProfit = 0;
            this.conversionAirtime = 0;
            this.conversionCash = 0;
            this.conversionCost = 0;
            var date = this.range.value;

            if (date.start === null) {
              console.log("please set date");
            } else if (date.end === null) {
              var startDate = new Date(date.start);
              var endDate = new Date(); //date.end

              console.log("start", startDate, "End", endDate);
              /*  var resultProductData = this.requests.filter(function (a) {
                    var hitDates = a.ProductHits || {};
                    // extract all date strings
                    hitDates = Object.keys(hitDates);
                    // improvement: use some. this is an improment because .map()
                    // and .filter() are walking through all elements.
                    // .some() stops this process if one item is found that returns true in the callback function and returns true for the whole expression
                   let hitDateMatchExists = hitDates.some(function(dateStr) {
                        var date = new Date(dateStr);
                        return date >= startDate && date <= endDate
                    });
                
                    return hitDateMatchExists;
                });
                        console.log(resultProductData); */

              var query = {
                start: startDate,
                end: endDate
              };
              this.requestUssd.getByDate(query).subscribe(function (data) {
                //  this.ref.markForCheck();
                console.log("REQUESTED DATA WHEN END IS NULL IS...", data);
                _this12.requests3 = data.data;
                _this12.requests = data.data; //  state:"complete"

                _this12.dataReq = _this12.requests3.filter(function (data) {
                  return data.transType === "Data" && data.state === "complete";
                });
                _this12.airtimeReq = _this12.requests3.filter(function (data) {
                  return data.transType === "M2A" && data.state === "complete";
                });
                _this12.conversionReq = _this12.requests3.filter(function (data) {
                  return data.transType === "A2M" && data.state === "complete";
                });
                console.log("DATA.....", _this12.dataReq);
                console.log("AIRTIME....", _this12.airtimeReq);
                console.log("CONVERSION....", _this12.conversionReq);

                _this12.dataReq.forEach(function (data) {
                  _this12.dataCredit = _this12.dataCredit + data.amount;
                  _this12.dataCash = _this12.dataCash + data.mpesaAmount;
                  _this12.dataProfit = Math.floor(_this12.dataCredit * 0.1);
                });

                _this12.airtimeReq.forEach(function (data) {
                  _this12.airtimeCredit = _this12.airtimeCredit + data.airtimeAmount;
                  _this12.airtimeCash = _this12.airtimeCash + data.mpesaAmount;
                  _this12.airtimeProfit = Math.floor(_this12.airtimeCredit * 0.1);
                  _this12.totalProfit = _this12.airtimeProfit + _this12.dataProfit;
                });

                _this12.conversionReq.forEach(function (data) {
                  _this12.conversionAirtime = _this12.conversionAirtime + data.amount;
                  _this12.conversionCash = _this12.conversionCash + data.mpesaAmount;

                  if (data.mpesaAmount > 1000) {
                    _this12.conversionCost = _this12.conversionCost + 8;
                  } else if (data.mpesaAmount < 1000 && data.mpesaAmount > 100) {
                    _this12.conversionCost = _this12.conversionCost + 4;
                  } else {// this.conversionCost = this.conversionCost  
                  }
                });

                var conv1 = _this12.requests3.filter(function (data) {
                  return data.transType === "A2M";
                });
                /*    conv1.forEach(data=>{
                  this.conversionCost = this.conversionCost + 1.5
                                  })*/


                console.log("data transaction..", _this12.dataCash, _this12.dataCredit, "profit...", _this12.dataProfit);
                console.log("airtime transaction..", _this12.airtimeCash, _this12.airtimeCredit, "profit...", _this12.airtimeProfit);
                console.log("PROFIT....", _this12.totalProfit);
                console.log("CONVERSION MPESA TOTAL", _this12.conversionCash);
                console.log("CONVERSION AIRTIME TOTAL", _this12.conversionAirtime);
                console.log("CONVERSION COST..", _this12.conversionCost);

                _this12.ref.markForCheck();
              });
            } else {
              /*
               let startDate = new Date(date.start).valueOf();
                      let endDate = new Date(date.end).valueOf();
              */
              var _startDate = new Date(date.start);

              var _endDate = new Date(date.end);

              console.log("start", _startDate, "End", _endDate);
              var _query = {
                start: _startDate,
                end: _endDate
              };
              this.requestUssd.getByDate(_query).subscribe(function (data) {
                //  this.ref.markForCheck();
                console.log("REQUESTED DATA WHEN END IS NULL IS...", data);
                _this12.requests3 = data.data;
                _this12.requests = data.data;
                _this12.dataReq = _this12.requests3.filter(function (data) {
                  return data.transType === "Data" && data.state === "complete";
                });
                _this12.airtimeReq = _this12.requests3.filter(function (data) {
                  return data.transType === "M2A" && data.state === "complete";
                });
                _this12.conversionReq = _this12.requests3.filter(function (data) {
                  return data.transType === "A2M" && data.state === "complete";
                });
                console.log("DATA.....", _this12.dataReq);
                console.log("AIRTIME....", _this12.airtimeReq);
                console.log("CONVERSION....", _this12.conversionReq);

                _this12.dataReq.forEach(function (data) {
                  _this12.dataCredit = _this12.dataCredit + data.amount;
                  _this12.dataCash = _this12.dataCash + data.mpesaAmount;
                });

                _this12.airtimeReq.forEach(function (data) {
                  _this12.airtimeCredit = _this12.airtimeCredit + data.airtimeAmount;
                  _this12.airtimeCash = _this12.airtimeCash + data.mpesaAmount;
                  _this12.airtimeProfit = Math.floor(_this12.airtimeCredit * 0.1);
                  _this12.dataProfit = Math.floor(_this12.dataCredit * 0.1);
                  _this12.totalProfit = _this12.airtimeProfit + _this12.dataProfit;
                });

                _this12.conversionReq.forEach(function (data) {
                  _this12.conversionAirtime = _this12.conversionAirtime + data.amount;
                  _this12.conversionCash = _this12.conversionCash + data.mpesaAmount;

                  if (data.mpesaAmount > 1000) {
                    _this12.conversionCost = _this12.conversionCost + 8;
                  } else if (data.mpesaAmount < 1000 && data.mpesaAmount > 100) {
                    _this12.conversionCost = _this12.conversionCost + 4;
                  } else {// this.conversionCost = this.conversionCost  
                  }
                });

                var conv1 = _this12.requests3.filter(function (data) {
                  return data.transType === "A2M" && data.sessionId !== "Website Request" && data.sessionId !== "App";
                });

                var air1 = _this12.requests3.filter(function (data) {
                  return data.transType === "M2A" && data.sessionId !== "Website Request" && data.sessionId !== "App";
                });

                var data1 = _this12.requests3.filter(function (data) {
                  return data.transType === "Data" && data.sessionId !== "Website Request" && data.sessionId !== "App";
                });

                _this12.conversionCost1 = (conv1.length + air1.length + data1.length) * 1.5;
                /*  conv1.forEach(data=>{
                     this.conversionCost1 = this.conversionCost1 + 1.5
                    
                        })*/

                console.log("data transaction..", _this12.dataCash, _this12.dataCredit, "profit...", _this12.dataProfit);
                console.log("airtime transaction..", _this12.airtimeCash, _this12.airtimeCredit, "profit...", _this12.airtimeProfit);
                console.log("PROFIT....", _this12.totalProfit);
                console.log("CONVERSION MPESA TOTAL", _this12.conversionCash);
                console.log("CONVERSION AIRTIME TOTAL", _this12.conversionAirtime);
                console.log("CONVERSION COST..", _this12.conversionCost);
                console.log("CONVERSION COST UNFULL + FULLFILLED..", _this12.conversionCost1, conv1.length, air1.length, data1.length);

                _this12.ref.markForCheck();
              });
            }
          }
        }, {
          key: "onInput",
          value: function onInput(event) {
            var _this13 = this;

            console.log(event);
            var val = this.searchKey = event.target.value;
            console.log(event.target.value);
            console.log("search value is...", event.target.value);

            if (event.target.value === "") {
              console.log("nothing to search");
              this.devices = this.devices3;
              this.ref.markForCheck();
            } else if (val && val.trim() != '') {
              this.requestUssd.findByNameDevices(this.devices3, val).then(function (data) {
                _this13.devices = data;

                _this13.ref.markForCheck();
              })["catch"](function (error) {
                return alert(JSON.stringify(error));
              });
            }
          }
        }, {
          key: "onCancel",
          value: function onCancel(event) {
            this.devices = this.devices3;
            this.ref.markForCheck();
          }
        }]);

        return FinanceComponent;
      }();

      FinanceComponent.ctorParameters = function () {
        return [{
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: _get_requests_service__WEBPACK_IMPORTED_MODULE_3__["GetRequestsService"]
        }, {
          type: ngx_socket_io__WEBPACK_IMPORTED_MODULE_4__["Socket"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"]
        }];
      };

      FinanceComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-finance',
        changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./finance.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/finance/finance/finance.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./finance.component.scss */
        "./src/app/finance/finance/finance.component.scss"))["default"]]
      })], FinanceComponent);
      /*
      
      <ion-row>
        <ion-col>
          {{conversionAirtime}}
        </ion-col>
      
        <ion-col>
          {{conversionCash}}
        </ion-col>
      
      
        <ion-col>
          {{conversionCost}}
        </ion-col>
      
      </ion-row>
      
      
      
      
      
      <ion-row>
        <ion-col>
          {{conversionAirtime}}
        </ion-col>
      
        <ion-col>
          {{conversionCash}}
        </ion-col>
      
      
        <ion-col>
          {{conversionCost}}
        </ion-col>
      
      </ion-row>
      
      */

      /***/
    },

    /***/
    "./src/app/folder/folder-routing.module.ts":
    /*!*************************************************!*\
      !*** ./src/app/folder/folder-routing.module.ts ***!
      \*************************************************/

    /*! exports provided: FolderPageRoutingModule */

    /***/
    function srcAppFolderFolderRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FolderPageRoutingModule", function () {
        return FolderPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _folder_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./folder.page */
      "./src/app/folder/folder.page.ts");
      /* harmony import */


      var _sms_sms_sms_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../sms/sms/sms.component */
      "./src/app/sms/sms/sms.component.ts");
      /* harmony import */


      var _finance_finance_finance_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../finance/finance/finance.component */
      "./src/app/finance/finance/finance.component.ts");
      /* harmony import */


      var _user_zone_user_zone_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../user-zone/user-zone.page */
      "./src/app/user-zone/user-zone.page.ts");
      /* harmony import */


      var _charts_charts_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../charts/charts.component */
      "./src/app/charts/charts.component.ts");
      /* harmony import */


      var _app_adds_app_adds_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../app-adds/app-adds.component */
      "./src/app/app-adds/app-adds.component.ts");

      var routes = [{
        path: 'requests',
        component: _folder_page__WEBPACK_IMPORTED_MODULE_3__["FolderPage"]
      }, {
        path: 'sms',
        component: _sms_sms_sms_component__WEBPACK_IMPORTED_MODULE_4__["SmsComponent"]
      }, {
        path: 'finance',
        component: _finance_finance_finance_component__WEBPACK_IMPORTED_MODULE_5__["FinanceComponent"]
      }, {
        path: 'userzone',
        component: _user_zone_user_zone_page__WEBPACK_IMPORTED_MODULE_6__["UserZonePage"]
      }, {
        path: 'charts',
        component: _charts_charts_component__WEBPACK_IMPORTED_MODULE_7__["ChartsComponent"]
      }, {
        path: 'addData',
        component: _app_adds_app_adds_component__WEBPACK_IMPORTED_MODULE_8__["AppAddsComponent"]
      }];

      var FolderPageRoutingModule = function FolderPageRoutingModule() {
        _classCallCheck(this, FolderPageRoutingModule);
      };

      FolderPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], FolderPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/folder/folder.module.ts":
    /*!*****************************************!*\
      !*** ./src/app/folder/folder.module.ts ***!
      \*****************************************/

    /*! exports provided: FolderPageModule */

    /***/
    function srcAppFolderFolderModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FolderPageModule", function () {
        return FolderPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _folder_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./folder-routing.module */
      "./src/app/folder/folder-routing.module.ts");
      /* harmony import */


      var _folder_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./folder.page */
      "./src/app/folder/folder.page.ts");
      /* harmony import */


      var _sms_sms_sms_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../sms/sms/sms.component */
      "./src/app/sms/sms/sms.component.ts");
      /* harmony import */


      var _finance_finance_finance_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../finance/finance/finance.component */
      "./src/app/finance/finance/finance.component.ts");
      /* harmony import */


      var _charts_charts_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ../charts/charts.component */
      "./src/app/charts/charts.component.ts");
      /* harmony import */


      var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @angular/material/form-field */
      "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/form-field.js");
      /* harmony import */


      var _angular_material_input__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! @angular/material/input */
      "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/input.js");
      /* harmony import */


      var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! @angular/material/datepicker */
      "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/datepicker.js");
      /* harmony import */


      var _angular_material_core__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! @angular/material/core */
      "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _app_adds_app_adds_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
      /*! ../app-adds/app-adds.component */
      "./src/app/app-adds/app-adds.component.ts");
      /* harmony import */


      var _edit_app_data_edit_app_data_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
      /*! ../edit-app-data/edit-app-data.component */
      "./src/app/edit-app-data/edit-app-data.component.ts");

      var FolderPageModule = function FolderPageModule() {
        _classCallCheck(this, FolderPageModule);
      };

      FolderPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _folder_routing_module__WEBPACK_IMPORTED_MODULE_5__["FolderPageRoutingModule"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_10__["MatFormFieldModule"], _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_12__["MatDatepickerModule"], _angular_material_input__WEBPACK_IMPORTED_MODULE_11__["MatInputModule"], _angular_material_core__WEBPACK_IMPORTED_MODULE_13__["MatNativeDateModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"]],
        providers: [_angular_material_datepicker__WEBPACK_IMPORTED_MODULE_12__["MatDatepickerModule"], _angular_material_core__WEBPACK_IMPORTED_MODULE_13__["MatNativeDateModule"]],
        declarations: [_folder_page__WEBPACK_IMPORTED_MODULE_6__["FolderPage"], _sms_sms_sms_component__WEBPACK_IMPORTED_MODULE_7__["SmsComponent"], _finance_finance_finance_component__WEBPACK_IMPORTED_MODULE_8__["FinanceComponent"], _charts_charts_component__WEBPACK_IMPORTED_MODULE_9__["ChartsComponent"], _app_adds_app_adds_component__WEBPACK_IMPORTED_MODULE_14__["AppAddsComponent"], _edit_app_data_edit_app_data_component__WEBPACK_IMPORTED_MODULE_15__["EditAppDataComponent"]]
      })], FolderPageModule);
      /***/
    },

    /***/
    "./src/app/folder/folder.page.scss":
    /*!*****************************************!*\
      !*** ./src/app/folder/folder.page.scss ***!
      \*****************************************/

    /*! exports provided: default */

    /***/
    function srcAppFolderFolderPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-menu-button {\n  color: var(--ion-color-primary);\n}\n\n#container {\n  text-align: center;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 50%;\n  transform: translateY(-50%);\n}\n\n#container strong {\n  font-size: 20px;\n  line-height: 26px;\n}\n\n#container p {\n  font-size: 16px;\n  line-height: 22px;\n  color: #8c8c8c;\n  margin: 0;\n}\n\n#container a {\n  text-decoration: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZm9sZGVyL2ZvbGRlci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSwrQkFBQTtBQUNGOztBQUVBO0VBQ0Usa0JBQUE7RUFDQSxrQkFBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0VBQ0EsUUFBQTtFQUNBLDJCQUFBO0FBQ0Y7O0FBRUE7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QUFDRjs7QUFFQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxTQUFBO0FBQ0Y7O0FBRUE7RUFDRSxxQkFBQTtBQUNGIiwiZmlsZSI6InNyYy9hcHAvZm9sZGVyL2ZvbGRlci5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tbWVudS1idXR0b24ge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xufVxuXG4jY29udGFpbmVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDA7XG4gIHJpZ2h0OiAwO1xuICB0b3A6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xufVxuXG4jY29udGFpbmVyIHN0cm9uZyB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgbGluZS1oZWlnaHQ6IDI2cHg7XG59XG5cbiNjb250YWluZXIgcCB7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgbGluZS1oZWlnaHQ6IDIycHg7XG4gIGNvbG9yOiAjOGM4YzhjO1xuICBtYXJnaW46IDA7XG59XG5cbiNjb250YWluZXIgYSB7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/folder/folder.page.ts":
    /*!***************************************!*\
      !*** ./src/app/folder/folder.page.ts ***!
      \***************************************/

    /*! exports provided: FolderPage */

    /***/
    function srcAppFolderFolderPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FolderPage", function () {
        return FolderPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _get_requests_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../get-requests.service */
      "./src/app/get-requests.service.ts");
      /* harmony import */


      var ngx_socket_io__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ngx-socket-io */
      "./node_modules/ngx-socket-io/__ivy_ngcc__/fesm2015/ngx-socket-io.js");
      /* harmony import */


      var _service_pwaupdate_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../service/pwaupdate.service */
      "./src/app/service/pwaupdate.service.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _request_details_request_details_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../request-details/request-details.page */
      "./src/app/request-details/request-details.page.ts");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @ionic/storage */
      "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _auth_auth_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! ../auth/auth.service */
      "./src/app/auth/auth.service.ts");

      var FolderPage = /*#__PURE__*/function () {
        function FolderPage(auth, menuCtrl, storage, router, activatedRoute, requestUssd, socket, ref, pwmIpdeter, modalController) {
          _classCallCheck(this, FolderPage);

          this.auth = auth;
          this.menuCtrl = menuCtrl;
          this.storage = storage;
          this.router = router;
          this.activatedRoute = activatedRoute;
          this.requestUssd = requestUssd;
          this.socket = socket;
          this.ref = ref;
          this.pwmIpdeter = pwmIpdeter;
          this.modalController = modalController;
          this.searchKey = "";
          this.requests = [];
          this.requests3 = [];
          this.M2A = [];
          this.A2M = [];
          this.Data = [];
          this.M2A3 = [];
          this.A2M3 = [];
          this.Data3 = [];
          this.states = "A2M";
          this.filterManual = false;
          this.range = new _angular_forms__WEBPACK_IMPORTED_MODULE_10__["FormGroup"]({
            start: new _angular_forms__WEBPACK_IMPORTED_MODULE_10__["FormControl"](),
            end: new _angular_forms__WEBPACK_IMPORTED_MODULE_10__["FormControl"]()
          });
        }

        _createClass(FolderPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this14 = this;

            // console.log("token data is...", this.auth.getToken());
            this.minDate = new Date(21, 9, 10);
            this.folder = this.activatedRoute.snapshot.paramMap.get('id');
            this.storage.get('token').then(function (data) {
              console.log("token data is...", data);
              _this14.requestUssd.httpOptions = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpHeaders"]({
                  'Content-Type': 'application/json',
                  'amness': data
                })
              };
            })["catch"](function (err) {
              console.log("error occured ...", err);

              _this14.router.navigate(['/login']);
            });
            this.storage.get('authenticated').then(function (data) {
              if (!data) {
                _this14.router.navigate(['/login']);
              }
            });
            this.socket.on('connect', function () {
              console.log('Successfully connected !');
            });
          }
        }, {
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {
            this.menuCtrl.enable(true);
            this.menuCtrl.swipeGesture(true); //  this.auth.isAuthenticated()

            if (this.auth.isAuthenticated(localStorage.getItem("token"))) {
              console.log("token expired");
              this.router.navigate(['/login']);
              this.storage.clear();
              localStorage.clear();
            } else {
              console.log("token NOT expired" + '\n ' + localStorage.getItem("token"));
              this.findAll();
            }
          }
        }, {
          key: "ionViewWillLeave",
          value: function ionViewWillLeave() {
            this.reqSub.unsubscribe();
          }
        }, {
          key: "onInput",
          value: function onInput(event) {
            var _this15 = this;

            console.log(event);
            var val = this.searchKey = event.target.value;
            console.log(event.target.value);
            console.log("search value is...", event.target.value);

            if (event.target.value === "") {
              console.log("nothing to search");
              this.requests = this.requests3;
              this.A2M = this.A2M3;
              this.M2A = this.M2A3;
              this.Data = this.Data3;
              this.ref.markForCheck();
            } else if (val && val.trim() != '') {
              /*  this.requestUssd.findByName(this.requests3 ,this.searchKey)
                .then(data => {
                    this.requests = data;
                    console.log(data);
                    this.ref.markForCheck();
                    
                })
                .catch(error => alert(JSON.stringify(error)));
              */
              //  if(this.states === "A2M"){
              this.requestUssd.findByName(this.A2M3, val).then(function (data) {
                _this15.A2M = data;
                console.log(data);
              })["catch"](function (error) {
                return alert(JSON.stringify(error));
              }); //  }else if(this.states === "M2A"){

              this.requestUssd.findByName(this.M2A3, val).then(function (data) {
                _this15.M2A = data;
                console.log(data);
              })["catch"](function (error) {
                return alert(JSON.stringify(error));
              }); // } else if(this.states === "Data"){

              this.requestUssd.findByName(this.Data3, val).then(function (data) {
                _this15.Data = data;
                console.log(data);
              })["catch"](function (error) {
                return alert(JSON.stringify(error));
              }); //  }
            }
          }
        }, {
          key: "onCancel",
          value: function onCancel(event) {
            this.requests = this.requests3;
            this.ref.markForCheck();
          }
        }, {
          key: "findAll",
          value: function findAll() {
            var _this16 = this;

            if (this.auth.isAuthenticated(localStorage.getItem("token"))) {
              console.log("token expired");
              this.router.navigate(['/login']);
              this.storage.clear();
              localStorage.clear();
            } else {
              console.log("token NOT expired" + '\n ' + localStorage.getItem("token"));
              this.reqSub = this.requestUssd.findAll().subscribe(function (data) {
                console.log(data);
                /*
                  err:
                expiredAt: "2021-09-09T16:23:06.000Z"
                message: "jwt expired"
                */

                if (data.msg === "invalid") {
                  console.log("token expired");

                  _this16.router.navigate(['/login']);

                  _this16.storage.clear();

                  localStorage.clear();
                } else {
                  _this16.requests3 = data.data;
                  _this16.requests = data.data;

                  _this16.seperateOrders(_this16.requests3);

                  _this16.ref.markForCheck();

                  console.log("data available");
                }
              }, function (error) {
                console.log(error);

                _this16.router.navigate(['/login']);

                _this16.storage.clear();

                localStorage.clear();
              });
            }
          }
        }, {
          key: "getUsersList",
          value: function getUsersList(event) {
            var _this17 = this;

            this.requestUssd.findAll().subscribe(function (data) {
              _this17.requests3 = data.data;
              _this17.requests = data.data;

              _this17.seperateOrders(_this17.requests3);

              _this17.ref.markForCheck();

              event.target.complete();
              console.log(data);
            }, function (error) {
              console.log(error);
              event.target.complete();

              _this17.router.navigate(['/login']);

              _this17.storage.clear();

              localStorage.clear();
            });
          }
        }, {
          key: "completTrans",
          value: function completTrans(req) {
            var _this18 = this;

            console.log(req._id);
            this.requestUssd.complete(req._id).subscribe(function (data) {
              _this18.findAll();

              console.log(data);
            });
          }
        }, {
          key: "sendMoney",
          value: function sendMoney(req) {
            var _this19 = this;

            console.log(req._id);
            this.requestUssd.sendMoney(req._id).subscribe(function (data) {
              _this19.findAll();

              console.log(data);
            });
          }
        }, {
          key: "sendCredit",
          value: function sendCredit(req) {
            var _this20 = this;

            console.log(req._id);
            this.requestUssd.sendCredit(req._id).subscribe(function (data) {
              _this20.findAll();

              console.log(data);
            });
          }
        }, {
          key: "presentModal",
          value: function presentModal(data) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
              var modal;
              return regeneratorRuntime.wrap(function _callee7$(_context7) {
                while (1) {
                  switch (_context7.prev = _context7.next) {
                    case 0:
                      _context7.next = 2;
                      return this.modalController.create({
                        component: _request_details_request_details_page__WEBPACK_IMPORTED_MODULE_7__["RequestDetailsPage"],
                        cssClass: 'my-custom-class',
                        showBackdrop: true,
                        swipeToClose: true,
                        animated: true,
                        componentProps: {
                          req: data
                        }
                      });

                    case 2:
                      modal = _context7.sent;
                      _context7.next = 5;
                      return modal.present();

                    case 5:
                      return _context7.abrupt("return", _context7.sent);

                    case 6:
                    case "end":
                      return _context7.stop();
                  }
                }
              }, _callee7, this);
            }));
          }
        }, {
          key: "seperateOrders",
          value: function seperateOrders(data) {
            //"Pending Payment Aproval" //Pending Payment Aproval //Paid //Shipped//Fullfilled
            console.log(data, "orders...");
            this.A2M = data.filter(function (requests) {
              return requests.transType === "A2M";
            });
            this.M2A = data.filter(function (requests) {
              return requests.transType === "M2A";
            });
            this.Data = data.filter(function (requests) {
              return requests.transType === "Data";
            });
            this.A2M3 = this.A2M;
            this.M2A3 = this.M2A;
            this.Data3 = this.Data;
            console.log(this.A2M);
          }
        }, {
          key: "getByDate",
          value: function getByDate() {
            var _this21 = this;

            var date = this.range.value;

            if (date.start === null) {
              console.log("please set date");
            } else if (date.end === null) {
              var startDate = new Date(date.start);
              var endDate = new Date(); //date.end

              console.log("start", startDate, "End", endDate);
              var query = {
                start: startDate,
                end: endDate
              };
              this.requestUssd.getByDate(query).subscribe(function (data) {
                //  this.ref.markForCheck();
                console.log("REQUESTED DATA WHEN END IS NULL IS...", data);

                if (data.msg === "invalid") {
                  console.log("token expired");

                  _this21.router.navigate(['/login']);

                  _this21.storage.clear();

                  localStorage.clear();
                } else {
                  _this21.requests3 = data.data;
                  _this21.requests = data.data;

                  _this21.seperateOrders(_this21.requests3);

                  _this21.ref.markForCheck();

                  console.log("data available");
                }
              }, function (error) {
                console.log(error);

                _this21.router.navigate(['/login']);

                _this21.storage.clear();

                localStorage.clear();
              });
            } else {
              /*
               let startDate = new Date(date.start).valueOf();
                      let endDate = new Date(date.end).valueOf();
              */
              var _startDate2 = new Date(date.start);

              var _endDate2 = new Date(date.end);

              console.log("start", _startDate2, "End", _endDate2);
              var _query2 = {
                start: _startDate2,
                end: _endDate2
              };
              this.requestUssd.getByDate(_query2).subscribe(function (data) {
                //  this.ref.markForCheck();
                console.log("REQUESTED DATA WHEN END IS NULL IS...", data);

                if (data.msg === "invalid") {
                  console.log("token expired");

                  _this21.router.navigate(['/login']);

                  _this21.storage.clear();

                  localStorage.clear();
                } else {
                  _this21.requests3 = data.data;
                  _this21.requests = data.data;

                  _this21.seperateOrders(_this21.requests3);

                  _this21.ref.markForCheck();

                  console.log("data available");
                }
              }, function (error) {
                console.log(error);

                _this21.router.navigate(['/login']);

                _this21.storage.clear();

                localStorage.clear();
              });
            }

            if (this.auth.isAuthenticated(localStorage.getItem("token"))) {
              console.log("token expired");
              this.router.navigate(['/login']);
              this.storage.clear();
              localStorage.clear();
            } else {
              console.log("token NOT expired" + '\n ' + localStorage.getItem("token"));
            }
          }
        }, {
          key: "placeFilters",
          value: function placeFilters() {
            if (this.filterManual) {
              console.log("filter is..", this.filterManual);
              this.A2M = this.A2M.filter(function (requests) {
                return requests.manual === true;
              });
              this.M2A = this.M2A.filter(function (requests) {
                return requests.manual === true;
              });
              this.Data = this.Data.filter(function (requests) {
                return requests.manual === true;
              });
              console.log(" first", this.A2M);
            } else {}

            console.log(" last", this.A2M);
            this.A2M3 = this.A2M;
            this.M2A3 = this.M2A;
            this.Data3 = this.Data;
            this.ref.markForCheck();
          }
        }, {
          key: "clearFilters",
          value: function clearFilters() {
            this.filterManual = false;
            this.seperateOrders(this.requests);
            this.ref.markForCheck();
          }
        }]);

        return FolderPage;
      }();

      FolderPage.ctorParameters = function () {
        return [{
          type: _auth_auth_service__WEBPACK_IMPORTED_MODULE_11__["AuthService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["MenuController"]
        }, {
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_8__["Storage"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: _get_requests_service__WEBPACK_IMPORTED_MODULE_3__["GetRequestsService"]
        }, {
          type: ngx_socket_io__WEBPACK_IMPORTED_MODULE_4__["Socket"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]
        }, {
          type: _service_pwaupdate_service__WEBPACK_IMPORTED_MODULE_5__["PwaupdateService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"]
        }];
      };

      FolderPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-folder',
        changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./folder.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/folder/folder.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./folder.page.scss */
        "./src/app/folder/folder.page.scss"))["default"]]
      })], FolderPage); //254740092818  254769250037  254748387837   254740734030 

      /***/
    },

    /***/
    "./src/app/sms/sms/sms.component.scss":
    /*!********************************************!*\
      !*** ./src/app/sms/sms/sms.component.scss ***!
      \********************************************/

    /*! exports provided: default */

    /***/
    function srcAppSmsSmsSmsComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Ntcy9zbXMvc21zLmNvbXBvbmVudC5zY3NzIn0= */";
      /***/
    },

    /***/
    "./src/app/sms/sms/sms.component.ts":
    /*!******************************************!*\
      !*** ./src/app/sms/sms/sms.component.ts ***!
      \******************************************/

    /*! exports provided: SmsComponent */

    /***/
    function srcAppSmsSmsSmsComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SmsComponent", function () {
        return SmsComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _get_requests_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../../get-requests.service */
      "./src/app/get-requests.service.ts");
      /* harmony import */


      var ngx_socket_io__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ngx-socket-io */
      "./node_modules/ngx-socket-io/__ivy_ngcc__/fesm2015/ngx-socket-io.js");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/storage */
      "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");

      var SmsComponent = /*#__PURE__*/function () {
        function SmsComponent(activatedRoute, storage, router, requestUssd, socket, ref) {
          _classCallCheck(this, SmsComponent);

          this.activatedRoute = activatedRoute;
          this.storage = storage;
          this.router = router;
          this.requestUssd = requestUssd;
          this.socket = socket;
          this.ref = ref;
          this.searchKey = "";
          this.device = "";
          this.myDevices = [];
          this.sms = [];
          this.title = "";
        }

        _createClass(SmsComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this22 = this;

            this.storage.get('authenticated').then(function (data) {
              if (!data) {
                _this22.router.navigate(['/login']);
              }
            });
            this.socket.on('connect', function () {
              console.log('Successfully connected !');
            });
            this.socket.on('connect', function () {
              console.log('Successfully connected !');

              _this22.socket.emit('dashRegister');
            });
            this.socket.on('dashSms', function (data) {
              if (data.client === _this22.searchKey) {
                console.log('Received sms!', data);

                if (data.responce === "success") {
                  _this22.sms = data.sms;

                  _this22.ref.markForCheck();
                } else {
                  _this22.sms = data.sms;

                  _this22.ref.markForCheck();

                  _this22.title = data.responce;
                }
              }
            });
            this.requestUssd.findDevices({
              name: null
            }).subscribe(function (data) {
              _this22.myDevices = data.data;
              console.log("myDevices are..", _this22.myDevices);

              if (data.msg === "invalid") {
                console.log("token expired");

                _this22.router.navigate(['/login']);

                _this22.storage.clear();
              } else {}
            });
          }
        }, {
          key: "sendReq",
          value: function sendReq() {
            console.log("clicked retrive sms...");
            this.socket.emit("retriveSms", {
              phone: this.device,
              client: this.searchKey
            });
          }
        }, {
          key: "onCancel",
          value: function onCancel() {
            this.searchKey = "";
          }
        }]);

        return SmsComponent;
      }();

      SmsComponent.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _get_requests_service__WEBPACK_IMPORTED_MODULE_3__["GetRequestsService"]
        }, {
          type: ngx_socket_io__WEBPACK_IMPORTED_MODULE_4__["Socket"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]
        }];
      };

      SmsComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-sms',
        changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./sms.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/sms/sms/sms.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./sms.component.scss */
        "./src/app/sms/sms/sms.component.scss"))["default"]]
      })], SmsComponent);
      /***/
    }
  }]);
})();
//# sourceMappingURL=folder-folder-module-es5.js.map