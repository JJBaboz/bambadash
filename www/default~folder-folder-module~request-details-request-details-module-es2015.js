(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~folder-folder-module~request-details-request-details-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/request-details/request-details.page.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/request-details/request-details.page.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\n\r\n<ion-content>\r\n\r\n  <ion-card  >\r\n\r\n    <ion-card-header>\r\n  \r\n      <ion-item>\r\n        <ion-avatar item-start>\r\n          <img src=\"assets/img/cash.png\">\r\n        </ion-avatar>\r\n\r\n        <ion-button slot=\"end\" color=\"primary\" (click)=\"dismiss()\" >\r\n          Close\r\n          </ion-button>\r\n\r\n      </ion-item>\r\n\r\n      <ion-card-subtitle>{{req.date | date:'medium' }}</ion-card-subtitle>\r\n      <ion-card-title>\r\n        {{req.phone}}\r\n      </ion-card-title>\r\n\r\n      <ion-card-title *ngIf=\"req.transType== 'M2A'\">\r\n      Type:  MPESA TO AIRTIME\r\n      </ion-card-title>\r\n\r\n      <ion-card-subtitle *ngIf=\"req.transType== 'M2A' \" > Completed At : {{req.completionTime | date:'medium' }}</ion-card-subtitle>\r\n\r\n      <ion-card-title *ngIf=\"req.transType== 'A2M'\">\r\n        Type:  AIRTIME TO MPESA\r\n        </ion-card-title>\r\n\r\n\r\n        <ion-card-title *ngIf=\"req.transType== 'Data'\">\r\n          Type:  DATA BUNDLES PURCHASE\r\n          </ion-card-title>\r\n\r\n          <ion-card-subtitle *ngIf=\"req.transType== 'M2A' \" > Completed At : {{req.completionTime | date:'medium' }}</ion-card-subtitle>\r\n\r\n\r\n    </ion-card-header>\r\n\r\n\r\n  <ion-card-content>\r\n   \r\n  <ion-list>\r\n\r\n\r\n    <ion-item>\r\n      <ion-label>\r\n        State\r\n      </ion-label>\r\n      \r\n      {{req.state}}\r\n    </ion-item>\r\n  \r\n\r\n    \r\n    <ion-item >\r\n      <ion-label>\r\n        Device Name\r\n      </ion-label>\r\n      \r\n      {{req.devname}}\r\n    </ion-item>\r\n\r\n\r\n    \r\n    <ion-item >\r\n      <ion-label>\r\n        Session\r\n      </ion-label>\r\n      \r\n      {{req.sessionId}}\r\n    </ion-item>\r\n\r\n\r\n    <ion-item >\r\n      <ion-label>\r\n        Data Id\r\n      </ion-label>\r\n      \r\n      {{req._id}}\r\n    </ion-item>\r\n\r\n\r\n    <ion-item >\r\n      <ion-label *ngIf=\"req.transType == 'M2A'\">\r\n       Airtime to be received\r\n      </ion-label>\r\n\r\n      <ion-label *ngIf=\"req.transType== 'A2M'\">\r\n        Amount of Airtime to Convert\r\n       </ion-label>\r\n\r\n       <ion-label *ngIf=\"req.transType== 'Data'\">\r\n        Amount of Airtime the Data will cost US.\r\n       </ion-label>\r\n\r\n\r\n        {{req.airtimeAmount? req.airtimeAmount:req.amount}}\r\n\r\n\r\n    </ion-item>\r\n\r\n\r\n    <ion-item >\r\n      <ion-label *ngIf=\"req.transType== 'M2A'\">\r\n       Amount of Cash to be paid BY Client\r\n      </ion-label>\r\n\r\n      <ion-label *ngIf=\"req.transType== 'Data'\">\r\n        Amount of Cash to be paid BY Client\r\n       </ion-label>\r\n\r\n      <ion-label *ngIf=\"req.transType== 'A2M'\">\r\n        Amount of Cash to be paid TO Client\r\n       </ion-label>\r\n\r\n\r\n        {{req.mpesaAmount}}\r\n    </ion-item>\r\n\r\n    <ion-item  *ngIf=\"req.transType== 'Data'\">\r\n      <ion-label>\r\n       Data Type:\r\n      </ion-label>\r\n      {{req.Provider}}---{{req.Category}}---{{req.dataAmount}}\r\n      </ion-item>\r\n\r\n\r\n      <ion-item *ngIf=\"req.transType== 'Data'\">\r\n        <ion-label >\r\n         Device Responce:\r\n        </ion-label>\r\n        {{req.dataSms == \"\" ?  'Incomplete':req.dataSms}}\r\n        </ion-item>\r\n\r\n\r\n        <ion-item *ngIf=\"req.transType== 'M2A'\">\r\n          <ion-label >\r\n           Device Responce:\r\n          </ion-label>\r\n          {{req.sms == \"\" ?  'Incomplete':req.sms}}\r\n          </ion-item>\r\n     \r\n\r\n<ion-item *ngIf=\"req.transType == 'A2M'\">\r\n     \r\n      <ion-label >\r\n        Name of Client :\r\n       </ion-label>\r\n        {{req.state == 'complete' && req.MpesaDetails[0] !== undefined? req.MpesaDetails[0].Value:'Incomplete transaction'}}\r\n    </ion-item>\r\n\r\n    <ion-item *ngIf=\"req.transType == 'A2M'\">\r\n      <ion-label>\r\n        TransactionID: \r\n      </ion-label>\r\n    \r\n      {{req.TransactionID}}\r\n    </ion-item>\r\n\r\n    <ion-item *ngIf=\"req.transType == 'M2A' || req.transType == 'Data' || req.sessionId != 'b2bApiPurchase' \">\r\n      <ion-label>\r\n        TransactionID:\r\n      </ion-label>\r\n    \r\n      {{req.MpesaDetails[0] !== undefined? req.MpesaDetails[1].Value:'Mpesa Transaction not completed by client'}}\r\n    </ion-item>\r\n\r\n    <ion-item *ngIf=\"req.sessionId == 'b2bApiPurchase' \">\r\n      <ion-label>\r\n        Source: \r\n      </ion-label>\r\n    \r\n      {{req.source}}\r\n    </ion-item>\r\n\r\n    <ion-item *ngIf=\"req.sessionId == 'b2bApiPurchase' \">\r\n      <ion-label>\r\n        SourceId: \r\n      </ion-label>\r\n    \r\n      {{req.sourceID}}\r\n    </ion-item>\r\n\r\n\r\n<ion-item>\r\n<ion-label>\r\nMpesa Response: \r\n</ion-label>\r\n\r\n{{req.ResultDesc}}\r\n</ion-item>\r\n\r\n\r\n\r\n<ion-item *ngIf=\"req.transType == 'M2A' || req.transType == 'Data' \">\r\n  <ion-label>\r\n    Distribution: \r\n  </ion-label>\r\n\r\n  <div *ngFor=\"let item of req.distribution\">\r\n\r\n   Device: {{item? item.Device:\"null\"}} ... Amount: {{item?item.amount:\"null\"}}\r\n\r\n  </div>\r\n\r\n\r\n</ion-item>\r\n\r\n\r\n\r\n<ion-item *ngIf= \"req.manual\" >\r\n  <ion-label>\r\n  Manual Trans: \r\n  </ion-label>\r\n\r\n  <ion-grid>\r\n    <ion-row>\r\n      <p>\r\n        By: {{req.manualDescr.by}}\r\n      </p>\r\n    </ion-row>\r\n\r\n\r\n    <ion-row>\r\n      <p>\r\n        aprovedBy: {{req.manualDescr.aprovedBy}}\r\n      </p>\r\n    </ion-row>\r\n\r\n  \r\n\r\n    <ion-row>\r\n      <p>\r\n        Description: {{req.manualDescr.reason}}\r\n      </p>\r\n    </ion-row>\r\n\r\n\r\n    <ion-row>\r\n      <p>\r\n        Time: {{req.manualDescr.time | date: 'medium'}}\r\n      </p>\r\n    </ion-row>\r\n\r\n    <ion-row>\r\n      <p>\r\n        approvedAt: {{req.manualDescr.approvedAt | date: 'medium'}}\r\n      </p>\r\n    </ion-row>\r\n\r\n    <ion-row>\r\n      <p>\r\n        Type: {{req.manualDescr.type}}\r\n      </p>\r\n    </ion-row>\r\n\r\n\r\n  </ion-grid>\r\n\r\n</ion-item>\r\n\r\n\r\n  \r\n\r\n<ion-item *ngIf= \"!req.manual\" >\r\n  <ion-label floating>\r\n  Transaction purpose:\r\n  </ion-label>\r\n  \r\n <ion-input type=\"text\" [(ngModel)]=\"purpose\" placeholder=\"Purpose of manual transaction\"></ion-input>\r\n\r\n  </ion-item>\r\n\r\n\r\n\r\n    </ion-list>\r\n  </ion-card-content>\r\n\r\n\r\n  <ion-button (click)=\"sendCredit(req)\" [disabled] = \"req.state == 'complete' || req.transType == 'A2M'  \">\r\n    \r\n    Send Airtime\r\n  </ion-button>\r\n\r\n  <ion-button *ngIf=\" user == 'babzgeo27@gmail.com'? true:user == 'mutie.michael@gmail.com'? true:user == 'gmngari1@gmail.com'? true:false  \"  (click)=\"sendMoney(req)\" [disabled] = \"req.state == 'complete' || req.transType != 'A2M' \">\r\n    \r\n  Send Money\r\n  </ion-button>\r\n\r\n\r\n  <ion-button *ngIf=\" user == 'babzgeo27@gmail.com'? false:user == 'mutie.michael@gmail.com'? false:user == 'gmngari1@gmail.com'? false:true \"  (click)=\"sendMoneyApproval(req)\" [disabled] = \"req.state == 'complete' || req.transType != 'A2M' \">\r\n    \r\n    Send Money Approval\r\n    </ion-button>\r\n\r\n\r\n  <ion-button (click)=\"sendData(req)\" [disabled] = \"req.state != 'complete' && req.transType == 'Data'? false:true\">\r\n    \r\n    Send Data\r\n  </ion-button>\r\n \r\n  <ion-button color=\"danger\" (click)=\"completTrans(req)\" [disabled] = \"req.state != 'complete'  ? false:true\">\r\n  Cancel\r\n  </ion-button>\r\n\r\n</ion-card>\r\n\r\n\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/get-requests.service.ts":
/*!*****************************************!*\
  !*** ./src/app/get-requests.service.ts ***!
  \*****************************************/
/*! exports provided: GetRequestsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetRequestsService", function() { return GetRequestsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");








//let url = environment.url3;
let url = _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].url2;
let httpOptions3 = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/json' })
};
let GetRequestsService = class GetRequestsService {
    constructor(router, storage, http) {
        this.router = router;
        this.storage = storage;
        this.http = http;
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/json' })
        };
    }
    clientContacts() {
        return this.http.post(`${url}/web/getdistinct`, {}, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    sendSms(phone, message, recepient) {
        return this.http.post(`${url}/web/sendSms`, { phone: phone, message: message, recepient: recepient }, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    getByDate(query) {
        return this.http.post(`${url}/web/getByDate`, { query: query }, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    login(data) {
        return this.http.post(`${url}/auth/login`, data, httpOptions3).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    register(data) {
        return this.http.post(`${url}/auth/register`, data, httpOptions3).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    findAll() {
        return this.http.get(`${url}/web/allrequests`, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    //post
    findDevices(device) {
        return this.http.post(`${url}/web/phoneBalance`, device, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    getUssdBal() {
        return this.http.get(`${url}/sms/balance`, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    mpesaBalance(transType) {
        return this.http.post(`${url}/web/mpesaBalance`, { type: transType }, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    sendDist(req) {
        return this.http.post(`${url}/web/sendDistributor`, req, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    mpesaBalance3(transType) {
        return this.http.post(`${url}/web/bcbalance`, { type: transType }, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    complete(myId) {
        return this.http.post(`${url}/web/markComplete`, myId, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    sendMoney(myId) {
        return this.http.post(`${url}/web/sendMoney`, myId, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    sendMoneyApproval(myId) {
        return this.http.post(`${url}/web/sendMoneyApproval`, myId, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    sendData(myId) {
        return this.http.post(`${url}/web/sendData`, myId, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    sendCredit(myId) {
        return this.http.post(`${url}/web/sendCredit`, myId, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    findByName(requests, searchKey) {
        let key = searchKey.toUpperCase();
        return Promise.resolve(requests.filter((requests) => (requests.sourceID + ' ' + requests.source + ' ' + requests._id + ' ' + requests.phone + ' ' + requests.transType + ' ' + requests.state + ' ' + requests.sessionId).toUpperCase().indexOf(key) > -1));
    }
    findByNameDevices(requests, searchKey) {
        let key = searchKey.toUpperCase();
        return Promise.resolve(requests.filter((requests) => (requests._id + ' ' + requests.phone + ' ' + requests.name + ' ' + requests.state).toUpperCase().indexOf(key) > -1));
    }
    extractData(res) {
        let body = res;
        return body || {};
    }
    handleError(error) {
        let errMsg;
        if (error instanceof Response) { //TokenExpiredError jwt expired
            const err = error || '';
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(errMsg);
    }
};
GetRequestsService.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
GetRequestsService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], GetRequestsService);



/***/ }),

/***/ "./src/app/request-details/request-details.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/request-details/request-details.page.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JlcXVlc3QtZGV0YWlscy9yZXF1ZXN0LWRldGFpbHMucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/request-details/request-details.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/request-details/request-details.page.ts ***!
  \*********************************************************/
/*! exports provided: RequestDetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestDetailsPage", function() { return RequestDetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _get_requests_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../get-requests.service */ "./src/app/get-requests.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");







let RequestDetailsPage = class RequestDetailsPage {
    constructor(alertController, storage, router, requestUssd, modalController) {
        this.alertController = alertController;
        this.storage = storage;
        this.router = router;
        this.requestUssd = requestUssd;
        this.modalController = modalController;
        this.purpose = "";
        this.user = "";
    }
    ngOnInit() {
        //  console.log(this.req);
        this.purpose = this.req.manual ? this.req.manualDescr.reason : "";
        this.storage.get('profile').then(data => {
            if (!data) {
                this.router.navigate(['/login']);
            }
            else {
                console.log(data);
                this.profile = data.profile;
                this.user = data.profile.email;
                //this.user = "jdfnsif"
            }
        });
        // && user != 'babzgeo27@gmail.com' || user != 'mutie.michael@gmail.com' || user != 'gmngari1@gmail.com'   
    }
    completTrans(req) {
        console.log(req._id);
        let id = req._id;
        req = {};
        req.id = id;
        req.phone = this.profile.phone;
        req.email = this.profile.email;
        req.purpose = this.purpose;
        this.requestUssd.complete(req).subscribe((data) => {
            // this.findAll()
            console.log(data);
            this.presentAlert(data.response);
        });
    }
    sendMoney(Data) {
        console.log(Data._id);
        let req = {};
        req.id = Data._id;
        req.phone = this.profile.phone;
        req.email = this.profile.email;
        req.purpose = this.purpose;
        if (req.manual) {
            req.email1 = Data.manualDescr ? Data.manualDescr.by : this.profile.email;
            req.time1 = Data.manualDescr.time;
        }
        this.requestUssd.sendMoney(req).subscribe((data) => {
            // this.findAll()
            console.log(data);
            this.presentAlert(data.response);
        });
    }
    sendMoneyApproval(req) {
        console.log(req._id);
        let id = req._id;
        req = {};
        req.id = id;
        req.phone = this.profile.phone;
        req.email = this.profile.email;
        req.purpose = this.purpose;
        this.requestUssd.sendMoneyApproval(req).subscribe((data) => {
            // this.findAll()
            console.log(data);
            this.presentAlert(data.response);
        });
    }
    sendData(req) {
        console.log(req._id);
        let data = {};
        data.id = req._id;
        data.phone = this.profile.phone;
        data.email = this.profile.email;
        data.purpose = this.purpose;
        data.Provider = req.Provider;
        this.requestUssd.sendData(req).subscribe((data) => {
            // this.findAll()
            console.log(data);
            this.presentAlert(data.response);
        });
    }
    sendCredit(req) {
        console.log(req._id);
        let id = req._id;
        req = {};
        req.id = id;
        req.phone = this.profile.phone;
        req.email = this.profile.email;
        req.purpose = this.purpose;
        this.requestUssd.sendCredit(req).subscribe((data) => {
            //this.findAll()
            console.log(data);
            this.presentAlert(data.response);
        });
    }
    dismiss() {
        // using the injected ModalController this page
        // can "dismiss" itself and optionally pass back data
        this.modalController.dismiss({
            'dismissed': true
        });
    }
    presentAlert(message) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'my-custom-class',
                header: 'Responce',
                subHeader: '',
                message: message,
                buttons: ['OK']
            });
            yield alert.present();
            const { role } = yield alert.onDidDismiss();
            console.log('onDidDismiss resolved with role', role);
        });
    }
};
RequestDetailsPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _get_requests_service__WEBPACK_IMPORTED_MODULE_4__["GetRequestsService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] }
];
RequestDetailsPage.propDecorators = {
    req: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"] }]
};
RequestDetailsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-request-details',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./request-details.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/request-details/request-details.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./request-details.page.scss */ "./src/app/request-details/request-details.page.scss")).default]
    })
], RequestDetailsPage);



/***/ })

}]);
//# sourceMappingURL=default~folder-folder-module~request-details-request-details-module-es2015.js.map