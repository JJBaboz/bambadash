(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-login-login-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login/login.page.html":
    /*!***********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login/login/login.page.html ***!
      \***********************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppLoginLoginLoginPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>login</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<!-- -->\r\n<ion-content padding class=\"profiles-bg\">\r\n\t<div class=\"auth-content\">\r\n\r\n\r\n\r\n\r\n\t\t<!-- Logo -->\r\n\t\t<div padding-horizontal text-center>\r\n\t\t\t<div >\r\n\t\t\t\r\n\t\t\t</div>\r\n\t\t\t<h2 ion-text class=\"text-secondary\" no-margin>\r\n\t\t\t\t<strong>bamba</strong>Swap\r\n\t\t\t</h2>\r\n\t\t</div>\r\n\r\n\r\n\r\n\r\n\r\n\t\t<div padding-vertical>\r\n\r\n\t\t  <ion-segment [(ngModel)]=\"auth\" color=\"light\">\r\n\r\n\t\t    <ion-segment-button value=\"login\">\r\n\t\t      Login\r\n\t\t\t\t</ion-segment-button>\r\n\r\n\t\t    <ion-segment-button value=\"register\">\r\n\t\t      Register\r\n\t\t\t\t</ion-segment-button>\r\n\r\n\t\t  </ion-segment>\r\n\t\t</div>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\t\t<div [ngSwitch]=\"auth\">\r\n\t\t\t<!-- Login form -->\r\n\t\t\t<div id=\"loginForm\" *ngSwitchCase=\"'login'\">\r\n\r\n\t\t\t\t<form [formGroup]=\"onLoginForm\">\r\n\t\t\t\t\t<ion-item>\r\n\t\t\t\t\t\t<ion-label floating>\r\n\t\t\t\t\t\t\t<ion-icon name=\"call\" item-start class=\"text-white\"></ion-icon>\r\n\t\t\t\t\t\t\tPhone Number\r\n\t\t\t\t\t\t</ion-label>\r\n\t\t\t\t\t\t<ion-input type=\"number\" formControlName=\"phone\"></ion-input>\r\n\t\t\t\t\t</ion-item>\r\n\t\t\t\t\t<p ion-text color=\"danger\" class=\"text-12x has-error\" *ngIf=\"onLoginForm.get('phone').touched && onLoginForm.get('phone').hasError('required')\">This field is required</p>\r\n\t\t\t\t\t<p ion-text color=\"danger\" class=\"text-12x has-error\" *ngIf=\"onLoginForm.get('phone').touched && onLoginForm.get('phone').hasError('minlength')\">Enter a valid phone number</p>\r\n\r\n\t\t\t\t\t<p ion-text color=\"danger\" class=\"text-12x has-error\" *ngIf=\"onLoginForm.get('phone').touched && onLoginForm.get('phone').hasError('maxlength')\">Enter a valid phone number</p>\r\n\r\n\r\n\t\t\t\t\t<ion-item>\r\n\t\t\t\t\t\t<ion-label floating>\r\n\t\t\t\t\t\t\t<ion-icon name=\"lock\" item-start class=\"text-white\"></ion-icon>\r\n\t\t\t\t\t\t\tPassword\r\n\t\t\t\t\t\t</ion-label>\r\n\t\t\t\t\t\t<ion-input type=\"password\" formControlName=\"password\"></ion-input>\r\n\t\t\t\t\t</ion-item>\r\n\t\t\t\t\t<p ion-text color=\"danger\" class=\"text-12x has-error\" *ngIf=\"onLoginForm.get('password').touched && onLoginForm.get('password').hasError('required')\">This field is required</p>\r\n\r\n\r\n\r\n\t\t\t\t</form>\r\n\r\n\r\n\r\n\r\n<p text-right ion-text color=\"light\" tappable (click)=\"forgotPass()\">\r\n\r\n\t<strong>Forgot Password?</strong>\r\n\r\n</p>\r\n\r\n\r\n\r\n\t\t\t\t<div>\r\n\t\t\t\t\t<ion-button icon-start block color=\"success\" (click)=\"login()\" [disabled]=\"!onLoginForm.valid\">\r\n\t\t\t\t\t\t<ion-icon name=\"log-in\"></ion-icon>\r\n\t\t\t\t\t\tSIGN IN\r\n\t\t\t\t\t</ion-button>\r\n\t\t\t\t</div>\r\n\r\n\r\n\t\t\t</div>\r\n\r\n\r\n\r\n\r\n\t\t\t<div id=\"registerForm\" *ngSwitchCase=\"'register'\">\r\n\t\t\t\t\r\n\t\t\t\t<!-- Register form (click)=\"register()\" -->\r\n\r\n\t\t    <form [formGroup]=\"onRegisterForm\" class=\"list-form\">\r\n\r\n\r\n\t\t      <ion-item>\r\n\t\t        <ion-label floating>\r\n\t\t          <ion-icon name=\"person\" item-start class=\"text-white\"></ion-icon>\r\n\t\t          First Name\r\n\t\t        </ion-label>\r\n\t\t        <ion-input type=\"text\" formControlName=\"name\"></ion-input>\r\n\t\t\t\t\t</ion-item>\r\n\r\n\r\n\t\t      <p ion-text color=\"danger\" class=\"text-12x has-error\" *ngIf=\"onRegisterForm.get('name').touched && onRegisterForm.get('name').hasError('required')\">This field is required</p>\r\n\t\t\t\t\t<p ion-text color=\"danger\" class=\"text-12x has-error\" *ngIf=\"onRegisterForm.get('name').touched && onRegisterForm.get('name').hasError('minLength')\">Should have at least 4 characters</p>\r\n\r\n<!--\t\t      <ion-item>\r\n\t\t\t\t\t\t\t<ion-label floating>\r\n\t\t\t\t\t\t\t\t<ion-icon name=\"person\" item-start class=\"text-white\"></ion-icon>\r\n\t\t\t\t\t\t\t\tLast Name\r\n\t\t\t\t\t\t\t</ion-label>\r\n\t\t\t\t\t\t\t<ion-input type=\"text\" formControlName=\"lastName\"></ion-input>\r\n\t\t\t\t\t\t</ion-item>\r\n\r\n\t\t\t\t\t\t<p ion-text color=\"danger\" class=\"text-12x has-error\" *ngIf=\"onRegisterForm.get('lastName').touched && onRegisterForm.get('lastName').hasError('minlength')\">Should have at least 2 characters</p>\r\n\r\n\t\t\t\t\t\t<p ion-text color=\"danger\" class=\"text-12x has-error\" *ngIf=\"onRegisterForm.get('lastName').touched && onRegisterForm.get('lastName').hasError('required')\">This field is required</p>\r\n\r\n-->\r\n\r\n\t\t      <ion-item>\r\n\t\t        <ion-label floating>\r\n\t\t          <ion-icon name=\"mail\" item-start class=\"text-white\"></ion-icon>\r\n\t\t          Email\r\n\t\t        </ion-label>\r\n\t\t        <ion-input  type=\"email\" formControlName=\"email\"></ion-input>\r\n\t\t\t\t\t</ion-item>\r\n\r\n\t\t      <p ion-text color=\"danger\" class=\"text-12x has-error\" *ngIf=\"onRegisterForm.get('email').touched && onRegisterForm.get('email').hasError('email')\"> Invalid email</p>\r\n\r\n\t\t      <ion-item>\r\n\t\t        <ion-label floating>\r\n\t\t          <ion-icon name=\"call\" item-start class=\"text-white\"></ion-icon>\r\n\t\t          Phone\r\n\t\t        </ion-label>\r\n\t\t        <ion-input  type=\"number\" formControlName=\"phone\"></ion-input>\r\n\t\t\t\t\t</ion-item>\r\n\r\n\r\n\t\t      <p ion-text color=\"danger\" class=\"text-12x has-error\" *ngIf=\"onRegisterForm.get('phone').touched && onRegisterForm.get('phone').hasError('required')\">This field is required</p>\r\n\r\n\t\t\t\t\t<p ion-text color=\"danger\" class=\"text-12x has-error\" *ngIf=\"onRegisterForm.get('phone').touched && onRegisterForm.get('phone').hasError('minlength')\">Enter a valid phone number</p>\r\n\r\n\t\t\t\t\t<p ion-text color=\"danger\" class=\"text-12x has-error\" *ngIf=\"onRegisterForm.get('phone').touched && onRegisterForm.get('phone').hasError('maxlength')\">Enter a valid phone number</p>\r\n\r\n\r\n\r\n\r\n\t\t      <ion-item>\r\n\t\t        <ion-label floating>\r\n\t\t          <ion-icon name=\"lock\" item-start class=\"text-white\"></ion-icon>\r\n\t\t          Password\r\n\t\t        </ion-label>\r\n\t\t        <ion-input type=\"password\" formControlName=\"password\"></ion-input>\r\n\t\t\t\t\t</ion-item>\r\n\r\n\t\t\t\t\t<p ion-text color=\"danger\" class=\"text-12x has-error\" *ngIf=\"onRegisterForm.get('password').touched && onRegisterForm.get('password').hasError('minlength')\">Should have at least 6 characters</p>\r\n\r\n\t\t      <p ion-text color=\"danger\" class=\"text-12x has-error\" *ngIf=\"onRegisterForm.get('password').touched && onRegisterForm.get('password').hasError('required')\">This field is required</p>\r\n\r\n\t\t\t\t</form>\r\n\r\n\r\n\r\n\t\t    <div margin-top>\r\n\r\n\r\n\t\t      <ion-button block color=\"secondary\"  [disabled]=\"!onRegisterForm.valid\" (click)=\"register()\" >\r\n\t\t        SIGN UP\r\n\t\t\t\t</ion-button>\r\n\r\n\r\n\t\t\t\t</div>\r\n\r\n\r\n\t\t  </div>\r\n\r\n\r\n\r\n\r\n\r\n\t\t</div>\r\n\r\n\r\n\t</div>\r\n\r\n\r\n\r\n</ion-content>\r\n\r\n";
      /***/
    },

    /***/
    "./src/app/get-requests.service.ts":
    /*!*****************************************!*\
      !*** ./src/app/get-requests.service.ts ***!
      \*****************************************/

    /*! exports provided: GetRequestsService */

    /***/
    function srcAppGetRequestsServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "GetRequestsService", function () {
        return GetRequestsService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../environments/environment */
      "./src/environments/environment.ts");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/storage */
      "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js"); //let url = environment.url3;


      var url = _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].url2;
      var httpOptions3 = {
        headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
          'Content-Type': 'application/json'
        })
      };

      var GetRequestsService = /*#__PURE__*/function () {
        function GetRequestsService(router, storage, http) {
          _classCallCheck(this, GetRequestsService);

          this.router = router;
          this.storage = storage;
          this.http = http;
          this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
              'Content-Type': 'application/json'
            })
          };
        }

        _createClass(GetRequestsService, [{
          key: "clientContacts",
          value: function clientContacts() {
            return this.http.post("".concat(url, "/web/getdistinct"), {}, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          }
        }, {
          key: "sendSms",
          value: function sendSms(phone, message, recepient) {
            return this.http.post("".concat(url, "/web/sendSms"), {
              phone: phone,
              message: message,
              recepient: recepient
            }, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          }
        }, {
          key: "getByDate",
          value: function getByDate(query) {
            return this.http.post("".concat(url, "/web/getByDate"), {
              query: query
            }, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          }
        }, {
          key: "login",
          value: function login(data) {
            return this.http.post("".concat(url, "/auth/login"), data, httpOptions3).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          }
        }, {
          key: "register",
          value: function register(data) {
            return this.http.post("".concat(url, "/auth/register"), data, httpOptions3).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          }
        }, {
          key: "findAll",
          value: function findAll() {
            return this.http.get("".concat(url, "/web/allrequests"), this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          } //post

        }, {
          key: "findDevices",
          value: function findDevices(device) {
            return this.http.post("".concat(url, "/web/phoneBalance"), device, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          }
        }, {
          key: "getUssdBal",
          value: function getUssdBal() {
            return this.http.get("".concat(url, "/sms/balance"), this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          }
        }, {
          key: "mpesaBalance",
          value: function mpesaBalance(transType) {
            return this.http.post("".concat(url, "/web/mpesaBalance"), {
              type: transType
            }, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          }
        }, {
          key: "sendDist",
          value: function sendDist(req) {
            return this.http.post("".concat(url, "/web/sendDistributor"), req, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          }
        }, {
          key: "mpesaBalance3",
          value: function mpesaBalance3(transType) {
            return this.http.post("".concat(url, "/web/bcbalance"), {
              type: transType
            }, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          }
        }, {
          key: "complete",
          value: function complete(myId) {
            return this.http.post("".concat(url, "/web/markComplete"), myId, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          }
        }, {
          key: "sendMoney",
          value: function sendMoney(myId) {
            return this.http.post("".concat(url, "/web/sendMoney"), myId, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          }
        }, {
          key: "sendMoneyApproval",
          value: function sendMoneyApproval(myId) {
            return this.http.post("".concat(url, "/web/sendMoneyApproval"), myId, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          }
        }, {
          key: "sendData",
          value: function sendData(myId) {
            return this.http.post("".concat(url, "/web/sendData"), myId, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          }
        }, {
          key: "sendCredit",
          value: function sendCredit(myId) {
            return this.http.post("".concat(url, "/web/sendCredit"), myId, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          }
        }, {
          key: "findByName",
          value: function findByName(requests, searchKey) {
            var key = searchKey.toUpperCase();
            return Promise.resolve(requests.filter(function (requests) {
              return (requests.sourceID + ' ' + requests.source + ' ' + requests._id + ' ' + requests.phone + ' ' + requests.transType + ' ' + requests.state + ' ' + requests.sessionId).toUpperCase().indexOf(key) > -1;
            }));
          }
        }, {
          key: "findByNameDevices",
          value: function findByNameDevices(requests, searchKey) {
            var key = searchKey.toUpperCase();
            return Promise.resolve(requests.filter(function (requests) {
              return (requests._id + ' ' + requests.phone + ' ' + requests.name + ' ' + requests.state).toUpperCase().indexOf(key) > -1;
            }));
          }
        }, {
          key: "extractData",
          value: function extractData(res) {
            var body = res;
            return body || {};
          }
        }, {
          key: "handleError",
          value: function handleError(error) {
            var errMsg;

            if (error instanceof Response) {
              //TokenExpiredError jwt expired
              var err = error || '';
              errMsg = "".concat(error.status, " - ").concat(error.statusText || '', " ").concat(err);
            } else {
              errMsg = error.message ? error.message : error.toString();
            }

            console.error(errMsg);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(errMsg);
          }
        }]);

        return GetRequestsService;
      }();

      GetRequestsService.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"]
        }, {
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"]
        }, {
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      };

      GetRequestsService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], GetRequestsService);
      /***/
    },

    /***/
    "./src/app/login/login/login-routing.module.ts":
    /*!*****************************************************!*\
      !*** ./src/app/login/login/login-routing.module.ts ***!
      \*****************************************************/

    /*! exports provided: LoginPageRoutingModule */

    /***/
    function srcAppLoginLoginLoginRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPageRoutingModule", function () {
        return LoginPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./login.page */
      "./src/app/login/login/login.page.ts");

      var routes = [{
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"]
      }];

      var LoginPageRoutingModule = function LoginPageRoutingModule() {
        _classCallCheck(this, LoginPageRoutingModule);
      };

      LoginPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], LoginPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/login/login/login.module.ts":
    /*!*********************************************!*\
      !*** ./src/app/login/login/login.module.ts ***!
      \*********************************************/

    /*! exports provided: LoginPageModule */

    /***/
    function srcAppLoginLoginLoginModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPageModule", function () {
        return LoginPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./login-routing.module */
      "./src/app/login/login/login-routing.module.ts");
      /* harmony import */


      var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./login.page */
      "./src/app/login/login/login.page.ts");

      var LoginPageModule = function LoginPageModule() {
        _classCallCheck(this, LoginPageModule);
      };

      LoginPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _login_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPageRoutingModule"]],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
      })], LoginPageModule);
      /***/
    },

    /***/
    "./src/app/login/login/login.page.scss":
    /*!*********************************************!*\
      !*** ./src/app/login/login/login.page.scss ***!
      \*********************************************/

    /*! exports provided: default */

    /***/
    function srcAppLoginLoginLoginPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".auth .grid {\n  padding: 0;\n}\n.auth .grid .col {\n  padding: 0;\n}\n.auth .btn-group .button-ios,\n.auth .btn-group .button-md {\n  border-radius: 0;\n}\n.auth .btn-group .button-ios:first-of-type,\n.auth .btn-group .button-md:first-of-type {\n  border-radius: 2px 0 0 2px;\n}\n.auth .btn-group .button-ios:last-of-type,\n.auth .btn-group .button-md:last-of-type {\n  border-radius: 0 2px 2px 0;\n}\n.auth .scroll-content {\n  display: flex;\n  align-items: baseline;\n  justify-content: center;\n}\n.auth .scroll-content .auth-content {\n  width: 80%;\n}\n.auth .scroll-content .auth-content .logo {\n  width: 80px;\n  height: 80px;\n  margin: 40px auto 0;\n}\n.auth .scroll-content .auth-content .list-form {\n  padding: 0;\n  margin-bottom: 0;\n}\n.auth .scroll-content .auth-content .list-form ion-item {\n  background: none;\n  padding: 0;\n}\n.auth .scroll-content .auth-content .list-form ion-item:first-child {\n  border-top: none;\n}\n.auth .scroll-content .auth-content .list-form ion-item:after {\n  display: none;\n}\n.auth .scroll-content .auth-content .list-form ion-item.item-md.item-block .item-inner,\n.auth .scroll-content .auth-content .list-form ion-item .item-ios.item-block .item-inner {\n  border-bottom-color: #ffd1d7 !important;\n}\n.auth .scroll-content .auth-content .list-form ion-item ion-label {\n  font-size: 1em;\n  color: #ffd1d7;\n}\n.auth .scroll-content .auth-content .list-form ion-item ion-label.label-ios, .auth .scroll-content .auth-content .list-form ion-item ion-label.label-md {\n  margin-top: 0;\n}\n.auth .scroll-content .auth-content .list-form ion-item input {\n  color: #ffd1d7;\n}\n@media (min-width: 768px) {\n  page-auth .scroll-content .auth-content,\n.auth .scroll-content .auth-content {\n    width: 40%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naW4vbG9naW4vbG9naW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUdFO0VBQ0UsVUFBQTtBQUZKO0FBR0k7RUFDRSxVQUFBO0FBRE47QUFNSTs7RUFFRSxnQkFBQTtBQUpOO0FBS007O0VBQ0UsMEJBQUE7QUFGUjtBQUlNOztFQUNFLDBCQUFBO0FBRFI7QUFNRTtFQUNFLGFBQUE7RUFDQSxxQkFBQTtFQUNBLHVCQUFBO0FBSko7QUFNSTtFQUNFLFVBQUE7QUFKTjtBQU1NO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQUpSO0FBVU07RUFDRSxVQUFBO0VBQ0EsZ0JBQUE7QUFSUjtBQVVRO0VBQ0UsZ0JBQUE7RUFDQSxVQUFBO0FBUlY7QUFVVTtFQUNFLGdCQUFBO0FBUlo7QUFXVTtFQUNFLGFBQUE7QUFUWjtBQWVPOztFQUNDLHVDQUFBO0FBWlI7QUFpQlU7RUFDRSxjQUFBO0VBQ0EsY0FBQTtBQWZaO0FBZ0JZO0VBRUMsYUFBQTtBQWZiO0FBbUJVO0VBQ0UsY0FBQTtBQWpCWjtBQXlCQTtFQUtLOztJQUNFLFVBQUE7RUF6Qkw7QUFDRiIsImZpbGUiOiJzcmMvYXBwL2xvZ2luL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5cclxuLmF1dGgge1xyXG4gIC5ncmlkIHtcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgICAuY29sIHtcclxuICAgICAgcGFkZGluZzogMDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5idG4tZ3JvdXAge1xyXG4gICAgLmJ1dHRvbi1pb3MsXHJcbiAgICAuYnV0dG9uLW1kIHtcclxuICAgICAgYm9yZGVyLXJhZGl1czogMDtcclxuICAgICAgJjpmaXJzdC1vZi10eXBlIHtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAycHggMCAwIDJweDtcclxuICAgICAgfVxyXG4gICAgICAmOmxhc3Qtb2YtdHlwZSB7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMCAycHggMnB4IDA7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5zY3JvbGwtY29udGVudCB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGJhc2VsaW5lO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcblxyXG4gICAgLmF1dGgtY29udGVudCB7XHJcbiAgICAgIHdpZHRoOiA4MCU7XHJcblxyXG4gICAgICAubG9nbyB7XHJcbiAgICAgICAgd2lkdGg6IDgwcHg7XHJcbiAgICAgICAgaGVpZ2h0OiA4MHB4O1xyXG4gICAgICAgIG1hcmdpbjogNDBweCBhdXRvIDA7XHJcbiAgICAgICAgLy8gYm9yZGVyLXJhZGl1czogMTAlO1xyXG4gICAgICAgIC8vIGJhY2tncm91bmQ6IHVybChcIi4uL2Fzc2V0cy9pbWcvaW9uYm9va2luZy1pY28ucG5nXCIpIG5vLXJlcGVhdDtcclxuICAgICAgICAvLyBiYWNrZ3JvdW5kLXNpemU6IDEwMCU7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC5saXN0LWZvcm0ge1xyXG4gICAgICAgIHBhZGRpbmc6IDA7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcclxuXHJcbiAgICAgICAgaW9uLWl0ZW0ge1xyXG4gICAgICAgICAgYmFja2dyb3VuZDogbm9uZTtcclxuICAgICAgICAgIHBhZGRpbmc6IDA7XHJcblxyXG4gICAgICAgICAgJjpmaXJzdC1jaGlsZCB7XHJcbiAgICAgICAgICAgIGJvcmRlci10b3A6IG5vbmU7XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgJjphZnRlciB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICAgICAgICB9XHJcblxyXG5cdFx0XHRcdFx0Ji5pdGVtLW1kLFxyXG5cdFx0XHRcdFx0Lml0ZW0taW9zIHtcclxuXHRcdFx0XHRcdFx0Ji5pdGVtLWJsb2NrIHtcclxuXHRcdFx0XHRcdFx0XHQuaXRlbS1pbm5lciB7XHJcblx0XHRcdFx0XHRcdFx0XHRib3JkZXItYm90dG9tLWNvbG9yOiAjZmZkMWQ3ICFpbXBvcnRhbnQ7XHJcblx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR9XHJcblxyXG4gICAgICAgICAgaW9uLWxhYmVsIHtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxZW07XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZkMWQ3O1xyXG4gICAgICAgICAgICAmLmxhYmVsLWlvcyxcclxuICAgICAgICAgICAgJi5sYWJlbC1tZCB7XHJcbiAgICAgICAgICAgIFx0bWFyZ2luLXRvcDogMDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIGlucHV0IHtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmQxZDc7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIHtcclxuXHJcblx0cGFnZS1hdXRoLFxyXG5cdC5hdXRoIHtcclxuXHQgIC5zY3JvbGwtY29udGVudCB7XHJcblx0ICAgIC5hdXRoLWNvbnRlbnQge1xyXG5cdCAgICAgIHdpZHRoOiA0MCU7XHJcblx0ICAgIH1cclxuXHQgIH1cclxuXHR9XHJcblxyXG59XHJcbiJdfQ== */";
      /***/
    },

    /***/
    "./src/app/login/login/login.page.ts":
    /*!*******************************************!*\
      !*** ./src/app/login/login/login.page.ts ***!
      \*******************************************/

    /*! exports provided: LoginPage */

    /***/
    function srcAppLoginLoginLoginPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPage", function () {
        return LoginPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/storage */
      "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
      /* harmony import */


      var _get_requests_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../../get-requests.service */
      "./src/app/get-requests.service.ts");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

      var LoginPage = /*#__PURE__*/function () {
        function LoginPage(storage, router, _fb, nav, forgotCtrl, menu, toastCtrl, loadingController, authService) {
          _classCallCheck(this, LoginPage);

          this.storage = storage;
          this.router = router;
          this._fb = _fb;
          this.nav = nav;
          this.forgotCtrl = forgotCtrl;
          this.menu = menu;
          this.toastCtrl = toastCtrl;
          this.loadingController = loadingController;
          this.authService = authService;
          this.auth = "login"; // this.menu.swipeEnable(false);

          this.menu.enable(false);
        }

        _createClass(LoginPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            localStorage.clear();
            this.onLoginForm = this._fb.group({
              phone: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(10), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(13)])],
              password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required])]
            });
            this.onRegisterForm = this._fb.group({
              name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(4)])],
              email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].email])],
              phone: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(10), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(10)])],
              password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(6)])]
            });
          }
        }, {
          key: "register",
          value: function register() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var _this = this;

              var loading;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      console.log(this.onRegisterForm.value);
                      _context.next = 3;
                      return this.loadingController.create({
                        cssClass: 'my-custom-class',
                        message: "Registering User...",
                        spinner: "crescent"
                      });

                    case 3:
                      loading = _context.sent;
                      _context.next = 6;
                      return loading.present();

                    case 6:
                      this.authService.register(JSON.stringify(this.onRegisterForm.value)).subscribe(function (data) {
                        console.log(data);

                        if (data.success) {
                          loading.dismiss();

                          _this.presentToast("User was added successfully", "bottom");

                          _this.router.navigate(['/login']);
                        } else {
                          _this.presentToast("User registration error", "bottom");

                          loading.dismiss();

                          _this.router.navigate(['/login']);

                          _this.presentToast(data.msg, "bottom");
                        }
                      }, function (error) {
                        loading.dismiss();

                        _this.presentToast("An error occurred please try again", "bottom");

                        console.log(error);
                      });

                    case 7:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          } // login and go to home page

        }, {
          key: "login",
          value: function login() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _this2 = this;

              var loading, sub;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      console.log(this.onLoginForm.value);
                      _context2.next = 3;
                      return this.loadingController.create({
                        cssClass: 'my-custom-class',
                        message: 'Please wait...',
                        spinner: "crescent"
                      });

                    case 3:
                      loading = _context2.sent;
                      _context2.next = 6;
                      return loading.present();

                    case 6:
                      /*
                      const { role, data } = await loading.onDidDismiss();
                      console.log('Loading dismissed!');
                      */
                      sub = this.authService.login(JSON.stringify(this.onLoginForm.value)).subscribe(function (data) {
                        console.log(data);

                        if (data.success) {
                          var prof = {
                            profile: data.result
                          }; //API TO GET AUTH

                          if (prof) {
                            _this2.storage.set('profile', prof);

                            _this2.storage.set('token', data.token.split(" ")[1]);

                            console.log("token is....", data.token);
                            localStorage.setItem("token", data.token.split(" ")[1]);
                            /*
                              this.authService.httpOptions = {
                                headers: new HttpHeaders({ 'Content-Type': 'application/json', 'amness':data })
                              };*/

                            _this2.storage.set('authenticated', true).then(function (data) {
                              //   const path = window.location.pathname.split('dash/')[1];
                              _this2.router.navigate(['/dash/requests']);
                            });

                            loading.dismiss();
                            sub.unsubscribe(); //	this.nav.setRoot('page-restaurant-list', {"data": prof, "from":"loginAuth", "auth": true});
                          }
                        } else {
                          loading.dismiss();

                          _this2.storage.set('profile', null);

                          _this2.storage.set('authenticated', false);

                          _this2.storage.set('token', null);

                          _this2.presentToast("Incorrect phone number and or password", "middle");

                          sub.unsubscribe();
                        }
                      }, function (err) {
                        console.log(err);
                        loading.dismiss();

                        _this2.presentToast("An Error ocurred please try again", "middle");
                      });

                    case 7:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "forgotPass",
          value: function forgotPass() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3);
            }));
          }
        }, {
          key: "presentToast",
          value: function presentToast(message, pos) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var toast;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      toast = this.toastCtrl.create({
                        message: message,
                        duration: 4000,
                        position: pos
                      });
                      _context4.next = 3;
                      return toast;

                    case 3:
                      _context4.sent.present;

                    case 4:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }]);

        return LoginPage;
      }();

      LoginPage.ctorParameters = function () {
        return [{
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["NavController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["AlertController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["MenuController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ToastController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["LoadingController"]
        }, {
          type: _get_requests_service__WEBPACK_IMPORTED_MODULE_5__["GetRequestsService"]
        }];
      };

      LoginPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-login',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./login.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login/login.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./login.page.scss */
        "./src/app/login/login/login.page.scss"))["default"]]
      })], LoginPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=login-login-login-module-es5.js.map