(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["user-zone-user-zone-module"], {
    /***/
    "./src/app/get-requests.service.ts":
    /*!*****************************************!*\
      !*** ./src/app/get-requests.service.ts ***!
      \*****************************************/

    /*! exports provided: GetRequestsService */

    /***/
    function srcAppGetRequestsServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "GetRequestsService", function () {
        return GetRequestsService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../environments/environment */
      "./src/environments/environment.ts");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/storage */
      "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js"); //let url = environment.url3;


      var url = _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].url2;
      var httpOptions3 = {
        headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
          'Content-Type': 'application/json'
        })
      };

      var GetRequestsService = /*#__PURE__*/function () {
        function GetRequestsService(router, storage, http) {
          _classCallCheck(this, GetRequestsService);

          this.router = router;
          this.storage = storage;
          this.http = http;
          this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
              'Content-Type': 'application/json'
            })
          };
        }

        _createClass(GetRequestsService, [{
          key: "clientContacts",
          value: function clientContacts() {
            return this.http.post("".concat(url, "/web/getdistinct"), {}, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          }
        }, {
          key: "sendSms",
          value: function sendSms(phone, message, recepient) {
            return this.http.post("".concat(url, "/web/sendSms"), {
              phone: phone,
              message: message,
              recepient: recepient
            }, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          }
        }, {
          key: "getByDate",
          value: function getByDate(query) {
            return this.http.post("".concat(url, "/web/getByDate"), {
              query: query
            }, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          }
        }, {
          key: "login",
          value: function login(data) {
            return this.http.post("".concat(url, "/auth/login"), data, httpOptions3).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          }
        }, {
          key: "register",
          value: function register(data) {
            return this.http.post("".concat(url, "/auth/register"), data, httpOptions3).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          }
        }, {
          key: "findAll",
          value: function findAll() {
            return this.http.get("".concat(url, "/web/allrequests"), this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          } //post

        }, {
          key: "findDevices",
          value: function findDevices(device) {
            return this.http.post("".concat(url, "/web/phoneBalance"), device, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          }
        }, {
          key: "getUssdBal",
          value: function getUssdBal() {
            return this.http.get("".concat(url, "/sms/balance"), this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          }
        }, {
          key: "mpesaBalance",
          value: function mpesaBalance(transType) {
            return this.http.post("".concat(url, "/web/mpesaBalance"), {
              type: transType
            }, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          }
        }, {
          key: "sendDist",
          value: function sendDist(req) {
            return this.http.post("".concat(url, "/web/sendDistributor"), req, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          }
        }, {
          key: "mpesaBalance3",
          value: function mpesaBalance3(transType) {
            return this.http.post("".concat(url, "/web/bcbalance"), {
              type: transType
            }, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          }
        }, {
          key: "complete",
          value: function complete(myId) {
            return this.http.post("".concat(url, "/web/markComplete"), myId, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          }
        }, {
          key: "sendMoney",
          value: function sendMoney(myId) {
            return this.http.post("".concat(url, "/web/sendMoney"), myId, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          }
        }, {
          key: "sendMoneyApproval",
          value: function sendMoneyApproval(myId) {
            return this.http.post("".concat(url, "/web/sendMoneyApproval"), myId, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          }
        }, {
          key: "sendData",
          value: function sendData(myId) {
            return this.http.post("".concat(url, "/web/sendData"), myId, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          }
        }, {
          key: "sendCredit",
          value: function sendCredit(myId) {
            return this.http.post("".concat(url, "/web/sendCredit"), myId, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
          }
        }, {
          key: "findByName",
          value: function findByName(requests, searchKey) {
            var key = searchKey.toUpperCase();
            return Promise.resolve(requests.filter(function (requests) {
              return (requests.sourceID + ' ' + requests.source + ' ' + requests._id + ' ' + requests.phone + ' ' + requests.transType + ' ' + requests.state + ' ' + requests.sessionId).toUpperCase().indexOf(key) > -1;
            }));
          }
        }, {
          key: "findByNameDevices",
          value: function findByNameDevices(requests, searchKey) {
            var key = searchKey.toUpperCase();
            return Promise.resolve(requests.filter(function (requests) {
              return (requests._id + ' ' + requests.phone + ' ' + requests.name + ' ' + requests.state).toUpperCase().indexOf(key) > -1;
            }));
          }
        }, {
          key: "extractData",
          value: function extractData(res) {
            var body = res;
            return body || {};
          }
        }, {
          key: "handleError",
          value: function handleError(error) {
            var errMsg;

            if (error instanceof Response) {
              //TokenExpiredError jwt expired
              var err = error || '';
              errMsg = "".concat(error.status, " - ").concat(error.statusText || '', " ").concat(err);
            } else {
              errMsg = error.message ? error.message : error.toString();
            }

            console.error(errMsg);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(errMsg);
          }
        }]);

        return GetRequestsService;
      }();

      GetRequestsService.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"]
        }, {
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"]
        }, {
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      };

      GetRequestsService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], GetRequestsService);
      /***/
    },

    /***/
    "./src/app/user-zone/user-zone-routing.module.ts":
    /*!*******************************************************!*\
      !*** ./src/app/user-zone/user-zone-routing.module.ts ***!
      \*******************************************************/

    /*! exports provided: UserZonePageRoutingModule */

    /***/
    function srcAppUserZoneUserZoneRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UserZonePageRoutingModule", function () {
        return UserZonePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _user_zone_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./user-zone.page */
      "./src/app/user-zone/user-zone.page.ts");

      var routes = [{
        path: '',
        component: _user_zone_page__WEBPACK_IMPORTED_MODULE_3__["UserZonePage"]
      }];

      var UserZonePageRoutingModule = function UserZonePageRoutingModule() {
        _classCallCheck(this, UserZonePageRoutingModule);
      };

      UserZonePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], UserZonePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/user-zone/user-zone.module.ts":
    /*!***********************************************!*\
      !*** ./src/app/user-zone/user-zone.module.ts ***!
      \***********************************************/

    /*! exports provided: UserZonePageModule */

    /***/
    function srcAppUserZoneUserZoneModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UserZonePageModule", function () {
        return UserZonePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _user_zone_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./user-zone-routing.module */
      "./src/app/user-zone/user-zone-routing.module.ts");
      /* harmony import */


      var _user_zone_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./user-zone.page */
      "./src/app/user-zone/user-zone.page.ts");

      var UserZonePageModule = function UserZonePageModule() {
        _classCallCheck(this, UserZonePageModule);
      };

      UserZonePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _user_zone_routing_module__WEBPACK_IMPORTED_MODULE_5__["UserZonePageRoutingModule"]],
        declarations: [_user_zone_page__WEBPACK_IMPORTED_MODULE_6__["UserZonePage"]]
      })], UserZonePageModule);
      /***/
    }
  }]);
})();
//# sourceMappingURL=user-zone-user-zone-module-es5.js.map