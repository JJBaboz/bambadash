(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["user-zone-user-zone-module"],{

/***/ "./src/app/get-requests.service.ts":
/*!*****************************************!*\
  !*** ./src/app/get-requests.service.ts ***!
  \*****************************************/
/*! exports provided: GetRequestsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetRequestsService", function() { return GetRequestsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");








//let url = environment.url3;
let url = _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].url2;
let httpOptions3 = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/json' })
};
let GetRequestsService = class GetRequestsService {
    constructor(router, storage, http) {
        this.router = router;
        this.storage = storage;
        this.http = http;
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/json' })
        };
    }
    clientContacts() {
        return this.http.post(`${url}/web/getdistinct`, {}, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    sendSms(phone, message, recepient) {
        return this.http.post(`${url}/web/sendSms`, { phone: phone, message: message, recepient: recepient }, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    getByDate(query) {
        return this.http.post(`${url}/web/getByDate`, { query: query }, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    login(data) {
        return this.http.post(`${url}/auth/login`, data, httpOptions3).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    register(data) {
        return this.http.post(`${url}/auth/register`, data, httpOptions3).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    findAll() {
        return this.http.get(`${url}/web/allrequests`, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    //post
    findDevices(device) {
        return this.http.post(`${url}/web/phoneBalance`, device, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    getUssdBal() {
        return this.http.get(`${url}/sms/balance`, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    mpesaBalance(transType) {
        return this.http.post(`${url}/web/mpesaBalance`, { type: transType }, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    sendDist(req) {
        return this.http.post(`${url}/web/sendDistributor`, req, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    mpesaBalance3(transType) {
        return this.http.post(`${url}/web/bcbalance`, { type: transType }, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    complete(myId) {
        return this.http.post(`${url}/web/markComplete`, myId, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    sendMoney(myId) {
        return this.http.post(`${url}/web/sendMoney`, myId, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    sendMoneyApproval(myId) {
        return this.http.post(`${url}/web/sendMoneyApproval`, myId, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    sendData(myId) {
        return this.http.post(`${url}/web/sendData`, myId, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    sendCredit(myId) {
        return this.http.post(`${url}/web/sendCredit`, myId, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    }
    findByName(requests, searchKey) {
        let key = searchKey.toUpperCase();
        return Promise.resolve(requests.filter((requests) => (requests.sourceID + ' ' + requests.source + ' ' + requests._id + ' ' + requests.phone + ' ' + requests.transType + ' ' + requests.state + ' ' + requests.sessionId).toUpperCase().indexOf(key) > -1));
    }
    findByNameDevices(requests, searchKey) {
        let key = searchKey.toUpperCase();
        return Promise.resolve(requests.filter((requests) => (requests._id + ' ' + requests.phone + ' ' + requests.name + ' ' + requests.state).toUpperCase().indexOf(key) > -1));
    }
    extractData(res) {
        let body = res;
        return body || {};
    }
    handleError(error) {
        let errMsg;
        if (error instanceof Response) { //TokenExpiredError jwt expired
            const err = error || '';
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(errMsg);
    }
};
GetRequestsService.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
GetRequestsService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], GetRequestsService);



/***/ }),

/***/ "./src/app/user-zone/user-zone-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/user-zone/user-zone-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: UserZonePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserZonePageRoutingModule", function() { return UserZonePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _user_zone_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./user-zone.page */ "./src/app/user-zone/user-zone.page.ts");




const routes = [
    {
        path: '',
        component: _user_zone_page__WEBPACK_IMPORTED_MODULE_3__["UserZonePage"]
    }
];
let UserZonePageRoutingModule = class UserZonePageRoutingModule {
};
UserZonePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], UserZonePageRoutingModule);



/***/ }),

/***/ "./src/app/user-zone/user-zone.module.ts":
/*!***********************************************!*\
  !*** ./src/app/user-zone/user-zone.module.ts ***!
  \***********************************************/
/*! exports provided: UserZonePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserZonePageModule", function() { return UserZonePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _user_zone_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./user-zone-routing.module */ "./src/app/user-zone/user-zone-routing.module.ts");
/* harmony import */ var _user_zone_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./user-zone.page */ "./src/app/user-zone/user-zone.page.ts");







let UserZonePageModule = class UserZonePageModule {
};
UserZonePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _user_zone_routing_module__WEBPACK_IMPORTED_MODULE_5__["UserZonePageRoutingModule"],
        ],
        declarations: [_user_zone_page__WEBPACK_IMPORTED_MODULE_6__["UserZonePage"]]
    })
], UserZonePageModule);



/***/ })

}]);
//# sourceMappingURL=user-zone-user-zone-module-es2015.js.map