(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["folder-folder-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app-adds/app-adds.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app-adds/app-adds.component.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\">\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n\n    <ion-title size=\"large\">App Data</ion-title>\n \n   \n  \n  </ion-toolbar>\n \n\n</ion-header>\n\n<ion-content>\n\n\n\n\n\n  <ion-list>\n   \n    <ion-item>\n         \n      \n      <ion-button (click)=\"presentNewModal(null)\"   slot=\"end\">\n        Add Add\n      </ion-button>\n\n\n    </ion-item>\n\n  </ion-list>\n\n<ion-list *ngFor=\"let data of products\">\n\n\n    <ion-card >\n\n      <ion-card-header >\n\n        <ion-item lines=\"none\" >\n          <ion-thumbnail slot=\"start\">\n            <img [src]=\"data.image? data.image: 'assets/img/water-drop1.png' \">\n          </ion-thumbnail>\n\n\n\n          <ion-label>\n            <ion-text>\n              Name: {{data.customName}}\n            </ion-text>\n\n\n            <div>\n              <p>\n                Description: {{data.description}}\n              </p>\n            </div>\n           \n\n          </ion-label >\n\n  \n\n        </ion-item>\n      \n      </ion-card-header>\n      \n\n\n      <ion-card-content class=\"ion-no-padding\">\n\n        <ion-item lines=\"none\">\n\n      \n          \n          <ion-button slot=\"end\"  (click)=\"presentEditModal(data)\" >\n            Edit Product\n          </ion-button>\n          </ion-item>\n\n      </ion-card-content>\n\n    </ion-card>\n\n\n\n  </ion-list>\n\n\n  \n\n\n</ion-content>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/charts/charts.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/charts/charts.component.html ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\">\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n\n  </ion-toolbar>\n \n\n</ion-header>\n\n<ion-content >\n\n\n  \n<ion-grid>\n  <ion-row>\n\n    <ion-item>\n      <iframe style=\"background: #21313C;border: none;border-radius: 2px;box-shadow: 0 2px 10px 0 rgba(70, 76, 79, .2);\" width=\"640\" height=\"480\" src=\"https://charts.mongodb.com/charts-project-0-osdea/embed/charts?id=56f56524-486d-4a71-ac4b-8e88505b27db&autoRefresh=300&theme=dark\"></iframe>\n    </ion-item>\n\n\n    </ion-row>\n\n\n    <ion-row>\n\n      <ion-item>\n        <iframe style=\"background: #FFFFFF;border: none;border-radius: 2px;box-shadow: 0 2px 10px 0 rgba(70, 76, 79, .2);\" width=\"640\" height=\"480\" src=\"https://charts.mongodb.com/charts-project-0-osdea/embed/charts?id=ad11799e-4a0d-4072-bd8a-5fa26dd7a632&autoRefresh=14400&theme=light\"></iframe>  \n      </ion-item>\n      </ion-row>\n\n\n      <ion-row>\n\n        <ion-item>\n          <iframe style=\"background: #21313C;border: none;border-radius: 2px;box-shadow: 0 2px 10px 0 rgba(70, 76, 79, .2);\" width=\"640\" height=\"480\" src=\"https://charts.mongodb.com/charts-project-0-osdea/embed/charts?id=be886e8a-7e10-48f0-a8f3-912a7732a9f1&autoRefresh=28800&theme=dark\"></iframe>    \n        </ion-item>\n    \n    \n        </ion-row>\n\n\n        <ion-row>\n\n          <ion-item>\n            <iframe style=\"background: #FFFFFF;border: none;border-radius: 2px;box-shadow: 0 2px 10px 0 rgba(70, 76, 79, .2);\" width=\"640\" height=\"480\" src=\"https://charts.mongodb.com/charts-project-0-osdea/embed/charts?id=c24ddee6-7a6f-4096-bd24-921c8a79ef3b&autoRefresh=14400&theme=light\"></iframe>  \n          </ion-item>\n          </ion-row>\n\n        <ion-row>\n\n          <ion-item>\n\n<iframe style=\"background: #21313C;border: none;border-radius: 2px;box-shadow: 0 2px 10px 0 rgba(70, 76, 79, .2);\" width=\"1000\" height=\"480\" src=\"https://charts.mongodb.com/charts-project-0-osdea/embed/charts?id=05708b6f-88f3-4e9f-b12d-3b90555cac65&autoRefresh=1800&theme=dark\"></iframe>         \n         \n          </ion-item>\n      \n          </ion-row>\n\n\n    </ion-grid>\n\n  </ion-content>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/edit-app-data/edit-app-data.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/edit-app-data/edit-app-data.component.html ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n\n\n<ion-content>\n\n\n  <ion-card  >\n    <ion-card-header class=\"ion-text-center\">\n\n      <ion-item>\n\n        <ion-button slot=\"end\" color=\"primary\" (click)=\"dismiss()\" >\n          Close\n          </ion-button>\n\n      </ion-item>\n\n      <ion-progress-bar ion-padding color=\"primary\" [value]=\"per\"></ion-progress-bar>\n\n      <div  style=\"  width:100%; height:100%\"> \n         <img height=50% width= 50%;  [src]=\"myUrl? myUrl:'assets/img/bambaswap.jpg'\"  />\n        \n         \n  <ion-button *ngIf=\"!isFileUploading && !isFileUploaded\" slot=\"start\" expand=\"full\" color=\"success\" >\n    \n    <span>Upload Image</span>\n    \n  \n    <input id=\"uploadBtn\" type=\"file\" class=\"upload\" (change)=\"uploadImage($event.target.files)\"/>\n  </ion-button>\n\n    \n    </div>\n\n    <ion-card-title >\n      Image Name: {{name}}\n      </ion-card-title>\n    \n    \n          <ion-text color=\"success\">\n            <p>Description: {{description}}</p>\n          </ion-text>\n      \n    </ion-card-header>\n\n    <ion-card-content>\n\n    \n      <ion-item >\n        <ion-label position=\"floating\" >Name</ion-label>\n\n        <ion-input [(ngModel)]=\"name\"></ion-input>\n      </ion-item>\n\n      <ion-item >\n        <ion-label position=\"floating\"  >Description</ion-label>\n\n        <ion-input [(ngModel)]=\"description\"></ion-input>\n      </ion-item>\n\n    \n\n\n      \n\n<ion-item lines=\"none\">\n\n  <ion-button *ngIf=\"!isFileUploading && isFileUploaded\" slot=\"start\" expand=\"full\" color=\"danger\"\n   (click)=\"delete()\">Re-upload Image</ion-button>\n\n  <ion-button *ngIf=\"mode === 'edit' \" [disabled]=\" !isFileUploaded || name === '' || description ==='' \" color=\"danger\"\n \n  slot=\"start\" (click)=\"deleteProduct()\" >\n    Delete Product\n  </ion-button>\n\n  <ion-button *ngIf=\"mode === 'edit' \" [disabled]=\" !isFileUploaded || name === '' || description ===''  \" slot = \"end\" expand=\"full\"\n  \n  color=\"success\" (click)=\"updateProduct()\" >Update App-Add</ion-button>\n    \n\n  <ion-button *ngIf=\"mode === 'new' \" [disabled]=\" !isFileUploaded || name === '' || description ==='' \" slot = \"end\" expand=\"full\"\n  \n  color=\"success\" (click)=\"storeImageData()\">Save App-Add</ion-button>\n\n</ion-item>\n\n\n    \n      \n     \n    </ion-card-content>\n  </ion-card>\n\n\n\n\n</ion-content>\n  \n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/finance/finance/finance.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/finance/finance/finance.component.html ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\n<ion-header [translucent]=\"true\">\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-menu-button></ion-menu-button>\r\n    </ion-buttons>\r\n\r\n    \r\n    <ion-searchbar [(ngModel)]=\"searchKey\" animated  (ionInput)=\"onInput($event)\" (ionCancel)=\"onCancel($event)\"></ion-searchbar>\r\n  \r\n\r\n  </ion-toolbar>\r\n \r\n\r\n</ion-header>\r\n\r\n\r\n<ion-content>\r\n\r\n  \r\n\r\n\r\n<ion-grid>\r\n  \r\n\r\n  <ion-list>\r\n \r\n\r\n    <ion-item-divider>\r\n      \r\n  \r\n      <ion-row >\r\n    \r\n     \r\n    \r\n      <ion-col size=\"auto\" class=\"ion-align-self-center\" >\r\n        <ion-button (click)=\"getMpesabalance()\" fill=\"outline\">MPESA Utility Balance: KES {{mpesaBalance3}}</ion-button>\r\n    \r\n      </ion-col>\r\n\r\n      <ion-col size=\"auto\" class=\"ion-align-self-center\" >\r\n        <ion-button (click)=\"getMpesabalance()\" fill=\"outline\">MPESA Working Balance : KES {{mpesaBalance2}}</ion-button>\r\n    \r\n      </ion-col>\r\n    \r\n\r\n      <ion-col size=\"auto\" class=\"ion-align-self-center\" >\r\n        <ion-button (click)=\"africabalance()\" fill=\"outline\">USSD/SMS Balance : KES {{smsBalance}}</ion-button>\r\n    \r\n      </ion-col>\r\n  \r\n    <!--\r\n <ion-col size=\"12\" class=\"ion-align-self-center\" >\r\n    <ion-button (click)=\"getMpesabalance()\" fill=\"outline\">Utility Account : {{mpesaBalance}}</ion-button>\r\n\r\n  </ion-col>\r\n\r\n\r\n-->\r\n     \r\n      </ion-row>\r\n    \r\n    </ion-item-divider>\r\n    \r\n    \r\n\r\n\r\n\r\n<ion-item-divider>\r\n      \r\n    \r\n  <ion-row *ngIf=\" user == 'babzgeo27@gmail.com'? false:true\" >\r\n\r\n  <ion-col size=\"auto\" *ngFor=\"let dev of devices\"  class=\"ion-align-self-start\" >\r\n\r\n    <!---->\r\n\r\n    <ion-button  fill=\"outline\" [color]=\"dev.state === 'online'? 'success':'danger'\">{{dev.name}} ({{dev.phone}}): KES {{dev.balance}} -- {{dev.state === 'online'? 'online':'offline'}} </ion-button>\r\n\r\n  </ion-col>\r\n\r\n  </ion-row>\r\n\r\n  \r\n  <ion-row *ngIf=\" user == 'babzgeo27@gmail.com'? true:false\" >\r\n\r\n    <ion-col size=\"auto\" *ngFor=\"let dev of devices\"  class=\"ion-align-self-start\" >\r\n  \r\n      <!--({{dev.phone}})   (click)=\"getCreditBalance(dev)\"  -->\r\n  \r\n      <ion-button  (click)=\"getCreditBalance(dev)\" fill=\"outline\" [color]=\"dev.state === 'online'? 'success':'danger'\">{{dev.name}} ({{dev.phone}}): KES {{dev.balance}} -- {{dev.state === 'online'? 'online':'offline'}} -- {{dev.usage}}</ion-button>\r\n  \r\n    </ion-col>\r\n    \r\n    </ion-row>\r\n\r\n\r\n</ion-item-divider>\r\n\r\n\r\n<ion-row >\r\n  <ion-col >\r\n\r\n    <ion-button (click)=\"getCreditBalance({name:null})\"  fill=\"outline\" color='success'>Data-Airtime Exchanger  {{dispersable}} : {{totalAirtime}} --ONLINE </ion-button>\r\n  </ion-col>\r\n</ion-row>\r\n\r\n\r\n\r\n<ion-item-divider >\r\n\r\n  <ion-row  >\r\n   \r\n    <ion-col size=\"12\" class=\"ion-align-self-center\" >\r\n\r\n      <ion-card >\r\n\r\n        <ion-card-header>\r\n          <ion-card-subtitle>This will prompt the buyer via mpesa pop-up menu</ion-card-subtitle>\r\n          <ion-card-title>Send Airtime to Distributor</ion-card-title>\r\n        </ion-card-header>\r\n        \r\n      \r\n        <ion-card-content>\r\n          <ion-item>\r\n            <ion-label>My Devices</ion-label>\r\n        \r\n          <ion-select [(ngModel)]=\"myDevice\" placeholder=\"Select One\" okText=\"Okay\" cancelText=\"Dismiss\">\r\n          \r\n              <ion-select-option *ngFor=\"let device of devices\" [value]=\"device.phone\">{{device.name}}--{{device.phone}}</ion-select-option>\r\n        \r\n            </ion-select>\r\n          </ion-item>\r\n\r\n          <ion-item>\r\n            <ion-label position=\"floating\">Buyer number </ion-label>\r\n            <ion-input [(ngModel)]=\"clientPhone\" clearInput placeholder=\"07...\" ></ion-input>\r\n          </ion-item>\r\n\r\n\r\n          <ion-item>\r\n            <ion-label position=\"floating\">Amount </ion-label>\r\n            <ion-input [(ngModel)]=\"Amount\" clearInput placeholder=\"Kes\" ></ion-input>\r\n          </ion-item>\r\n\r\n\r\n        </ion-card-content>\r\n\r\n\r\n        <ion-item *ngIf=\" user == 'babzgeo27@gmail.com'? true:user == 'mutie.michael@gmail.com'? true:user == 'gmngari1@gmail.com'? true:false  \">\r\n          <ion-button [disabled]=\" clientPhone != '' && Amount != null && myDevice != '' ? false:true   \" (click)=\"sendAirtime()\" expand=\"block\" fill=\"outline\" >Send</ion-button>\r\n        </ion-item>\r\n\r\n      </ion-card>\r\n      \r\n    </ion-col>\r\n  \r\n  \r\n    </ion-row>\r\n  </ion-item-divider>\r\n\r\n\r\n\r\n</ion-list>\r\n\r\n\r\n<ion-list *ngIf=\" user == 'babzgeo27@gmail.com'? true:user == 'mutie.michael@gmail.com'? true:user == 'gmngari1@gmail.com'? true:user == 'muli@bambaswap.com'? true:false  \" >\r\n\r\n\r\n  <ion-title size=\"large\">Breakdown</ion-title>\r\n\r\n<ion-item-divider>\r\n<ion-row>\r\n  <ion-col>\r\n  <mat-form-field appearance=\"fill\">\r\n    <mat-label>Enter a date range</mat-label>\r\n    <mat-date-range-input [formGroup]=\"range\" [rangePicker]=\"picker\">\r\n      <input [min]=\"minDate\" [max]=\"maxDate\"   matStartDate formControlName=\"start\" placeholder=\"Start date\">\r\n      <input  [min]=\"minDate\" [max]=\"maxDate\"  matEndDate formControlName=\"end\" placeholder=\"End date\">\r\n    </mat-date-range-input>\r\n    <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\r\n    <mat-date-range-picker #picker></mat-date-range-picker>\r\n  \r\n    <mat-error *ngIf=\"range.controls.start.hasError('matStartDateInvalid')\">Invalid start date</mat-error>\r\n    <mat-error *ngIf=\"range.controls.end.hasError('matEndDateInvalid')\">Invalid end date</mat-error>\r\n  </mat-form-field>\r\n</ion-col>\r\n\r\n  <ion-col>\r\n    <ion-button  (click)=\"calcRequests()\" expand=\"block\" fill=\"outline\" >Search</ion-button>\r\n  </ion-col>\r\n\r\n</ion-row>\r\n</ion-item-divider>\r\n\r\n<ion-text color=\"primary\">\r\n  <h4>Start: {{range.value.start | date:'medium' }}</h4>\r\n</ion-text>\r\n\r\n<ion-text color=\"primary\">\r\n  <h4>End: {{range.value.end | date:'medium' }}</h4>\r\n</ion-text>\r\n\r\n<ion-item>\r\n\r\n</ion-item>\r\n\r\n<ion-item-divider>\r\n\r\n  <ion-title size=\"medium\">Airtime Purchase</ion-title>\r\n  \r\n</ion-item-divider>\r\n\r\n\r\n<ion-row class=\"ion-align-items-center\">\r\n\r\n  <ion-col>\r\n    TYPE\r\n  </ion-col>\r\n  <ion-col>\r\n    CREDIT\r\n  </ion-col>\r\n  <ion-col>\r\n    AMOUNT\r\n  </ion-col>\r\n  <ion-col>\r\n    PROFIT\r\n  </ion-col>\r\n\r\n</ion-row>\r\n\r\n\r\n\r\n<ion-row class=\"ion-align-items-center\">\r\n\r\n  <ion-col>\r\n    Airtime \r\n  </ion-col>\r\n  <ion-col>\r\n    {{airtimeCredit}}\r\n  </ion-col>\r\n  <ion-col>\r\n    {{airtimeCash}}\r\n  </ion-col>\r\n  <ion-col>\r\n    {{airtimeProfit}}\r\n  </ion-col>\r\n\r\n</ion-row>\r\n\r\n\r\n<ion-row class=\"ion-align-items-center\">\r\n\r\n  <ion-col>\r\n    Data \r\n  </ion-col>\r\n  <ion-col>\r\n    {{dataCredit}}\r\n  </ion-col>\r\n  <ion-col>\r\n    {{dataCash}}\r\n  </ion-col>\r\n  <ion-col>\r\n    {{dataProfit}}\r\n  </ion-col>\r\n\r\n</ion-row>\r\n\r\n\r\n\r\n\r\n<ion-row class=\"ion-align-items-center\">\r\n\r\n  <ion-col>\r\n    TOTALS \r\n  </ion-col>\r\n  <ion-col>\r\n    {{dataCredit + airtimeCredit}}\r\n  </ion-col>\r\n  <ion-col>\r\n    {{dataCash + airtimeCash }}\r\n  </ion-col>\r\n  <ion-col>\r\n    {{airtimeProfit + dataProfit}}\r\n  </ion-col>\r\n\r\n</ion-row>\r\n\r\n\r\n\r\n\r\n<ion-row >\r\n\r\n\r\n\r\n\r\n</ion-row>\r\n\r\n<ion-item>\r\n\r\n</ion-item>\r\n\r\n\r\n\r\n<ion-item-divider>\r\n\r\n  <ion-title size=\"medium\">Conversion </ion-title>\r\n  \r\n</ion-item-divider>\r\n\r\n\r\n<ion-row class=\"ion-align-items-center\">\r\n  <ion-col>\r\n    AIRTIME RECEIVED\r\n  </ion-col>\r\n  <ion-col>\r\n    CASH SENT\r\n  </ion-col>\r\n  <ion-col>\r\n    COST\r\n  </ion-col>\r\n</ion-row>\r\n\r\n\r\n<ion-row>\r\n  <ion-col>\r\n    {{conversionAirtime}}\r\n  </ion-col>\r\n\r\n  <ion-col>\r\n    {{conversionCash}}\r\n  </ion-col>\r\n\r\n\r\n  <ion-col>\r\n    {{conversionCost}}\r\n  </ion-col>\r\n\r\n</ion-row>\r\n\r\n<ion-row>\r\n\r\n  <ion-col>\r\n  Mpesa-Transaction cost\r\n  </ion-col>\r\n\r\n\r\n  <ion-col>\r\n     ---\r\n    </ion-col>\r\n\r\n\r\n    <ion-col>\r\n      <ion-col>\r\n        {{conversionCost}}\r\n      </ion-col>\r\n      </ion-col>\r\n\r\n</ion-row>\r\n\r\n\r\n<ion-row>\r\n\r\n  <ion-col>\r\n  USSD- Dialing cost\r\n  </ion-col>\r\n\r\n\r\n  <ion-col>\r\n     ---\r\n    </ion-col>\r\n\r\n\r\n    <ion-col>\r\n      <ion-col>\r\n        {{conversionCost1}}\r\n      </ion-col>\r\n      </ion-col>\r\n\r\n</ion-row>\r\n\r\n\r\n<ion-row>\r\n\r\n  <ion-col>\r\n  TOTAL cost\r\n  </ion-col>\r\n\r\n\r\n  <ion-col>\r\n     ---\r\n    </ion-col>\r\n\r\n\r\n    <ion-col>\r\n      <ion-col>\r\n        {{conversionCost +  conversionCost1}}\r\n      </ion-col>\r\n      </ion-col>\r\n\r\n</ion-row>\r\n\r\n</ion-list>\r\n\r\n</ion-grid>\r\n\r\n</ion-content>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/folder/folder.page.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/folder/folder.page.html ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\">\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-menu-button></ion-menu-button>\r\n    </ion-buttons>\r\n\r\n    <ion-title size=\"large\">{{ folder }}</ion-title>\r\n \r\n    <ion-searchbar [(ngModel)]=\"searchKey\" animated  (ionInput)=\"onInput($event)\" (ionCancel)=\"onCancel($event)\"></ion-searchbar>\r\n  \r\n \r\n    <ion-row>\r\n\r\n    <ion-col>\r\n      <mat-form-field appearance=\"fill\">\r\n        <mat-label>Enter a date range</mat-label>\r\n        <mat-date-range-input [formGroup]=\"range\" [rangePicker]=\"picker\">\r\n          <input [min]=\"minDate\" [max]=\"maxDate\"   matStartDate formControlName=\"start\" placeholder=\"Start date\">\r\n          <input  [min]=\"minDate\" [max]=\"maxDate\"  matEndDate formControlName=\"end\" placeholder=\"End date\">\r\n        </mat-date-range-input>\r\n        <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\r\n        <mat-date-range-picker #picker></mat-date-range-picker>\r\n      \r\n        <mat-error *ngIf=\"range.controls.start.hasError('matStartDateInvalid')\">Invalid start date</mat-error>\r\n        <mat-error *ngIf=\"range.controls.end.hasError('matEndDateInvalid')\">Invalid end date</mat-error>\r\n      </mat-form-field>\r\n<ion-row>\r\n  <ion-button  (click)=\"getByDate()\" expand=\"block\" fill=\"outline\" >Search</ion-button>\r\n</ion-row>\r\n     \r\n    </ion-col>\r\n    \r\n    \r\n\r\n\r\n     \r\n        <ion-col size=\"4\">\r\n          <ion-item>\r\n            <ion-label>Manual Transactions</ion-label>\r\n        \r\n          <ion-select [(ngModel)]=\"filterManual\" placeholder=\"Show Manual Requests\" okText=\"Okay\" cancelText=\"Dismiss\">\r\n          \r\n              <ion-select-option  [value]= true >Yes</ion-select-option>\r\n              <ion-select-option  [value]= false >No</ion-select-option>\r\n        \r\n            </ion-select>\r\n          </ion-item>\r\n\r\n<ion-item>\r\n\r\n  <ion-button col (click)=\"placeFilters()\" >\r\n    Filter\r\n    </ion-button>\r\n\r\n    <ion-button color= \"danger\" col (click)=\"clearFilters()\">\r\n      Clear\r\n      </ion-button>\r\n</ion-item>\r\n\r\n      </ion-col>\r\n\r\n \r\n     \r\n    </ion-row>\r\n\r\n\r\n  </ion-toolbar>\r\n \r\n\r\n</ion-header>\r\n\r\n<ion-content >\r\n \r\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"getUsersList($event)\" pullMin=\"100\" pullMax=\"200\">\r\n    <ion-refresher-content \r\n    pullingIcon=\"arrow-down-outline\" \r\n    pullingText=\"Pull to refresh\" \r\n    refreshingSpinner=\"crescent\"\r\n    refreshingText=\"Refreshing...\">\r\n  </ion-refresher-content>\r\n  </ion-refresher>\r\n\r\n  <ion-segment color=\"dark\" [(ngModel)]=\"states\"  >\r\n\r\n\r\n    <ion-segment-button value=\"A2M\">\r\n      AIRTIME TO MPESA\r\n    </ion-segment-button>\r\n  \r\n    <ion-segment-button  value=\"M2A\">\r\n      BUY AIRTIME\r\n    </ion-segment-button>\r\n\r\n    <ion-segment-button  value=\"Data\">\r\n     BUY DATA\r\n    </ion-segment-button>\r\n\r\n\r\n\r\n  </ion-segment>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n  <ion-grid  [ngSwitch]=\"states\">\r\n\r\n\r\n    <ion-row  *ngSwitchCase=\"'A2M'\" >\r\n\r\n      <ion-virtual-scroll [items]=\"A2M\">\r\n      \r\n\r\n<ion-item button (click)=\"presentModal(req)\" *virtualItem=\"let req\" >\r\n  <ion-thumbnail slot=\"start\">\r\n    <img src=\"assets/img/cash.png\">\r\n  </ion-thumbnail>\r\n  <ion-label class=\"ion-text-wrap\">\r\n    Phone: {{req.phone}}\r\n    <ion-text color=\"primary\">\r\n      <h3>Date: {{req.date | date:'medium' }}</h3>\r\n    </ion-text>\r\n    <ion-text *ngIf=\"req.transType== 'M2A'\" color=\"secondary\">\r\n      <h3>Type:  MPESA TO AIRTIME</h3>\r\n    </ion-text>\r\n\r\n    <ion-text *ngIf=\"req.transType== 'A2M'\" color=\"success\">\r\n      <h3>Type:  AIRTIME TO MPESA</h3>\r\n    </ion-text>\r\n\r\n    <ion-text *ngIf=\"req.transType== 'Data'\" color=\"success\">\r\n      <h3>Type:   DATA BUNDLES</h3>\r\n    </ion-text>\r\n\r\n    \r\n    <ion-text *ngIf=\"req.transType== 'M2A'\" >\r\n      Airtime to be received: <span style=\"color: green;\">Kes {{req.amount}}</span>\r\n    </ion-text>\r\n\r\n    <ion-text *ngIf=\"req.transType== 'A2M'\" >\r\n      Airtime to Convert: <span style=\"color: green;\"> Kes {{req.amount}}</span>\r\n    </ion-text>\r\n\r\n    <ion-text *ngIf=\"req.transType== 'Data' \" >\r\n      Data type: <span style=\"color: green;\"> {{req.Category}}</span>\r\n    </ion-text>\r\n\r\n    \r\n\r\n\r\n    <ion-text *ngIf=\"req.transType== 'M2A'\" >\r\n    Cash to Pay: <span style=\"color: green\">Kes {{req.mpesaAmount}}</span>\r\n    </ion-text>\r\n\r\n    <ion-text *ngIf=\"req.transType== 'Data'\" >\r\n      Cash to Pay: <span style=\"color: green\">Kes {{req.mpesaAmount}}</span>\r\n      </ion-text>\r\n\r\n    <ion-text *ngIf=\"req.transType== 'A2M'\" >\r\n      Cash to be received by client: <span style=\"color: green\">Kes {{req.mpesaAmount}}</span>\r\n    </ion-text>\r\n\r\n\r\n  <ion-text *ngIf=\"req.state === 'complete'\" color=\"success\" >\r\n    <p> {{req.state}}</p>\r\n  </ion-text>\r\n\r\n  <ion-text *ngIf=\"req.state === 'pendingMpesa' || req.state ==='pendingCredit' || req.state ==='pendingData' || req.state ==='pendingApproval' \" color=\"danger\" >\r\n    <p> {{req.state}}</p>\r\n  </ion-text>\r\n\r\n  <ion-text *ngIf=\"req.state === 'error404' || req.state === 'requestExpired' \" color=\"danger\">\r\n    <p> {{req.state}}</p>\r\n  </ion-text>\r\n  \r\n \r\n  \r\n\r\n  </ion-label>\r\n\r\n  \r\n\r\n\r\n  \r\n\r\n\r\n  \r\n</ion-item>\r\n\r\n\r\n</ion-virtual-scroll>\r\n\r\n\r\n\r\n  </ion-row>\r\n\r\n\r\n\r\n  <ion-row   *ngSwitchCase=\"'M2A'\" >\r\n\r\n    <ion-virtual-scroll [items]=\"M2A\">\r\n    \r\n\r\n<ion-item button (click)=\"presentModal(req)\" *virtualItem=\"let req\" >\r\n<ion-thumbnail slot=\"start\">\r\n  <img src=\"assets/img/cash.png\">\r\n</ion-thumbnail>\r\n<ion-label class=\"ion-text-wrap\">\r\n  Phone: {{req.phone}}\r\n  <ion-text color=\"primary\">\r\n    <h3>Date: {{req.date | date:'medium' }}</h3>\r\n  </ion-text>\r\n  <ion-text *ngIf=\"req.transType== 'M2A'\" color=\"secondary\">\r\n    <h3>Type:  MPESA TO AIRTIME</h3>\r\n  </ion-text>\r\n\r\n  <ion-text *ngIf=\"req.transType== 'A2M'\" color=\"success\">\r\n    <h3>Type:  AIRTIME TO MPESA</h3>\r\n  </ion-text>\r\n\r\n  <ion-text *ngIf=\"req.transType== 'Data'\" color=\"success\">\r\n    <h3>Type:   DATA BUNDLES</h3>\r\n  </ion-text>\r\n\r\n  \r\n  <ion-text *ngIf=\"req.transType== 'M2A'\" >\r\n    Airtime to be received: <span style=\"color: green;\">Kes {{req.amount}}</span>\r\n  </ion-text>\r\n\r\n  <ion-text *ngIf=\"req.transType== 'A2M'\" >\r\n    Airtime to Convert: <span style=\"color: green;\"> Kes {{req.amount}}</span>\r\n  </ion-text>\r\n\r\n  <ion-text *ngIf=\"req.transType== 'Data' \" >\r\n    Data type: <span style=\"color: green;\"> {{req.Category}}</span>\r\n  </ion-text>\r\n\r\n  \r\n\r\n\r\n  <ion-text *ngIf=\"req.transType== 'M2A'\" >\r\n  Cash to Pay: <span style=\"color: green\">Kes {{req.mpesaAmount}}</span>\r\n  </ion-text>\r\n\r\n  <ion-text *ngIf=\"req.transType== 'Data'\" >\r\n    Cash to Pay: <span style=\"color: green\">Kes {{req.mpesaAmount}}</span>\r\n    </ion-text>\r\n\r\n  <ion-text *ngIf=\"req.transType== 'A2M'\" >\r\n    Cash to be received by client: <span style=\"color: green\">Kes {{req.mpesaAmount}}</span>\r\n  </ion-text>\r\n\r\n\r\n<ion-text *ngIf=\"req.state === 'complete'\" color=\"success\" >\r\n  <p> {{req.state}}</p>\r\n</ion-text>\r\n\r\n<ion-text *ngIf=\"req.state === 'pendingMpesa' || req.state ==='pendingCredit' || req.state ==='pendingData' \" color=\"danger\" >\r\n  <p> {{req.state}}</p>\r\n</ion-text>\r\n\r\n<ion-text *ngIf=\"req.state === 'error404' \"  color=\"danger\">\r\n  <p> {{req.state}}</p>\r\n</ion-text>\r\n\r\n</ion-label>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</ion-item>\r\n\r\n\r\n</ion-virtual-scroll>\r\n\r\n\r\n\r\n</ion-row>\r\n\r\n\r\n\r\n\r\n<ion-row  *ngSwitchCase=\"'Data'\" >\r\n\r\n  <ion-virtual-scroll [items]=\"Data\">\r\n  \r\n\r\n<ion-item button (click)=\"presentModal(req)\" *virtualItem=\"let req\" >\r\n<ion-thumbnail slot=\"start\">\r\n<img src=\"assets/img/cash.png\">\r\n</ion-thumbnail>\r\n<ion-label class=\"ion-text-wrap\">\r\nPhone: {{req.phone}}\r\n<ion-text color=\"primary\">\r\n  <h3>Date: {{req.date | date:'medium' }}</h3>\r\n</ion-text>\r\n<ion-text *ngIf=\"req.transType== 'M2A'\" color=\"secondary\">\r\n  <h3>Type:  MPESA TO AIRTIME</h3>\r\n</ion-text>\r\n\r\n<ion-text *ngIf=\"req.transType== 'A2M'\" color=\"success\">\r\n  <h3>Type:  AIRTIME TO MPESA</h3>\r\n</ion-text>\r\n\r\n<ion-text *ngIf=\"req.transType== 'Data'\" color=\"success\">\r\n  <h3>Type:   DATA BUNDLES</h3>\r\n</ion-text>\r\n\r\n\r\n<ion-text *ngIf=\"req.transType== 'M2A'\" >\r\n  Airtime to be received: <span style=\"color: green;\">Kes {{req.amount}}</span>\r\n</ion-text>\r\n\r\n<ion-text *ngIf=\"req.transType== 'A2M'\" >\r\n  Airtime to Convert: <span style=\"color: green;\"> Kes {{req.amount}}</span>\r\n</ion-text>\r\n\r\n<ion-text *ngIf=\"req.transType== 'Data' \" >\r\n  Data type: <span style=\"color: green;\"> {{req.Category}}</span>\r\n</ion-text>\r\n\r\n\r\n\r\n\r\n<ion-text *ngIf=\"req.transType== 'M2A'\" >\r\nCash to Pay: <span style=\"color: green\">Kes {{req.mpesaAmount}}</span>\r\n</ion-text>\r\n\r\n<ion-text *ngIf=\"req.transType== 'Data'\" >\r\n  Cash to Pay: <span style=\"color: green\">Kes {{req.mpesaAmount}}</span>\r\n  </ion-text>\r\n\r\n<ion-text *ngIf=\"req.transType== 'A2M'\" >\r\n  Cash to be received by client: <span style=\"color: green\">Kes {{req.mpesaAmount}}</span>\r\n</ion-text>\r\n\r\n\r\n<ion-text *ngIf=\"req.state === 'complete'\" color=\"success\" >\r\n<p> {{req.state}}</p>\r\n</ion-text>\r\n\r\n<ion-text *ngIf=\"req.state === 'pendingMpesa' || req.state ==='pendingCredit' || req.state ==='pendingData' \" color=\"danger\" >\r\n<p> {{req.state}}</p>\r\n</ion-text>\r\n\r\n<ion-text *ngIf=\"req.state === 'error404' \"  color=\"danger\">\r\n  <p> {{req.state}}</p>\r\n</ion-text>\r\n\r\n</ion-label>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</ion-item>\r\n\r\n\r\n</ion-virtual-scroll>\r\n\r\n\r\n\r\n</ion-row>\r\n\r\n\r\n</ion-grid>\r\n\r\n\r\n</ion-content>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/sms/sms/sms.component.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/sms/sms/sms.component.html ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\">\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-menu-button></ion-menu-button>\r\n    </ion-buttons>\r\n \r\n    <ion-searchbar [(ngModel)]=\"searchKey\" (ionCancel)=\"onCancel()\"></ion-searchbar>\r\n  \r\n\r\n  </ion-toolbar>\r\n \r\n\r\n</ion-header>\r\n\r\n\r\n<ion-content>\r\n\r\n  <ion-list>\r\n <ion-list-header>\r\n   \r\n  </ion-list-header>\r\n\r\n\r\n  <ion-item>\r\n    <ion-label>My Devices</ion-label>\r\n\r\n  <ion-select [(ngModel)]=\"device\" placeholder=\"Select One\" okText=\"Okay\" cancelText=\"Dismiss\">\r\n  \r\n      <ion-select-option *ngFor=\"let device of myDevices\" [value]=\"device.phone\">{{device.name}}--{{device.phone}}</ion-select-option>\r\n\r\n    </ion-select>\r\n  </ion-item>\r\n\r\n</ion-list>\r\n\r\n<!--\r\n<h3 *ngIf=\"sms.length === 0? true:false \">Server responce: {{title}}</h3>\r\n\r\n  <h3 *ngIf=\"sms.length === 0? true:false \"> No sms to display</h3>\r\n-->\r\n\r\n<ion-grid>\r\n  <ion-row>\r\n  <ion-col  class=\"ion-align-self-center\" size=\"12\" *ngFor=\"let sms of sms\" >\r\n    <ion-card  >\r\n\r\n      <ion-card-header>\r\n        <ion-avatar item-start>\r\n          <img src=\"assets/img/cash.png\">\r\n        </ion-avatar>\r\n        <ion-card-title>\r\n          Sambaza Sms Sent by Client\r\n        </ion-card-title>\r\n      </ion-card-header>\r\n  \r\n  \r\n    <ion-card-content>\r\n      <ion-list>\r\n        <ion-item>\r\n        \r\n          <ion-label>\r\n            Phone number: \r\n          </ion-label>\r\n          <h3>{{searchKey}}</h3>\r\n        </ion-item>\r\n\r\n        <ion-item >\r\n          <ion-label>\r\n            Date: \r\n          </ion-label>\r\n  \r\n          <p>{{sms.date | date:'medium'}}</p>\r\n        </ion-item>\r\n            \r\n              <ion-item >\r\n                <ion-label>\r\n                  Message: \r\n                </ion-label>\r\n        \r\n                <p>{{sms.body}}</p>\r\n              </ion-item>\r\n        \r\n        \r\n              <ion-item >\r\n                <ion-label>\r\n                  From: \r\n                </ion-label>\r\n               {{sms.address}}\r\n              </ion-item>\r\n        </ion-list>\r\n    </ion-card-content>\r\n\r\n  \r\n\r\n  </ion-card>\r\n</ion-col>\r\n</ion-row>\r\n</ion-grid>\r\n\r\n\r\n\r\n  <ion-button  (click)=\"sendReq()\" color=\"success\">Send request for Sms</ion-button>\r\n</ion-content>\r\n\r\n\r\n\r\n");

/***/ }),

/***/ "./src/app/app-adds/app-adds.component.scss":
/*!**************************************************!*\
  !*** ./src/app/app-adds/app-adds.component.scss ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC1hZGRzL2FwcC1hZGRzLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/app-adds/app-adds.component.ts":
/*!************************************************!*\
  !*** ./src/app/app-adds/app-adds.component.ts ***!
  \************************************************/
/*! exports provided: AppAddsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppAddsComponent", function() { return AppAddsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _edit_app_data_edit_app_data_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../edit-app-data/edit-app-data.component */ "./src/app/edit-app-data/edit-app-data.component.ts");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");






let AppAddsComponent = class AppAddsComponent {
    constructor(modalController, afFirestore) {
        this.modalController = modalController;
        this.afFirestore = afFirestore;
        this.products = [];
    }
    ngOnInit() {
        this.fetchData();
    }
    presentNewModal(data) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log(data);
            const modal = yield this.modalController.create({
                component: _edit_app_data_edit_app_data_component__WEBPACK_IMPORTED_MODULE_3__["EditAppDataComponent"],
                cssClass: 'my-custom-class',
                showBackdrop: true,
                swipeToClose: true,
                animated: true,
                componentProps: { req: data, mode: "new" }
            });
            modal.onWillDismiss().then(data => {
                console.log("about to dismiss modal");
                // this.getProducts()
            });
            return yield modal.present();
        });
    }
    presentEditModal(data) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log(data);
            const modal = yield this.modalController.create({
                component: _edit_app_data_edit_app_data_component__WEBPACK_IMPORTED_MODULE_3__["EditAppDataComponent"],
                cssClass: 'my-custom-class',
                showBackdrop: true,
                swipeToClose: true,
                animated: true,
                componentProps: { req: data, mode: "edit" }
            });
            modal.onWillDismiss().then(data => {
                console.log("about to dismiss modal");
                // this.getProducts()
            });
            return yield modal.present();
        });
    }
    dismiss() {
        // using the injected ModalController this page
        // can "dismiss" itself and optionally pass back data
        this.modalController.dismiss({
            'dismissed': true
        });
    }
    getDataFromFirestore() {
        return this.afFirestore.collection('appImages').snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(actions => {
            return actions.map(action => {
                const data = action.payload.doc.data();
                const id = action.payload.doc.id;
                return Object.assign({ id }, data);
            });
        }));
    }
    fetchData() {
        this.getDataFromFirestore().subscribe((data) => {
            this.products = data;
            console.log(data);
        }, (error) => {
            console.error('Error fetching data:', error);
        });
    }
};
AppAddsComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"] }
];
AppAddsComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-app-adds',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./app-adds.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app-adds/app-adds.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./app-adds.component.scss */ "./src/app/app-adds/app-adds.component.scss")).default]
    })
], AppAddsComponent);



/***/ }),

/***/ "./src/app/auth/auth.service.ts":
/*!**************************************!*\
  !*** ./src/app/auth/auth.service.ts ***!
  \**************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _auth0_angular_jwt__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @auth0/angular-jwt */ "./node_modules/@auth0/angular-jwt/__ivy_ngcc__/fesm2015/auth0-angular-jwt.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");



const helper = new _auth0_angular_jwt__WEBPACK_IMPORTED_MODULE_2__["JwtHelperService"]();

let AuthService = class AuthService {
    constructor(storage) {
        this.storage = storage;
    }
    getToken() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            return yield this.storage.get('token');
        });
    }
    isAuthenticated(token) {
        // get the token
        return helper.isTokenExpired(token);
        // return a boolean reflecting 
        // whether or not the token is expired
    }
};
AuthService.ctorParameters = () => [
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] }
];
AuthService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], AuthService);



/***/ }),

/***/ "./src/app/charts/charts.component.scss":
/*!**********************************************!*\
  !*** ./src/app/charts/charts.component.scss ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NoYXJ0cy9jaGFydHMuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/charts/charts.component.ts":
/*!********************************************!*\
  !*** ./src/app/charts/charts.component.ts ***!
  \********************************************/
/*! exports provided: ChartsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChartsComponent", function() { return ChartsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let ChartsComponent = class ChartsComponent {
    constructor() { }
    ngOnInit() { }
};
ChartsComponent.ctorParameters = () => [];
ChartsComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-charts',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./charts.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/charts/charts.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./charts.component.scss */ "./src/app/charts/charts.component.scss")).default]
    })
], ChartsComponent);



/***/ }),

/***/ "./src/app/edit-app-data/edit-app-data.component.scss":
/*!************************************************************!*\
  !*** ./src/app/edit-app-data/edit-app-data.component.scss ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2VkaXQtYXBwLWRhdGEvZWRpdC1hcHAtZGF0YS5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/edit-app-data/edit-app-data.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/edit-app-data/edit-app-data.component.ts ***!
  \**********************************************************/
/*! exports provided: EditAppDataComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditAppDataComponent", function() { return EditAppDataComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/storage */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-storage.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");





let EditAppDataComponent = class EditAppDataComponent {
    constructor(modalController, afs, afStorage) {
        this.modalController = modalController;
        this.afs = afs;
        this.afStorage = afStorage;
        // File uploading status
        this.isFileUploading = false;
        this.isFileUploaded = false;
        this.myUrl = "";
        this.fileStoragePath = "";
        this.per = 0;
        this.name = "";
        this.price = 0;
        this.description = "";
    }
    ngOnInit() {
        if (this.mode === "edit") {
            this.name = this.req.customName;
            this.description = this.req.description;
            this.myUrl = this.req.image;
            this.isFileUploading = false;
            this.isFileUploaded = true;
        }
        else if (this.mode === "new") {
            this.isFileUploading = false;
            this.isFileUploaded = false;
        }
        else {
            console.log("undefined action request");
        }
    }
    uploadImage(event) {
        const file = event.item(0);
        console.log(file.name);
        // Image validation
        if (file.type.split('/')[0] !== 'image') {
            console.log('File type is not supported!');
            return;
        }
        this.isFileUploading = true;
        this.isFileUploaded = false;
        this.imgName = file.name;
        console.log(file.name);
        const storageRef = this.afStorage.ref(`images`);
        const fileExt = file.name.split('.').pop();
        const imageName = `${file.name}.${fileExt}`; // Replace with your custom name or generate dynamically
        const imageRef = storageRef.child(imageName);
        // Upload the image file
        const uploadTask = imageRef.put(file);
        this.percentageVal = uploadTask.percentageChanges();
        this.percentageVal.subscribe(resp => {
            this.per = Math.floor(resp) / 100;
            console.log("PERCENTAGE..", this.per);
            if (resp === 100) {
                imageRef.getDownloadURL().subscribe(resp => {
                    console.log("FILE URL IS...", resp);
                    this.myUrl = resp;
                    this.isFileUploading = false;
                    this.isFileUploaded = true;
                });
            }
            else {
            }
        });
        // Get the download URL of the uploaded image
        // Storage path
        // this.fileStoragePath = `waterApp/${new Date().getTime()}_${file.name}`;
        /*
        
        // Image reference
          const imageRef = this.afStorage.ref(this.fileStoragePath);
      
          
      
          // File upload task
          this.fileUploadTask = this.afStorage.upload(this.fileStoragePath, file);
      
      
         // imageRef.put(file);
      
      
          // Show uploading progress
          this.percentageVal =  this.fileUploadTask.percentageChanges()
      
          
          this.UploadedImageURL = imageRef.getDownloadURL();
      
         
          
          this.percentageVal.subscribe(resp=>{
              
         
      
            this.per = Math.floor(resp) /100
      
            console.log( "PERCENTAGE..", this.per);
      
            if (resp === 100) {
      
      
              this.UploadedImageURL.subscribe(resp=>{
              
                console.log( "FILE URL IS...", resp);
      
                this.myUrl = resp
      
                this.isFileUploading = false
                this.isFileUploaded = true
      
            })
              
            } else {
              
            }
      
        })
      */
    }
    uploadImageToStorage(imageFile) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const storageRef = this.afStorage.ref('new_app_adds');
            const fileExt = imageFile.name.split('.').pop(); // Get the original file extension
            const imageName = `${this.name}.${fileExt}`; // Generate a unique name with the original file extension
            const imageRef = storageRef.child(imageName);
            yield imageRef.put(imageFile);
            const imageUrl = yield imageRef.getDownloadURL().toPromise();
            return imageUrl;
        });
    }
    storeImageData() {
        const collectionRef = this.afs.collection('appImages');
        collectionRef.add({
            image: this.myUrl,
            customName: this.name,
            description: this.description
        }).then(data => {
            console.log(data);
            this.dismiss();
        }).catch(error => {
            console.log(error);
        });
    }
    handleImageUpload(imageFile, customName) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            try {
                const imageUrl = yield this.uploadImageToStorage(imageFile);
                //    await this.storeImageData(imageUrl, customName);
                console.log('Image uploaded and data stored successfully.');
            }
            catch (error) {
                console.error('Error uploading image and storing data:', error);
            }
        });
    }
    delete() {
        const originalImageRef = this.afStorage.refFromURL(this.myUrl);
        console.log(originalImageRef);
        originalImageRef.delete().subscribe(data => {
            console.log(data);
            this.isFileUploading = this.isFileUploaded = false;
            this.myUrl = "";
        }, error => {
            console.log(error);
        });
    }
    deleteProduct() {
        const originalImageRef = this.afStorage.refFromURL(this.myUrl);
        const docRef = this.afs.collection('appImages').doc(this.req.id);
        docRef.delete().then(d => {
            console.log("DELETED PRODUCT FROM DATABASE..", d);
            this.isFileUploading = this.isFileUploaded = false;
            this.myUrl = "";
            this.name = "";
            this.description = "";
            this.mode = "new";
        }).catch(e => {
            console.log("failed to delete", e);
        });
        console.log(this.req.id);
        if (originalImageRef) {
            originalImageRef.delete().subscribe(data => {
                console.log(data);
                this.isFileUploading = this.isFileUploaded = false;
                this.myUrl = "";
            }, error => {
                console.log(error);
            });
        }
    }
    updateProduct() {
        const docRef = this.afs.collection('appImages').doc(this.req.id);
        let newDoc = {
            customName: this.name,
            description: this.description,
            image: this.myUrl
        };
        docRef.update(newDoc).then(d => {
            console.log("DELETED PRODUCT FROM DATABASE..", d);
            this.isFileUploading = false;
            this.isFileUploaded = true;
        }).catch(e => {
            console.log("failed to delete", e);
        });
        /*  this.requestUssd.updateProducts(
            {query:{_id:this.req._id},item:{name:this.name,description:this.description,price:this.price,
              image:this.myUrl,fileStoragePath:this.fileStoragePath}}).subscribe(data=>{
      
      console.log(data);
      
      if (data.state === 'ok') {
        
        this.dismiss()
      } else {
        this.dismiss()
      }
      
              }, error=>{
      
                console.log(error);
                this.dismiss()
                
              })
      */
    }
    dismiss() {
        // using the injected ModalController this page
        // can "dismiss" itself and optionally pass back data
        this.modalController.dismiss({
            'dismissed': true
        });
    }
};
EditAppDataComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"] },
    { type: _angular_fire_storage__WEBPACK_IMPORTED_MODULE_2__["AngularFireStorage"] }
];
EditAppDataComponent.propDecorators = {
    req: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"] }],
    mode: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"] }]
};
EditAppDataComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-edit-app-data',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./edit-app-data.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/edit-app-data/edit-app-data.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./edit-app-data.component.scss */ "./src/app/edit-app-data/edit-app-data.component.scss")).default]
    })
], EditAppDataComponent);



/***/ }),

/***/ "./src/app/finance/finance/finance.component.scss":
/*!********************************************************!*\
  !*** ./src/app/finance/finance/finance.component.scss ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZpbmFuY2UvZmluYW5jZS9maW5hbmNlLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/finance/finance/finance.component.ts":
/*!******************************************************!*\
  !*** ./src/app/finance/finance/finance.component.ts ***!
  \******************************************************/
/*! exports provided: FinanceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FinanceComponent", function() { return FinanceComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _get_requests_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../get-requests.service */ "./src/app/get-requests.service.ts");
/* harmony import */ var ngx_socket_io__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-socket-io */ "./node_modules/ngx-socket-io/__ivy_ngcc__/fesm2015/ngx-socket-io.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");









let FinanceComponent = class FinanceComponent {
    constructor(storage, router, activatedRoute, requestUssd, socket, ref, alertController) {
        this.storage = storage;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.requestUssd = requestUssd;
        this.socket = socket;
        this.ref = ref;
        this.alertController = alertController;
        this.range = new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormGroup"]({
            start: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"](),
            end: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"]()
        });
        this.searchKey = "";
        this.mpesaBalance2 = ""; //working
        this.mpesaBalance3 = ""; //general
        this.devices = [];
        this.devices3 = [];
        this.myDevice = "";
        this.clientPhone = "";
        this.Amount = null;
        this.requests3 = [];
        this.requests = [];
        this.user = "";
        this.dataReq = [];
        this.airtimeReq = [];
        this.conversionReq = [];
        this.smsBalance = 0;
        this.airtimeCredit = 0;
        this.dataCredit = 0;
        this.airtimeCash = 0;
        this.dataCash = 0;
        this.dataProfit = 0;
        this.airtimeProfit = 0;
        this.totalProfit = 0;
        this.conversionAirtime = 0;
        this.conversionCash = 0;
        this.conversionCost = 0;
        this.conversionCost1 = 0;
        this.totalAirtime = 0;
        this.dispersable = 0;
        this.minDate = new Date(21, 9, 10);
    }
    ngOnInit() {
        this.getMpesabalance();
        this.getCreditBalance({ name: null });
        this.africabalance();
        this.storage.get('profile').then(data => {
            if (!data) {
            }
            else {
                this.user = data.profile.email;
                console.log(data.profile.email);
            }
        });
        this.storage.get('authenticated').then(data => {
            if (!data) {
                this.router.navigate(['/login']);
            }
        });
        //received mpesa balance
        this.socket.on("b2cBalance", data => {
            console.log("balance from mpesa balance api...", data);
            //{"Key":"AccountBalance","Value":"Working Account|KES|0.00|0.00|0.00|0.00&Utility Account|KES|30544.18|30544.18|0.00|0.00&Charges Paid Account|KES|0.00|0.00|0.00|0.00"}
            if (data.Result.ResultCode === 0) {
                /// this.mpesaBalance = 
                let bal = data.Result.ResultParameters.ResultParameter[1].Value;
                console.log(data.Result.ResultCode);
                //"Working Account|KES|0.00|0.00|0.00|0.00&Utility Account|KES|5447.91|5447.91|0.00|0.00&Charges Paid Account|KES|0.00|0.00|0.00|0.00"
                this.mpesaBalance2 = bal.split(" ")[1].split("|")[2]; //working 
                this.mpesaBalance3 = bal.split(" ")[2].split("|")[2]; //utillity
                console.log(this.mpesaBalance3);
                this.ref.markForCheck();
            }
            else {
            }
        });
        //USSD BALANCE 
        this.socket.on("ussdBalance", data => {
            this.smsBalance = data;
            this.ref.markForCheck();
            console.log("sms balance....", data);
            console.log("balance from mpesa balance api...", data);
        });
        /*
        this.socket.on("dashBoardCredit",data=>{
        
          console.log("balance from mpesa balance api...", data);
          
          
          
          
          //{"Key":"AccountBalance","Value":"Working Account|KES|0.00|0.00|0.00|0.00&Utility Account|KES|30544.18|30544.18|0.00|0.00&Charges Paid Account|KES|0.00|0.00|0.00|0.00"}
           
          this.devices = data
          this.ref.markForCheck();
          console.log("credit balance....", data);
          
          })
        
        */
    }
    sendAirtime() {
        //this.socket.emit("sendtoDist",{phone:this.clientPhone, amount:this.Amount, device:this.myDevice})
        /*
         {sessionId:"Manual Dash",  text:[""],
           phone:this.clientPhone,  amount:this.Amount,  otherTransNum:null,  scratchCard:null,
           mpesaAmount:null, deviceId:devices[0]  }
         */
        let req = { phone: this.clientPhone, amount: this.Amount, device: this.myDevice, initializer: this.user };
        //&& user == 'babzgeo27@gmail.com'? false:true
        this.requestUssd.sendDist(req).subscribe(data => {
            if (data.msg === "invalid") {
                console.log("token expired");
                this.router.navigate(['/login']);
                this.storage.clear();
            }
            else {
                this.presentAlert("Sent to server, check in Sms menu if the client received credit.");
                console.log("credit results....", data);
            }
        });
    }
    presentAlert(message) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'my-custom-class',
                header: 'Responce',
                subHeader: '',
                message: message,
                buttons: ['OK']
            });
            yield alert.present();
            const { role } = yield alert.onDidDismiss();
            console.log('onDidDismiss resolved with role', role);
        });
    }
    getMpesabalance() {
        this.requestUssd.mpesaBalance3("A2M").subscribe(data => {
            console.log("mpesa balance....", data);
            //  console.log("mpesa balance 2....", data.data.MpesaDetails[3].Value); //workin accc
            // console.log("mpesa balance....", data.data.MpesaDetails[2].Value); //utility acc
            if (data.result === "success") {
                //this.mpesaBalance = data.data.MpesaDetails[2].Value
                //  this.mpesaBalance2 = data.data.MpesaDetails[3].Value
                //  this.ref.markForCheck();
            }
            else {
            }
        });
        this.requestUssd.mpesaBalance3("A2M").subscribe(data => {
            console.log("mpesa balance btob balance api....", data);
        });
    }
    getCreditBalance(device) {
        this.requestUssd.findDevices(device).subscribe(data => {
            this.totalAirtime = 0;
            this.dispersable = 0;
            this.devices = data.data;
            this.devices3 = this.devices;
            /*  this.devices3 = this.devices.filter((value: any, index: number, array: any[]) =>{
          
                console.log(value.state);
                
          
               return value.state === "online"
          
              })*/
            console.log("devvices online ....", this.devices3);
            this.devices.forEach(element => {
                // this.totalAirtime = this.totalAirtime + element.balance
                // this.dispersable = this.dispersable + element.usage
            });
            let usageDevice = this.devices.filter(v => {
                return v.state === "online";
            });
            usageDevice.forEach(element => {
                this.totalAirtime = this.totalAirtime + element.balance;
                this.dispersable = this.dispersable + element.usage;
            });
            this.ref.markForCheck();
            console.log("credit balance....", this.totalAirtime, this.dispersable);
            if (data.msg === "invalid") {
                console.log("token expired");
                this.router.navigate(['/login']);
                this.storage.clear();
            }
            else {
            }
        });
    }
    africabalance() {
        this.requestUssd.getUssdBal().subscribe(data => {
            this.smsBalance = data;
            this.ref.markForCheck();
            console.log("sms balance....", data);
        });
    }
    findAll() {
        this.requestUssd.findAll().subscribe((data) => {
            this.requests3 = data.data;
            this.requests = data.data;
            this.ref.markForCheck();
            console.log(data);
        });
    }
    calcRequests() {
        console.log(this.range.value);
        this.requests = [];
        this.requests3 = [];
        this.airtimeCredit = 0;
        this.dataCredit = 0;
        this.airtimeCash = 0;
        this.dataCash = 0;
        this.dataProfit = 0;
        this.airtimeProfit = 0;
        this.totalProfit = 0;
        this.conversionAirtime = 0;
        this.conversionCash = 0;
        this.conversionCost = 0;
        let date = this.range.value;
        if (date.start === null) {
            console.log("please set date");
        }
        else if (date.end === null) {
            var startDate = new Date(date.start);
            var endDate = new Date(); //date.end
            console.log("start", startDate, "End", endDate);
            /*  var resultProductData = this.requests.filter(function (a) {
                  var hitDates = a.ProductHits || {};
                  // extract all date strings
                  hitDates = Object.keys(hitDates);
                  // improvement: use some. this is an improment because .map()
                  // and .filter() are walking through all elements.
                  // .some() stops this process if one item is found that returns true in the callback function and returns true for the whole expression
                 let hitDateMatchExists = hitDates.some(function(dateStr) {
                      var date = new Date(dateStr);
                      return date >= startDate && date <= endDate
                  });
      
      
                  return hitDateMatchExists;
              });
      
              console.log(resultProductData); */
            let query = {
                start: startDate,
                end: endDate,
            };
            this.requestUssd.getByDate(query).subscribe((data) => {
                //  this.ref.markForCheck();
                console.log("REQUESTED DATA WHEN END IS NULL IS...", data);
                this.requests3 = data.data;
                this.requests = data.data;
                //  state:"complete"
                this.dataReq = this.requests3.filter((data) => {
                    return (data.transType === "Data" && data.state === "complete");
                });
                this.airtimeReq = this.requests3.filter((data) => {
                    return data.transType === "M2A" && data.state === "complete";
                });
                this.conversionReq = this.requests3.filter((data) => {
                    return data.transType === "A2M" && data.state === "complete";
                });
                console.log("DATA.....", this.dataReq);
                console.log("AIRTIME....", this.airtimeReq);
                console.log("CONVERSION....", this.conversionReq);
                this.dataReq.forEach(data => {
                    this.dataCredit = this.dataCredit + data.amount;
                    this.dataCash = this.dataCash + data.mpesaAmount;
                    this.dataProfit = Math.floor(this.dataCredit * 0.1);
                });
                this.airtimeReq.forEach(data => {
                    this.airtimeCredit = this.airtimeCredit + data.airtimeAmount;
                    this.airtimeCash = this.airtimeCash + data.mpesaAmount;
                    this.airtimeProfit = Math.floor(this.airtimeCredit * 0.1);
                    this.totalProfit = this.airtimeProfit + this.dataProfit;
                });
                this.conversionReq.forEach(data => {
                    this.conversionAirtime = this.conversionAirtime + data.amount;
                    this.conversionCash = this.conversionCash + data.mpesaAmount;
                    if (data.mpesaAmount > 1000) {
                        this.conversionCost = this.conversionCost + 8;
                    }
                    else if (data.mpesaAmount < 1000 && data.mpesaAmount > 100) {
                        this.conversionCost = this.conversionCost + 4;
                    }
                    else {
                        // this.conversionCost = this.conversionCost  
                    }
                });
                let conv1 = this.requests3.filter((data) => {
                    return data.transType === "A2M";
                });
                /*    conv1.forEach(data=>{
  
              this.conversionCost = this.conversionCost + 1.5
              
                  })*/
                console.log("data transaction..", this.dataCash, this.dataCredit, "profit...", this.dataProfit);
                console.log("airtime transaction..", this.airtimeCash, this.airtimeCredit, "profit...", this.airtimeProfit);
                console.log("PROFIT....", this.totalProfit);
                console.log("CONVERSION MPESA TOTAL", this.conversionCash);
                console.log("CONVERSION AIRTIME TOTAL", this.conversionAirtime);
                console.log("CONVERSION COST..", this.conversionCost);
                this.ref.markForCheck();
            });
        }
        else {
            /*
             let startDate = new Date(date.start).valueOf();
                    let endDate = new Date(date.end).valueOf();
            */
            let startDate = new Date(date.start);
            let endDate = new Date(date.end);
            console.log("start", startDate, "End", endDate);
            let query = {
                start: startDate,
                end: endDate,
            };
            this.requestUssd.getByDate(query).subscribe((data) => {
                //  this.ref.markForCheck();
                console.log("REQUESTED DATA WHEN END IS NULL IS...", data);
                this.requests3 = data.data;
                this.requests = data.data;
                this.dataReq = this.requests3.filter((data) => {
                    return data.transType === "Data" && data.state === "complete";
                });
                this.airtimeReq = this.requests3.filter((data) => {
                    return data.transType === "M2A" && data.state === "complete";
                });
                this.conversionReq = this.requests3.filter((data) => {
                    return data.transType === "A2M" && data.state === "complete";
                });
                console.log("DATA.....", this.dataReq);
                console.log("AIRTIME....", this.airtimeReq);
                console.log("CONVERSION....", this.conversionReq);
                this.dataReq.forEach(data => {
                    this.dataCredit = this.dataCredit + data.amount;
                    this.dataCash = this.dataCash + data.mpesaAmount;
                });
                this.airtimeReq.forEach(data => {
                    this.airtimeCredit = this.airtimeCredit + data.airtimeAmount;
                    this.airtimeCash = this.airtimeCash + data.mpesaAmount;
                    this.airtimeProfit = Math.floor(this.airtimeCredit * 0.1);
                    this.dataProfit = Math.floor(this.dataCredit * 0.1);
                    this.totalProfit = this.airtimeProfit + this.dataProfit;
                });
                this.conversionReq.forEach(data => {
                    this.conversionAirtime = this.conversionAirtime + data.amount;
                    this.conversionCash = this.conversionCash + data.mpesaAmount;
                    if (data.mpesaAmount > 1000) {
                        this.conversionCost = this.conversionCost + 8;
                    }
                    else if (data.mpesaAmount < 1000 && data.mpesaAmount > 100) {
                        this.conversionCost = this.conversionCost + 4;
                    }
                    else {
                        // this.conversionCost = this.conversionCost  
                    }
                });
                let conv1 = this.requests3.filter((data) => {
                    return data.transType === "A2M" && data.sessionId !== "Website Request" && data.sessionId !== "App";
                });
                let air1 = this.requests3.filter((data) => {
                    return data.transType === "M2A" && data.sessionId !== "Website Request" && data.sessionId !== "App";
                });
                let data1 = this.requests3.filter((data) => {
                    return data.transType === "Data" && data.sessionId !== "Website Request" && data.sessionId !== "App";
                });
                this.conversionCost1 = (conv1.length + air1.length + data1.length) * 1.5;
                /*  conv1.forEach(data=>{

                   this.conversionCost1 = this.conversionCost1 + 1.5
                    
                        })*/
                console.log("data transaction..", this.dataCash, this.dataCredit, "profit...", this.dataProfit);
                console.log("airtime transaction..", this.airtimeCash, this.airtimeCredit, "profit...", this.airtimeProfit);
                console.log("PROFIT....", this.totalProfit);
                console.log("CONVERSION MPESA TOTAL", this.conversionCash);
                console.log("CONVERSION AIRTIME TOTAL", this.conversionAirtime);
                console.log("CONVERSION COST..", this.conversionCost);
                console.log("CONVERSION COST UNFULL + FULLFILLED..", this.conversionCost1, conv1.length, air1.length, data1.length);
                this.ref.markForCheck();
            });
        }
    }
    onInput(event) {
        console.log(event);
        const val = this.searchKey = event.target.value;
        console.log(event.target.value);
        console.log("search value is...", event.target.value);
        if (event.target.value === "") {
            console.log("nothing to search");
            this.devices = this.devices3;
            this.ref.markForCheck();
        }
        else if (val && val.trim() != '') {
            this.requestUssd.findByNameDevices(this.devices3, val)
                .then(data => {
                this.devices = data;
                this.ref.markForCheck();
            })
                .catch(error => alert(JSON.stringify(error)));
        }
    }
    onCancel(event) {
        this.devices = this.devices3;
        this.ref.markForCheck();
    }
};
FinanceComponent.ctorParameters = () => [
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _get_requests_service__WEBPACK_IMPORTED_MODULE_3__["GetRequestsService"] },
    { type: ngx_socket_io__WEBPACK_IMPORTED_MODULE_4__["Socket"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"] }
];
FinanceComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-finance',
        changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./finance.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/finance/finance/finance.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./finance.component.scss */ "./src/app/finance/finance/finance.component.scss")).default]
    })
], FinanceComponent);

/*

<ion-row>
  <ion-col>
    {{conversionAirtime}}
  </ion-col>

  <ion-col>
    {{conversionCash}}
  </ion-col>


  <ion-col>
    {{conversionCost}}
  </ion-col>

</ion-row>





<ion-row>
  <ion-col>
    {{conversionAirtime}}
  </ion-col>

  <ion-col>
    {{conversionCash}}
  </ion-col>


  <ion-col>
    {{conversionCost}}
  </ion-col>

</ion-row>

*/ 


/***/ }),

/***/ "./src/app/folder/folder-routing.module.ts":
/*!*************************************************!*\
  !*** ./src/app/folder/folder-routing.module.ts ***!
  \*************************************************/
/*! exports provided: FolderPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FolderPageRoutingModule", function() { return FolderPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _folder_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./folder.page */ "./src/app/folder/folder.page.ts");
/* harmony import */ var _sms_sms_sms_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../sms/sms/sms.component */ "./src/app/sms/sms/sms.component.ts");
/* harmony import */ var _finance_finance_finance_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../finance/finance/finance.component */ "./src/app/finance/finance/finance.component.ts");
/* harmony import */ var _user_zone_user_zone_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../user-zone/user-zone.page */ "./src/app/user-zone/user-zone.page.ts");
/* harmony import */ var _charts_charts_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../charts/charts.component */ "./src/app/charts/charts.component.ts");
/* harmony import */ var _app_adds_app_adds_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../app-adds/app-adds.component */ "./src/app/app-adds/app-adds.component.ts");









const routes = [
    {
        path: 'requests',
        component: _folder_page__WEBPACK_IMPORTED_MODULE_3__["FolderPage"],
    },
    {
        path: 'sms',
        component: _sms_sms_sms_component__WEBPACK_IMPORTED_MODULE_4__["SmsComponent"],
    },
    {
        path: 'finance',
        component: _finance_finance_finance_component__WEBPACK_IMPORTED_MODULE_5__["FinanceComponent"],
    },
    {
        path: 'userzone',
        component: _user_zone_user_zone_page__WEBPACK_IMPORTED_MODULE_6__["UserZonePage"],
    },
    {
        path: 'charts',
        component: _charts_charts_component__WEBPACK_IMPORTED_MODULE_7__["ChartsComponent"],
    },
    {
        path: 'addData',
        component: _app_adds_app_adds_component__WEBPACK_IMPORTED_MODULE_8__["AppAddsComponent"],
    }
];
let FolderPageRoutingModule = class FolderPageRoutingModule {
};
FolderPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], FolderPageRoutingModule);



/***/ }),

/***/ "./src/app/folder/folder.module.ts":
/*!*****************************************!*\
  !*** ./src/app/folder/folder.module.ts ***!
  \*****************************************/
/*! exports provided: FolderPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FolderPageModule", function() { return FolderPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _folder_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./folder-routing.module */ "./src/app/folder/folder-routing.module.ts");
/* harmony import */ var _folder_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./folder.page */ "./src/app/folder/folder.page.ts");
/* harmony import */ var _sms_sms_sms_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../sms/sms/sms.component */ "./src/app/sms/sms/sms.component.ts");
/* harmony import */ var _finance_finance_finance_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../finance/finance/finance.component */ "./src/app/finance/finance/finance.component.ts");
/* harmony import */ var _charts_charts_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../charts/charts.component */ "./src/app/charts/charts.component.ts");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/form-field.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/input.js");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/datepicker.js");
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/core */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _app_adds_app_adds_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../app-adds/app-adds.component */ "./src/app/app-adds/app-adds.component.ts");
/* harmony import */ var _edit_app_data_edit_app_data_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../edit-app-data/edit-app-data.component */ "./src/app/edit-app-data/edit-app-data.component.ts");

















let FolderPageModule = class FolderPageModule {
};
FolderPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _folder_routing_module__WEBPACK_IMPORTED_MODULE_5__["FolderPageRoutingModule"],
            _angular_material_form_field__WEBPACK_IMPORTED_MODULE_10__["MatFormFieldModule"],
            _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_12__["MatDatepickerModule"],
            _angular_material_input__WEBPACK_IMPORTED_MODULE_11__["MatInputModule"],
            _angular_material_core__WEBPACK_IMPORTED_MODULE_13__["MatNativeDateModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"]
        ],
        providers: [
            _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_12__["MatDatepickerModule"],
            _angular_material_core__WEBPACK_IMPORTED_MODULE_13__["MatNativeDateModule"]
        ],
        declarations: [_folder_page__WEBPACK_IMPORTED_MODULE_6__["FolderPage"], _sms_sms_sms_component__WEBPACK_IMPORTED_MODULE_7__["SmsComponent"], _finance_finance_finance_component__WEBPACK_IMPORTED_MODULE_8__["FinanceComponent"], _charts_charts_component__WEBPACK_IMPORTED_MODULE_9__["ChartsComponent"], _app_adds_app_adds_component__WEBPACK_IMPORTED_MODULE_14__["AppAddsComponent"], _edit_app_data_edit_app_data_component__WEBPACK_IMPORTED_MODULE_15__["EditAppDataComponent"]],
    })
], FolderPageModule);



/***/ }),

/***/ "./src/app/folder/folder.page.scss":
/*!*****************************************!*\
  !*** ./src/app/folder/folder.page.scss ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-menu-button {\n  color: var(--ion-color-primary);\n}\n\n#container {\n  text-align: center;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 50%;\n  transform: translateY(-50%);\n}\n\n#container strong {\n  font-size: 20px;\n  line-height: 26px;\n}\n\n#container p {\n  font-size: 16px;\n  line-height: 22px;\n  color: #8c8c8c;\n  margin: 0;\n}\n\n#container a {\n  text-decoration: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZm9sZGVyL2ZvbGRlci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSwrQkFBQTtBQUNGOztBQUVBO0VBQ0Usa0JBQUE7RUFDQSxrQkFBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0VBQ0EsUUFBQTtFQUNBLDJCQUFBO0FBQ0Y7O0FBRUE7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QUFDRjs7QUFFQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxTQUFBO0FBQ0Y7O0FBRUE7RUFDRSxxQkFBQTtBQUNGIiwiZmlsZSI6InNyYy9hcHAvZm9sZGVyL2ZvbGRlci5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tbWVudS1idXR0b24ge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xufVxuXG4jY29udGFpbmVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDA7XG4gIHJpZ2h0OiAwO1xuICB0b3A6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xufVxuXG4jY29udGFpbmVyIHN0cm9uZyB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgbGluZS1oZWlnaHQ6IDI2cHg7XG59XG5cbiNjb250YWluZXIgcCB7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgbGluZS1oZWlnaHQ6IDIycHg7XG4gIGNvbG9yOiAjOGM4YzhjO1xuICBtYXJnaW46IDA7XG59XG5cbiNjb250YWluZXIgYSB7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/folder/folder.page.ts":
/*!***************************************!*\
  !*** ./src/app/folder/folder.page.ts ***!
  \***************************************/
/*! exports provided: FolderPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FolderPage", function() { return FolderPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _get_requests_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../get-requests.service */ "./src/app/get-requests.service.ts");
/* harmony import */ var ngx_socket_io__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-socket-io */ "./node_modules/ngx-socket-io/__ivy_ngcc__/fesm2015/ngx-socket-io.js");
/* harmony import */ var _service_pwaupdate_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../service/pwaupdate.service */ "./src/app/service/pwaupdate.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _request_details_request_details_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../request-details/request-details.page */ "./src/app/request-details/request-details.page.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../auth/auth.service */ "./src/app/auth/auth.service.ts");













let FolderPage = class FolderPage {
    constructor(auth, menuCtrl, storage, router, activatedRoute, requestUssd, socket, ref, pwmIpdeter, modalController) {
        this.auth = auth;
        this.menuCtrl = menuCtrl;
        this.storage = storage;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.requestUssd = requestUssd;
        this.socket = socket;
        this.ref = ref;
        this.pwmIpdeter = pwmIpdeter;
        this.modalController = modalController;
        this.searchKey = "";
        this.requests = [];
        this.requests3 = [];
        this.M2A = [];
        this.A2M = [];
        this.Data = [];
        this.M2A3 = [];
        this.A2M3 = [];
        this.Data3 = [];
        this.states = "A2M";
        this.filterManual = false;
        this.range = new _angular_forms__WEBPACK_IMPORTED_MODULE_10__["FormGroup"]({
            start: new _angular_forms__WEBPACK_IMPORTED_MODULE_10__["FormControl"](),
            end: new _angular_forms__WEBPACK_IMPORTED_MODULE_10__["FormControl"]()
        });
    }
    ngOnInit() {
        // console.log("token data is...", this.auth.getToken());
        this.minDate = new Date(21, 9, 10);
        this.folder = this.activatedRoute.snapshot.paramMap.get('id');
        this.storage.get('token').then(data => {
            console.log("token data is...", data);
            this.requestUssd.httpOptions = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpHeaders"]({ 'Content-Type': 'application/json', 'amness': data })
            };
        }).catch(err => {
            console.log("error occured ...", err);
            this.router.navigate(['/login']);
        });
        this.storage.get('authenticated').then(data => {
            if (!data) {
                this.router.navigate(['/login']);
            }
        });
        this.socket.on('connect', () => {
            console.log('Successfully connected !');
        });
    }
    ionViewWillEnter() {
        this.menuCtrl.enable(true);
        this.menuCtrl.swipeGesture(true);
        //  this.auth.isAuthenticated()
        if (this.auth.isAuthenticated(localStorage.getItem("token"))) {
            console.log("token expired");
            this.router.navigate(['/login']);
            this.storage.clear();
            localStorage.clear();
        }
        else {
            console.log("token NOT expired" + '\n ' + localStorage.getItem("token"));
            this.findAll();
        }
    }
    ionViewWillLeave() {
        this.reqSub.unsubscribe();
    }
    onInput(event) {
        console.log(event);
        const val = this.searchKey = event.target.value;
        console.log(event.target.value);
        console.log("search value is...", event.target.value);
        if (event.target.value === "") {
            console.log("nothing to search");
            this.requests = this.requests3;
            this.A2M = this.A2M3;
            this.M2A = this.M2A3;
            this.Data = this.Data3;
            this.ref.markForCheck();
        }
        else if (val && val.trim() != '') {
            /*  this.requestUssd.findByName(this.requests3 ,this.searchKey)
              .then(data => {
                  this.requests = data;
                  console.log(data);
                  this.ref.markForCheck();
                  
              })
              .catch(error => alert(JSON.stringify(error)));
          */
            //  if(this.states === "A2M"){
            this.requestUssd.findByName(this.A2M3, val)
                .then(data => {
                this.A2M = data;
                console.log(data);
            })
                .catch(error => alert(JSON.stringify(error)));
            //  }else if(this.states === "M2A"){
            this.requestUssd.findByName(this.M2A3, val)
                .then(data => {
                this.M2A = data;
                console.log(data);
            })
                .catch(error => alert(JSON.stringify(error)));
            // } else if(this.states === "Data"){
            this.requestUssd.findByName(this.Data3, val)
                .then(data => {
                this.Data = data;
                console.log(data);
            })
                .catch(error => alert(JSON.stringify(error)));
            //  }
        }
    }
    onCancel(event) {
        this.requests = this.requests3;
        this.ref.markForCheck();
    }
    findAll() {
        if (this.auth.isAuthenticated(localStorage.getItem("token"))) {
            console.log("token expired");
            this.router.navigate(['/login']);
            this.storage.clear();
            localStorage.clear();
        }
        else {
            console.log("token NOT expired" + '\n ' + localStorage.getItem("token"));
            this.reqSub = this.requestUssd.findAll().subscribe((data) => {
                console.log(data);
                /*
                  err:
                expiredAt: "2021-09-09T16:23:06.000Z"
                message: "jwt expired"
                */
                if (data.msg === "invalid") {
                    console.log("token expired");
                    this.router.navigate(['/login']);
                    this.storage.clear();
                    localStorage.clear();
                }
                else {
                    this.requests3 = data.data;
                    this.requests = data.data;
                    this.seperateOrders(this.requests3);
                    this.ref.markForCheck();
                    console.log("data available");
                }
            }, error => {
                console.log(error);
                this.router.navigate(['/login']);
                this.storage.clear();
                localStorage.clear();
            });
        }
    }
    getUsersList(event) {
        this.requestUssd.findAll().subscribe((data) => {
            this.requests3 = data.data;
            this.requests = data.data;
            this.seperateOrders(this.requests3);
            this.ref.markForCheck();
            event.target.complete();
            console.log(data);
        }, error => {
            console.log(error);
            event.target.complete();
            this.router.navigate(['/login']);
            this.storage.clear();
            localStorage.clear();
        });
    }
    completTrans(req) {
        console.log(req._id);
        this.requestUssd.complete(req._id).subscribe((data) => {
            this.findAll();
            console.log(data);
        });
    }
    sendMoney(req) {
        console.log(req._id);
        this.requestUssd.sendMoney(req._id).subscribe((data) => {
            this.findAll();
            console.log(data);
        });
    }
    sendCredit(req) {
        console.log(req._id);
        this.requestUssd.sendCredit(req._id).subscribe((data) => {
            this.findAll();
            console.log(data);
        });
    }
    presentModal(data) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            // console.log(data);
            const modal = yield this.modalController.create({
                component: _request_details_request_details_page__WEBPACK_IMPORTED_MODULE_7__["RequestDetailsPage"],
                cssClass: 'my-custom-class',
                showBackdrop: true,
                swipeToClose: true,
                animated: true,
                componentProps: { req: data }
            });
            return yield modal.present();
        });
    }
    seperateOrders(data) {
        //"Pending Payment Aproval" //Pending Payment Aproval //Paid //Shipped//Fullfilled
        console.log(data, "orders...");
        this.A2M = data.filter((requests) => {
            return (requests.transType) === ("A2M");
        });
        this.M2A = data.filter((requests) => {
            return (requests.transType) === ("M2A");
        });
        this.Data = data.filter((requests) => {
            return (requests.transType) === ("Data");
        });
        this.A2M3 = this.A2M;
        this.M2A3 = this.M2A;
        this.Data3 = this.Data;
        console.log(this.A2M);
    }
    getByDate() {
        let date = this.range.value;
        if (date.start === null) {
            console.log("please set date");
        }
        else if (date.end === null) {
            var startDate = new Date(date.start);
            var endDate = new Date(); //date.end
            console.log("start", startDate, "End", endDate);
            let query = {
                start: startDate,
                end: endDate,
            };
            this.requestUssd.getByDate(query).subscribe((data) => {
                //  this.ref.markForCheck();
                console.log("REQUESTED DATA WHEN END IS NULL IS...", data);
                if (data.msg === "invalid") {
                    console.log("token expired");
                    this.router.navigate(['/login']);
                    this.storage.clear();
                    localStorage.clear();
                }
                else {
                    this.requests3 = data.data;
                    this.requests = data.data;
                    this.seperateOrders(this.requests3);
                    this.ref.markForCheck();
                    console.log("data available");
                }
            }, error => {
                console.log(error);
                this.router.navigate(['/login']);
                this.storage.clear();
                localStorage.clear();
            });
        }
        else {
            /*
             let startDate = new Date(date.start).valueOf();
                    let endDate = new Date(date.end).valueOf();
            */
            let startDate = new Date(date.start);
            let endDate = new Date(date.end);
            console.log("start", startDate, "End", endDate);
            let query = {
                start: startDate,
                end: endDate,
            };
            this.requestUssd.getByDate(query).subscribe((data) => {
                //  this.ref.markForCheck();
                console.log("REQUESTED DATA WHEN END IS NULL IS...", data);
                if (data.msg === "invalid") {
                    console.log("token expired");
                    this.router.navigate(['/login']);
                    this.storage.clear();
                    localStorage.clear();
                }
                else {
                    this.requests3 = data.data;
                    this.requests = data.data;
                    this.seperateOrders(this.requests3);
                    this.ref.markForCheck();
                    console.log("data available");
                }
            }, error => {
                console.log(error);
                this.router.navigate(['/login']);
                this.storage.clear();
                localStorage.clear();
            });
        }
        if (this.auth.isAuthenticated(localStorage.getItem("token"))) {
            console.log("token expired");
            this.router.navigate(['/login']);
            this.storage.clear();
            localStorage.clear();
        }
        else {
            console.log("token NOT expired" + '\n ' + localStorage.getItem("token"));
        }
    }
    placeFilters() {
        if (this.filterManual) {
            console.log("filter is..", this.filterManual);
            this.A2M = this.A2M.filter((requests) => {
                return (requests.manual) === (true);
            });
            this.M2A = this.M2A.filter((requests) => {
                return (requests.manual) === (true);
            });
            this.Data = this.Data.filter((requests) => {
                return (requests.manual) === (true);
            });
            console.log(" first", this.A2M);
        }
        else {
        }
        console.log(" last", this.A2M);
        this.A2M3 = this.A2M;
        this.M2A3 = this.M2A;
        this.Data3 = this.Data;
        this.ref.markForCheck();
    }
    clearFilters() {
        this.filterManual = false;
        this.seperateOrders(this.requests);
        this.ref.markForCheck();
    }
};
FolderPage.ctorParameters = () => [
    { type: _auth_auth_service__WEBPACK_IMPORTED_MODULE_11__["AuthService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["MenuController"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_8__["Storage"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _get_requests_service__WEBPACK_IMPORTED_MODULE_3__["GetRequestsService"] },
    { type: ngx_socket_io__WEBPACK_IMPORTED_MODULE_4__["Socket"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
    { type: _service_pwaupdate_service__WEBPACK_IMPORTED_MODULE_5__["PwaupdateService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"] }
];
FolderPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-folder',
        changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./folder.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/folder/folder.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./folder.page.scss */ "./src/app/folder/folder.page.scss")).default]
    })
], FolderPage);

//254740092818  254769250037  254748387837   254740734030 


/***/ }),

/***/ "./src/app/sms/sms/sms.component.scss":
/*!********************************************!*\
  !*** ./src/app/sms/sms/sms.component.scss ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Ntcy9zbXMvc21zLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/sms/sms/sms.component.ts":
/*!******************************************!*\
  !*** ./src/app/sms/sms/sms.component.ts ***!
  \******************************************/
/*! exports provided: SmsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SmsComponent", function() { return SmsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _get_requests_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../get-requests.service */ "./src/app/get-requests.service.ts");
/* harmony import */ var ngx_socket_io__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-socket-io */ "./node_modules/ngx-socket-io/__ivy_ngcc__/fesm2015/ngx-socket-io.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");







let SmsComponent = class SmsComponent {
    constructor(activatedRoute, storage, router, requestUssd, socket, ref) {
        this.activatedRoute = activatedRoute;
        this.storage = storage;
        this.router = router;
        this.requestUssd = requestUssd;
        this.socket = socket;
        this.ref = ref;
        this.searchKey = "";
        this.device = "";
        this.myDevices = [];
        this.sms = [];
        this.title = "";
    }
    ngOnInit() {
        this.storage.get('authenticated').then(data => {
            if (!data) {
                this.router.navigate(['/login']);
            }
        });
        this.socket.on('connect', () => {
            console.log('Successfully connected !');
        });
        this.socket.on('connect', () => {
            console.log('Successfully connected !');
            this.socket.emit('dashRegister');
        });
        this.socket.on('dashSms', (data) => {
            if (data.client === this.searchKey) {
                console.log('Received sms!', data);
                if (data.responce === "success") {
                    this.sms = data.sms;
                    this.ref.markForCheck();
                }
                else {
                    this.sms = data.sms;
                    this.ref.markForCheck();
                    this.title = data.responce;
                }
            }
        });
        this.requestUssd.findDevices({ name: null }).subscribe((data) => {
            this.myDevices = data.data;
            console.log("myDevices are..", this.myDevices);
            if (data.msg === "invalid") {
                console.log("token expired");
                this.router.navigate(['/login']);
                this.storage.clear();
            }
            else {
            }
        });
    }
    sendReq() {
        console.log("clicked retrive sms...");
        this.socket.emit("retriveSms", { phone: this.device, client: this.searchKey });
    }
    onCancel() {
        this.searchKey = "";
    }
};
SmsComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _get_requests_service__WEBPACK_IMPORTED_MODULE_3__["GetRequestsService"] },
    { type: ngx_socket_io__WEBPACK_IMPORTED_MODULE_4__["Socket"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] }
];
SmsComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-sms',
        changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./sms.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/sms/sms/sms.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./sms.component.scss */ "./src/app/sms/sms/sms.component.scss")).default]
    })
], SmsComponent);



/***/ })

}]);
//# sourceMappingURL=folder-folder-module-es2015.js.map