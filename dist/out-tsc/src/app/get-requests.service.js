import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../environments/environment';
let url = environment.url;
let httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
let GetRequestsService = class GetRequestsService {
    constructor(http) {
        this.http = http;
    }
    findAll() {
        return this.http.get(`${url}/web/allrequests`).pipe(map(this.extractData), catchError(this.handleError));
    }
    extractData(res) {
        let body = res;
        return body || {};
    }
    handleError(error) {
        let errMsg;
        if (error instanceof Response) {
            const err = error || '';
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
};
GetRequestsService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], GetRequestsService);
export { GetRequestsService };
//# sourceMappingURL=get-requests.service.js.map