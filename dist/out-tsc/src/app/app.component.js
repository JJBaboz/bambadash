import { __decorate } from "tslib";
import { Component } from '@angular/core';
let AppComponent = class AppComponent {
    constructor(platform, splashScreen, statusBar) {
        this.platform = platform;
        this.splashScreen = splashScreen;
        this.statusBar = statusBar;
        this.selectedIndex = 0;
        this.appPages = [
            {
                title: 'All Requests',
                url: '/folder/Inbox',
                icon: 'mail'
            },
        ];
        /*
        {
            title: 'Outbox',
            url: '/folder/Outbox',
            icon: 'paper-plane'
          },
          {
            title: 'Favorites',
            url: '/folder/Favorites',
            icon: 'heart'
          },
          {
            title: 'Archived',
            url: '/folder/Archived',
            icon: 'archive'
          },
          {
            title: 'Trash',
            url: '/folder/Trash',
            icon: 'trash'
          },
          {
            title: 'Spam',
            url: '/folder/Spam',
            icon: 'warning'
          }
      
        */
        this.labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];
        this.initializeApp();
    }
    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
    }
    ngOnInit() {
        const path = window.location.pathname.split('folder/')[1];
        if (path !== undefined) {
            this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
        }
    }
};
AppComponent = __decorate([
    Component({
        selector: 'app-root',
        templateUrl: 'app.component.html',
        styleUrls: ['app.component.scss']
    })
], AppComponent);
export { AppComponent };
//# sourceMappingURL=app.component.js.map