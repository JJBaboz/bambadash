import { __decorate } from "tslib";
import { Component } from '@angular/core';
let FolderPage = class FolderPage {
    constructor(activatedRoute, requestUssd) {
        this.activatedRoute = activatedRoute;
        this.requestUssd = requestUssd;
        this.searchKey = "";
        this.requests = [];
    }
    ngOnInit() {
        this.folder = this.activatedRoute.snapshot.paramMap.get('id');
        this.requestUssd.findAll().subscribe((data) => {
            this.requests = data.data;
            console.log(data);
        });
    }
};
FolderPage = __decorate([
    Component({
        selector: 'app-folder',
        templateUrl: './folder.page.html',
        styleUrls: ['./folder.page.scss'],
    })
], FolderPage);
export { FolderPage };
//# sourceMappingURL=folder.page.js.map