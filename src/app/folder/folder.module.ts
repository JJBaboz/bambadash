import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FolderPageRoutingModule } from './folder-routing.module';

import { FolderPage } from './folder.page';
import { SmsComponent } from '../sms/sms/sms.component';
import { FinanceComponent } from '../finance/finance/finance.component';
import { ChartsComponent } from '../charts/charts.component';

import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import {  ReactiveFormsModule} from '@angular/forms';
import { AppAddsComponent } from '../app-adds/app-adds.component';
import { EditAppDataComponent } from '../edit-app-data/edit-app-data.component';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FolderPageRoutingModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule, 
    ReactiveFormsModule,
    FormsModule
    
  ],
  providers: [  
    MatDatepickerModule,
    MatNativeDateModule  
  ],
  declarations: [FolderPage,SmsComponent,FinanceComponent,ChartsComponent,AppAddsComponent,EditAppDataComponent],
  
})
export class FolderPageModule {

  
}
