import { Component, OnInit ,ChangeDetectionStrategy, ChangeDetectorRef,} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GetRequestsService } from '../get-requests.service';
import { Socket } from 'ngx-socket-io';
import { PwaupdateService } from '../service/pwaupdate.service';
import { MenuController, ModalController } from '@ionic/angular';
import { RequestDetailsPage } from '../request-details/request-details.page';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { HttpHeaders } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';
import {FormGroup, FormControl} from '@angular/forms';
import { AuthService } from '../auth/auth.service';



@Component({
  selector: 'app-folder',
  changeDetection: ChangeDetectionStrategy.OnPush,
  
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit {
  public folder: string;
  searchKey: string = "";
  requests : any[] = [];
  requests3: any[] = [];


 

  M2A:any[] = []

  A2M:any[] = []

  Data:any[] = []


 
  M2A3:any[] = []

  A2M3:any[] = []

  Data3:any[] = []

  states: string = "A2M"


  filterManual = false

 reqSub:Subscription 

 minDate: Date;
 maxDate: Date;

 range = new FormGroup({
   start: new FormControl(),
   end: new FormControl()
 });



  constructor(public auth:AuthService, public menuCtrl: MenuController,private storage:Storage  ,public router: Router,private activatedRoute: ActivatedRoute, 
    private requestUssd:GetRequestsService,private socket:Socket, private ref: ChangeDetectorRef,
    private pwmIpdeter:PwaupdateService,
    public modalController: ModalController
    
    ) { }

  ngOnInit() {

   // console.log("token data is...", this.auth.getToken());
   this.minDate = new Date(21, 9, 10);
 





  this.folder = this.activatedRoute.snapshot.paramMap.get('id');

   
     
    
this.storage.get('token').then(data =>{

  console.log("token data is...", data);

  this.requestUssd.httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json', 'amness':data })
  };

  
}).catch(err=>{

  console.log("error occured ...",err);
  


  this.router.navigate(['/login'])

})


    this.storage.get('authenticated').then(data=>{

if(!data){

  this.router.navigate(['/login'])

}

    })

   


    this.socket.on('connect', () => {

      console.log('Successfully connected !');
  
   
  
          })
    
  }


  ionViewWillEnter() {
    this.menuCtrl.enable(true);
    this.menuCtrl.swipeGesture(true)

  //  this.auth.isAuthenticated()

  

  if(this.auth.isAuthenticated(localStorage.getItem("token"))){

    console.log("token expired")

    this.router.navigate(['/login'])

    this.storage.clear()
    
    localStorage.clear()

  }else{

    console.log("token NOT expired" + '\n ' + localStorage.getItem("token"))

    this.findAll()
  }

   }

   ionViewWillLeave() {
 
    this.reqSub.unsubscribe()

   }
  
	onInput(event) {

    console.log(event);
    const val =  this.searchKey = event.target.value;

    console.log(event.target.value);
    console.log("search value is...",event.target.value);
    
    
   if(event.target.value === ""){


    console.log("nothing to search");

     this.requests = this.requests3;

     this.A2M = this.A2M3

     this.M2A = this.M2A3

     this.Data = this.Data3 


     this.ref.markForCheck();
     
   }else if(val && val.trim() != ''){

  /*  this.requestUssd.findByName(this.requests3 ,this.searchKey)
    .then(data => {
        this.requests = data;
        console.log(data);
        this.ref.markForCheck();
        
    })
    .catch(error => alert(JSON.stringify(error)));
*/

  //  if(this.states === "A2M"){


      this.requestUssd.findByName(this.A2M3 ,val)
      .then(data => {
          this.A2M = data;
          console.log(data);
          
          
      })
      .catch(error => alert(JSON.stringify(error)));


  //  }else if(this.states === "M2A"){

      this.requestUssd.findByName(this.M2A3 ,val)
      .then(data => {
          this.M2A = data;
          console.log(data);
          
          
      })
      .catch(error => alert(JSON.stringify(error)));


   // } else if(this.states === "Data"){

      this.requestUssd.findByName(this.Data3 ,val)
      .then(data => {
          this.Data = data;
          console.log(data);
          
          
      })
      .catch(error => alert(JSON.stringify(error)));


  //  }




  
  }



}



onCancel(event) {
  this.requests = this.requests3;
   this.ref.markForCheck();
}



findAll(){

  if(this.auth.isAuthenticated(localStorage.getItem("token"))){

    console.log("token expired")

    this.router.navigate(['/login'])

    this.storage.clear()
    
    localStorage.clear()

  }else{

    console.log("token NOT expired" + '\n ' + localStorage.getItem("token"))


    
 this.reqSub =  this.requestUssd.findAll().subscribe((data)=>{

  console.log(data);
/*
  err:
expiredAt: "2021-09-09T16:23:06.000Z"
message: "jwt expired"
*/

  if (data.msg === "invalid" ) {
   
  console.log("token expired");

  this.router.navigate(['/login'])

this.storage.clear()

localStorage.clear()
    
  } else {


    this.requests3 = data.data
    this.requests = data.data

   this.seperateOrders(this.requests3 )

    this.ref.markForCheck();
    console.log("data available");

  
  }

  
  
}, error =>{

  console.log(error);
  

  this.router.navigate(['/login'])

  this.storage.clear()

  localStorage.clear()

})

  }


}


getUsersList(event) {
  this.requestUssd.findAll().subscribe((data)=>{
    this.requests3 = data.data
    this.requests = data.data

    this.seperateOrders(this.requests3 )
    this.ref.markForCheck();

    event.target.complete();
    console.log(data);
    
  },error=>{

    console.log(error);
    
    
    event.target.complete();

    this.router.navigate(['/login'])

    this.storage.clear()
    
    localStorage.clear()



  })


  
}






completTrans(req){

  console.log(req._id);
  
  this.requestUssd.complete(req._id).subscribe((data)=>{
  this.findAll()
  
    console.log(data);
    
  })

}




sendMoney(req){

  console.log(req._id);
  
  this.requestUssd.sendMoney(req._id).subscribe((data)=>{
  this.findAll()
  
    console.log(data);
    
  })

}



sendCredit(req){

  console.log(req._id);
  
  this.requestUssd.sendCredit(req._id).subscribe((data)=>{
  this.findAll()
  
    console.log(data);
    
  })

}


async presentModal(data) {

 // console.log(data);
  
  const modal = await this.modalController.create({
    component: RequestDetailsPage,
    cssClass: 'my-custom-class',
    showBackdrop:true,
    swipeToClose: true,
    animated:true,
    componentProps: {req:data}
  });
  return await modal.present();
}





seperateOrders(data) {

  //"Pending Payment Aproval" //Pending Payment Aproval //Paid //Shipped//Fullfilled

  console.log(data,"orders...");

this.A2M = data.filter((requests: any) =>{


  return (requests.transType) === ("A2M")
  })

  this.M2A = data.filter((requests: any) =>{

    return (requests.transType) === ("M2A")
    })


    this.Data = data.filter((requests: any) =>{

      return (requests.transType) === ("Data")
      })


     

      this.A2M3 = this.A2M 

      this.M2A3 = this.M2A 

      this.Data3 = this.Data 


console.log(this.A2M);



}


getByDate(){


  let date = this.range.value

  if (date.start === null) {
  
    console.log("please set date");
    
    
  } else if (date.end === null) {
  
  
  
    
  
    var startDate = new Date(date.start);
          var endDate = new Date();  //date.end
  
          console.log("start",startDate ,"End",endDate);
  
      
  
          let query = {
            start:startDate,
            end:endDate,
          
  }
  
  this.requestUssd.getByDate(query).subscribe((data)=>{
   
    //  this.ref.markForCheck();
      console.log("REQUESTED DATA WHEN END IS NULL IS...",data);
    
      if (data.msg === "invalid" ) {

        console.log("token expired");
      
        this.router.navigate(['/login'])
      
      this.storage.clear()
      
      localStorage.clear()
          
        } else {
      
      
          this.requests3 = data.data
          this.requests = data.data
      
         this.seperateOrders(this.requests3 )
      
          this.ref.markForCheck();
          console.log("data available");
      
        
        }
      
    
  
      
    } ,error =>{

      console.log(error);
      
    
      this.router.navigate(['/login'])
    
      this.storage.clear()
    
      localStorage.clear()
    
    })
    
  }else{
  /*
   let startDate = new Date(date.start).valueOf();
          let endDate = new Date(date.end).valueOf();
  */
  
          let startDate = new Date(date.start)
          let endDate = new Date(date.end)
  
          console.log("start",startDate ,"End",endDate);
  
          let query = {
                    start:startDate,
                    end:endDate,      
          }
          
  
          this.requestUssd.getByDate(query).subscribe((data)=>{
   
            //  this.ref.markForCheck();
              console.log("REQUESTED DATA WHEN END IS NULL IS...",data);
            
              if (data.msg === "invalid" ) {
   
                console.log("token expired");
              
                this.router.navigate(['/login'])
              
              this.storage.clear()
              
              localStorage.clear()
                  
                } else {
              
              
                  this.requests3 = data.data
                  this.requests = data.data
              
                 this.seperateOrders(this.requests3 )
              
                  this.ref.markForCheck();
                  console.log("data available");
              
                
                }
              
            
          
              
            } ,error =>{

              console.log(error);
              
            
              this.router.navigate(['/login'])
            
              this.storage.clear()
            
              localStorage.clear()
            
            })
              
  
  }




  if(this.auth.isAuthenticated(localStorage.getItem("token"))){

    console.log("token expired")

    this.router.navigate(['/login'])

    this.storage.clear()
    
    localStorage.clear()

  }else{

    console.log("token NOT expired" + '\n ' + localStorage.getItem("token"))


    


  
  

  }





}



placeFilters(){

  if (this.filterManual) {
 
    console.log("filter is..", this.filterManual);
    

    this.A2M = this.A2M.filter((requests: any) =>{
    
    
      return (requests.manual) === (true)
      })
    
      this.M2A = this.M2A.filter((requests: any) =>{
    
        return (requests.manual) === (true)
        })
    
    
        this.Data =  this.Data.filter((requests: any) =>{
    
          return (requests.manual) === (true)
          })
    
    
         
  
    
    console.log(" first",this.A2M);
    
    

  } else {
    
  }

  console.log(" last",this.A2M);

  this.A2M3 = this.A2M 
    
  this.M2A3 = this.M2A 

  this.Data3 = this.Data
  this.ref.markForCheck();




}

clearFilters(){

  this.filterManual = false

  this.seperateOrders(this.requests)

  this.ref.markForCheck();
}


}


//254740092818  254769250037  254748387837   254740734030 