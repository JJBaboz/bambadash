import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FolderPage } from './folder.page';
import { SmsComponent } from '../sms/sms/sms.component';
import { FinanceComponent } from '../finance/finance/finance.component';
import { UserZonePage } from '../user-zone/user-zone.page';
import { ChartsComponent } from '../charts/charts.component';
import { AppAddsComponent } from '../app-adds/app-adds.component';
const routes: Routes = [
  {
    path: 'requests',
    component: FolderPage,
  },
  {
    path: 'sms',
    component: SmsComponent,
    
  },
  {
    path: 'finance',
    component: FinanceComponent,
    
  },
  {
    path: 'userzone',
    component: UserZonePage,
    
  },
  {
    path: 'charts',
    component: ChartsComponent,
    
  },
  {
    path: 'addData',
    component: AppAddsComponent,
    
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FolderPageRoutingModule {}
