
import { LoadingController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import {  NavController, AlertController, ToastController, MenuController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { GetRequestsService } from '../../get-requests.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  
  public onLoginForm: FormGroup;
  public onRegisterForm: FormGroup;
  auth: string = "login";

  constructor(private storage:Storage  ,public router: Router,private _fb: FormBuilder,
		public nav: NavController, public forgotCtrl: AlertController,
    public menu: MenuController, public toastCtrl: ToastController, 
    
    public loadingController: LoadingController,
    public authService: GetRequestsService
    ) { 

     // this.menu.swipeEnable(false);
      this.menu.enable(false);
    }

  ngOnInit() {

    localStorage.clear()

    this.onLoginForm = this._fb.group({

      phone: ['', Validators.compose([
        Validators.required,Validators.minLength(10),Validators.maxLength(13)
			])],


      password: ['', Validators.compose([
        Validators.required
			])]


    });



    this.onRegisterForm = this._fb.group({

      name: ['', Validators.compose([
        Validators.required,Validators.minLength(4)
			])],

      email: ['', Validators.compose([
       Validators.email
			])],

      phone: ['', Validators.compose([
        Validators.required,Validators.minLength(10),Validators.maxLength(10)
			])]
			,
      password: ['', Validators.compose([
        Validators.required,Validators.minLength(6)
			])]


    });

  }


  
  async register() {

		console.log(this.onRegisterForm.value);




    const loading = await this.loadingController.create({

      cssClass: 'my-custom-class',
      message: "Registering User...",
      spinner:"crescent"
    });
    
    
    await loading.present();



		this.authService.register(JSON.stringify(this.onRegisterForm.value)).subscribe((data:any) =>{

		console.log(data);

		if(data.success){

      loading.dismiss();

    this.presentToast("User was added successfully","bottom");
    
    this.router.navigate(['/login']);
    
		}	else {

      this.presentToast("User registration error","bottom");

        loading.dismiss();


        this.router.navigate(['/login']);

			this.presentToast(data.msg, "bottom");


		}



		}, error =>{

      loading.dismiss();
			this.presentToast("An error occurred please try again", "bottom")
			console.log(error);



		})




   }



  // login and go to home page
  async login() {

console.log(this.onLoginForm.value);

const loading = await this.loadingController.create({

  cssClass: 'my-custom-class',
  message: 'Please wait...',
  spinner:"crescent"
});


await loading.present();

/*
const { role, data } = await loading.onDidDismiss();
console.log('Loading dismissed!');
*/

let sub = this.authService.login(JSON.stringify(this.onLoginForm.value)).subscribe((data:any)=>{

console.log(data);

if(data.success){


	let prof:any = {profile:data.result} ; //API TO GET AUTH


	if(prof){

	this.storage.set('profile', prof )
  
  this.storage.set('token',data.token.split(" ")[1] )

  console.log("token is....", data.token);
  localStorage.setItem("token",data.token.split(" ")[1] );
/*
  this.authService.httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json', 'amness':data })
  };*/



  this.storage.set('authenticated',true ).then(data=>{
 //   const path = window.location.pathname.split('dash/')[1];

 //setTimeout(() => {

  this.router.navigate(['/dash/requests' ]);
  
 //}, 10000);


    
  } )

 loading.dismiss();

	sub.unsubscribe()

//	this.nav.setRoot('page-restaurant-list', {"data": prof, "from":"loginAuth", "auth": true});



	}


} else{


  loading.dismiss();

  this.storage.set('profile', null )
  this.storage.set('authenticated',false )
  this.storage.set('token',null )

this.presentToast("Incorrect phone number and or password" , "middle")


sub.unsubscribe()

}



}, err =>{

console.log(err);
 loading.dismiss();

this.presentToast("An Error ocurred please try again" , "middle")



})


	}


 async forgotPass() {
/*
    let forgot = this.forgotCtrl.create({

      title: 'Forgot Password?',
      message: "Enter you email address to send a reset link password.",
      inputs: [
        {
          name: 'email',
          placeholder: 'Email',
          type: 'email'
        },
			],

      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Send',
          handler: data => {
            console.log('Send clicked');
            let toast = this.toastCtrl.create({
              message: 'Email was sended successfully',
              duration: 3000,
              position: 'top',
              cssClass: 'dark-trans',
              closeButtonText: 'OK',
              showCloseButton: true
            });
           (await toast).present();
          }
        }
      ]
    });
    forgot.present();

    */
	}


async	presentToast(message:string, pos:any) {





  
    const toast = this.toastCtrl.create({
      message: message,
			duration: 4000,
			position:pos
    });
    
     (await toast).present

   

 
  }




}
