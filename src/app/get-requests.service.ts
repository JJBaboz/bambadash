import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable,throwError } from 'rxjs';
import { map, tap, catchError } from 'rxjs/operators';
import {environment} from '../environments/environment'
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';


//let url = environment.url3;


let url = environment.url2;

let httpOptions3 = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json'})
};


@Injectable({
  providedIn: 'root'
})



export class GetRequestsService {

  httpOptions= {
    headers: new HttpHeaders({ 'Content-Type': 'application/json'})
  };
  
  constructor(public router: Router,private storage:Storage  ,public http:HttpClient) {



   }


   clientContacts(): Observable<any> {
    return this.http.post(`${url}/web/getdistinct`,{},this.httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }


  sendSms(phone,message,recepient): Observable<any> {
    return this.http.post(`${url}/web/sendSms`,{phone:phone,message:message,recepient:recepient},this.httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }
  


  getByDate(query): Observable<any> {
    return this.http.post(`${url}/web/getByDate`,{query:query},this.httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }


  
   login(data:any){

    return this.http.post(`${url}/auth/login`,data,httpOptions3).pipe(
  
      map(this.extractData),
  
      catchError(this.handleError)
  
      );
  
  
  }


  
register(data:any){

  return this.http.post(`${url}/auth/register`,data,httpOptions3).pipe(
  
  map(this.extractData),
  
  catchError(this.handleError)
  
  );
  
  
  }


   findAll(): Observable<any> {

    
    
    return this.http.get(`${url}/web/allrequests`,this.httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  //post
  findDevices(device): Observable<any> {
    return this.http.post(`${url}/web/phoneBalance`,device,this.httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }


  getUssdBal(): Observable<any> {
    return this.http.get(`${url}/sms/balance`,this.httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  mpesaBalance(transType): Observable<any> {
    return this.http.post(`${url}/web/mpesaBalance`,{type:transType},this.httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }



  sendDist(req): Observable<any> {
    return this.http.post(`${url}/web/sendDistributor`,req,this.httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }


  mpesaBalance3(transType): Observable<any> {
    return this.http.post(`${url}/web/bcbalance`,{type:transType},this.httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  complete(myId): Observable<any> {
    return this.http.post(`${url}/web/markComplete`,myId,this.httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }


  sendMoney(myId): Observable<any> {
    return this.http.post(`${url}/web/sendMoney`,myId,this.httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }


  sendMoneyApproval(myId): Observable<any> {
    return this.http.post(`${url}/web/sendMoneyApproval`,myId,this.httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }


  sendData(myId): Observable<any> {
    return this.http.post(`${url}/web/sendData`,myId,this.httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }


  
  sendCredit(myId): Observable<any> {
    return this.http.post(`${url}/web/sendCredit`,myId,this.httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }
  

  
  findByName(requests,searchKey: string) {

    let key: string = searchKey.toUpperCase();
    return Promise.resolve(requests.filter((requests: any) =>

		(requests.sourceID + ' ' +  requests.source + ' ' + requests._id +  ' ' + requests.phone +  ' ' + requests.transType +  ' ' + requests.state +  ' ' + requests.sessionId).toUpperCase().indexOf(key) > -1));

	}

  findByNameDevices(requests,searchKey: string) {

    let key: string = searchKey.toUpperCase();
    return Promise.resolve(requests.filter((requests: any) =>

		( requests._id +  ' ' + requests.phone +  ' ' + requests.name +  ' ' + requests.state ).toUpperCase().indexOf(key) > -1));

	}






   private extractData(res: Response) {
    let body = res;
    return body || {};
  }

  private handleError (error: Response | any) {
    let errMsg: string;
   

    if (error instanceof Response) { //TokenExpiredError jwt expired
      const err = error || '';
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    }  else{

        
      errMsg = error.message ? error.message : error.toString();
    }

    console.error(errMsg);
    return throwError(errMsg);
  }
}
