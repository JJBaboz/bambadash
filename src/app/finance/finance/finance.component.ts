import { Component, OnInit ,ChangeDetectionStrategy, ChangeDetectorRef,} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GetRequestsService } from '../../get-requests.service';
import { Socket } from 'ngx-socket-io';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import {FormGroup, FormControl} from '@angular/forms';
import { HttpHeaders } from '@angular/common/http';
import { AlertController } from '@ionic/angular';
import { timeStamp } from 'console';

@Component({
  selector: 'app-finance',

  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './finance.component.html',
  styleUrls: ['./finance.component.scss'],
})
export class FinanceComponent implements OnInit {
  minDate: Date;
  maxDate: Date;

  range = new FormGroup({
    start: new FormControl(),
    end: new FormControl()
  });

  searchKey: string = "";

  mpesaBalance2:String = ""//working

  mpesaBalance3:String = ""//general
  devices = []
  devices3 = []
  myDevice=""

  clientPhone:String=""
  Amount:Number=null
  requests3 = []
  requests = []

  user = ""
  dataReq = []
  airtimeReq = []
  conversionReq = []

  smsBalance = 0;


  airtimeCredit= 0
  dataCredit = 0
airtimeCash = 0 
dataCash = 0

dataProfit =0
airtimeProfit= 0
totalProfit = 0

conversionAirtime = 0
conversionCash = 0
conversionCost = 0

conversionCost1 = 0

totalAirtime= 0

dispersable = 0



  constructor(private storage:Storage  ,public router: Router,private activatedRoute: ActivatedRoute, 
    
    private requestUssd:GetRequestsService,private socket:Socket, private ref: ChangeDetectorRef,
    public alertController: AlertController) {

      this.minDate = new Date(21, 9, 10);

     }

  ngOnInit() {

       


  
this.getMpesabalance()

this.getCreditBalance({name:null})
this.africabalance()



 

    this.storage.get('profile').then(data=>{

      if(!data){
      
     
      
      }else{


          this.user = data.profile.email
          console.log(data.profile.email);
          


      }
      
          })

       
          this.storage.get('authenticated').then(data=>{

            if(!data){
            
              this.router.navigate(['/login'])
            
            }
            
                })


//received mpesa balance
this.socket.on("b2cBalance",data=>{

console.log("balance from mpesa balance api...", data);




//{"Key":"AccountBalance","Value":"Working Account|KES|0.00|0.00|0.00|0.00&Utility Account|KES|30544.18|30544.18|0.00|0.00&Charges Paid Account|KES|0.00|0.00|0.00|0.00"}
 

  if(data.Result.ResultCode === 0){

   /// this.mpesaBalance = 
  
   let bal  = data.Result.ResultParameters.ResultParameter[1].Value
  
   

    console.log( data.Result.ResultCode);

//"Working Account|KES|0.00|0.00|0.00|0.00&Utility Account|KES|5447.91|5447.91|0.00|0.00&Charges Paid Account|KES|0.00|0.00|0.00|0.00"
  
    this.mpesaBalance2 =  bal.split(" ")[1].split("|")[2]  //working 
    this.mpesaBalance3 =  bal.split(" ")[2].split("|")[2] //utillity
   

    console.log(this.mpesaBalance3);

    this.ref.markForCheck();
    

    }else{


    }

})





//USSD BALANCE 

this.socket.on("ussdBalance",data=>{
  
  this.smsBalance = data
  this.ref.markForCheck();
  console.log("sms balance....", data);
  console.log("balance from mpesa balance api...", data);
  
  
  

  
  })


/*
this.socket.on("dashBoardCredit",data=>{

  console.log("balance from mpesa balance api...", data);
  
  
  
  
  //{"Key":"AccountBalance","Value":"Working Account|KES|0.00|0.00|0.00|0.00&Utility Account|KES|30544.18|30544.18|0.00|0.00&Charges Paid Account|KES|0.00|0.00|0.00|0.00"}
   
  this.devices = data
  this.ref.markForCheck();
  console.log("credit balance....", data);
  
  })

*/
  }

  

sendAirtime(){



  //this.socket.emit("sendtoDist",{phone:this.clientPhone, amount:this.Amount, device:this.myDevice})

 /* 
  {sessionId:"Manual Dash",  text:[""],
    phone:this.clientPhone,  amount:this.Amount,  otherTransNum:null,  scratchCard:null, 
    mpesaAmount:null, deviceId:devices[0]  }
  */

    let req = {phone:this.clientPhone, amount:this.Amount, device:this.myDevice,initializer:this.user}

    //&& user == 'babzgeo27@gmail.com'? false:true
    this.requestUssd.sendDist(req).subscribe(data=>{

  
      if (data.msg === "invalid") {
        
        console.log("token expired");
  
        this.router.navigate(['/login'])
  
      this.storage.clear()
  
  
        
      } else {

        this.presentAlert("Sent to server, check in Sms menu if the client received credit.")

        console.log("credit results....", data);

    
        
      }
      
    })

}



async presentAlert(message) {
  const alert = await this.alertController.create({
    cssClass: 'my-custom-class',
    header: 'Responce',
    subHeader: '',
    message: message,
    buttons: ['OK']
  });
  await alert.present();

  const { role } = await alert.onDidDismiss();
  console.log('onDidDismiss resolved with role', role);

}



getMpesabalance(){

  this.requestUssd.mpesaBalance3("A2M").subscribe(data=>{



   console.log("mpesa balance....", data);

  //  console.log("mpesa balance 2....", data.data.MpesaDetails[3].Value); //workin accc
  
   // console.log("mpesa balance....", data.data.MpesaDetails[2].Value); //utility acc
  


    if(data.result === "success"){
     //this.mpesaBalance = data.data.MpesaDetails[2].Value
  
   //  this.mpesaBalance2 = data.data.MpesaDetails[3].Value
  
    //  this.ref.markForCheck();

    }else{


    }
    
  })



  this.requestUssd.mpesaBalance3("A2M").subscribe(data=>{



    console.log("mpesa balance btob balance api....",data);



    
  })





}


getCreditBalance(device){


  this.requestUssd.findDevices(device).subscribe(data=>{

    this.totalAirtime = 0
    this.dispersable = 0

    this.devices = data.data

    this.devices3 = this.devices

  /*  this.devices3 = this.devices.filter((value: any, index: number, array: any[]) =>{

      console.log(value.state);
      

     return value.state === "online"

    })*/

    console.log("devvices online ....",this.devices3);
    

    this.devices.forEach(element => {

    // this.totalAirtime = this.totalAirtime + element.balance

     // this.dispersable = this.dispersable + element.usage


      
    });

    let usageDevice = this.devices.filter(v =>{

    return v.state === "online"

    })

    usageDevice.forEach(element => {

      this.totalAirtime = this.totalAirtime + element.balance

     this.dispersable = this.dispersable + element.usage


      
    });



    this.ref.markForCheck();
    console.log("credit balance....", this.totalAirtime , this.dispersable );


    if (data.msg === "invalid") {
      
      console.log("token expired");

      this.router.navigate(['/login'])

    this.storage.clear()


      
    } else {
      
      
    }
    
  })

}



africabalance(){

  this.requestUssd.getUssdBal().subscribe(data=>{
    this.smsBalance = data
    this.ref.markForCheck();
    console.log("sms balance....", data);

  })



}



  



findAll(){

  this.requestUssd.findAll().subscribe((data)=>{
    this.requests3 = data.data
    this.requests = data.data
    this.ref.markForCheck();
    console.log(data);
    
  })


}




calcRequests(){

console.log(this.range.value);

this.requests = []
this.requests3 = []

this.airtimeCredit= 0
this.dataCredit = 0
this.airtimeCash = 0 
this.dataCash = 0

this.dataProfit =0
this.airtimeProfit= 0
this.totalProfit = 0

this.conversionAirtime = 0
this.conversionCash = 0
this.conversionCost = 0


let date = this.range.value

if (date.start === null) {

  console.log("please set date");
  
  
} else if (date.end === null) {



  

  var startDate = new Date(date.start);
        var endDate = new Date();  //date.end

        console.log("start",startDate ,"End",endDate);

      /*  var resultProductData = this.requests.filter(function (a) {
            var hitDates = a.ProductHits || {};
            // extract all date strings
            hitDates = Object.keys(hitDates);
            // improvement: use some. this is an improment because .map()
            // and .filter() are walking through all elements.
            // .some() stops this process if one item is found that returns true in the callback function and returns true for the whole expression
           let hitDateMatchExists = hitDates.some(function(dateStr) {
                var date = new Date(dateStr);
                return date >= startDate && date <= endDate
            });


            return hitDateMatchExists;
        });

        console.log(resultProductData); */
  


        let query = {
          start:startDate,
          end:endDate,
        
}


this.requestUssd.getByDate(query).subscribe((data)=>{
 
//  this.ref.markForCheck();
  console.log("REQUESTED DATA WHEN END IS NULL IS...",data);




  
  this.requests3 = data.data
  this.requests = data.data


  //  state:"complete"

this.dataReq =  this.requests3.filter((data)=>{

return (data.transType === "Data" && data.state === "complete") 

})

this.airtimeReq =  this.requests3.filter((data)=>{

  return data.transType === "M2A" && data.state === "complete"
  
  })

  this.conversionReq =  this.requests3.filter((data)=>{

    return data.transType === "A2M" && data.state === "complete"
    
    })


    console.log("DATA.....", this.dataReq);
    
    console.log("AIRTIME....", this.airtimeReq);
    
    console.log("CONVERSION....", this.conversionReq);


    this.dataReq.forEach(data=>{

this.dataCredit = this.dataCredit + data.amount

this.dataCash = this.dataCash + data.mpesaAmount

this.dataProfit = Math.floor(this.dataCredit *0.1)

    })


    this.airtimeReq.forEach(data=>{

      this.airtimeCredit = this.airtimeCredit + data.airtimeAmount
      
      this.airtimeCash = this.airtimeCash + data.mpesaAmount


      this.airtimeProfit = Math.floor(this.airtimeCredit *0.1)
  
      this.totalProfit = this.airtimeProfit + this.dataProfit
      
      
          })


    
          this.conversionReq.forEach(data=>{

            this.conversionAirtime = this.conversionAirtime + data.amount
            
            this.conversionCash = this.conversionCash + data.mpesaAmount

            if (data.mpesaAmount > 1000) {

              this.conversionCost = this.conversionCost + 8
              
            } else if (data.mpesaAmount < 1000 && data.mpesaAmount > 100) {

              this.conversionCost = this.conversionCost + 4
              
            } else {
              
             // this.conversionCost = this.conversionCost  
            }

            
            
                })



               let conv1=  this.requests3.filter((data)=>{

                  return data.transType === "A2M"  
                  
                  })

                  
              /*    conv1.forEach(data=>{

            this.conversionCost = this.conversionCost + 1.5 
            
                })*/



    console.log("data transaction..", this.dataCash, this.dataCredit, "profit...", this.dataProfit);
    console.log("airtime transaction..", this.airtimeCash, this.airtimeCredit, "profit...", this.airtimeProfit);
    console.log("PROFIT....", this.totalProfit);


    console.log("CONVERSION MPESA TOTAL", this.conversionCash);
    console.log("CONVERSION AIRTIME TOTAL", this.conversionAirtime);
    
    
    console.log("CONVERSION COST..",this.conversionCost);
    
    
  
    

    


  this.ref.markForCheck();
  
})
  
}else{
/*
 let startDate = new Date(date.start).valueOf();
        let endDate = new Date(date.end).valueOf();
*/

        let startDate = new Date(date.start)
        let endDate = new Date(date.end)

        console.log("start",startDate ,"End",endDate);

        let query = {
                  start:startDate,
                  end:endDate,      
        }
        

        this.requestUssd.getByDate(query).subscribe((data)=>{
 
          //  this.ref.markForCheck();
            console.log("REQUESTED DATA WHEN END IS NULL IS...",data);
          
          
          
          
            
            this.requests3 = data.data
            this.requests = data.data
          
          
          this.dataReq =  this.requests3.filter((data)=>{
          
          return data.transType === "Data" && data.state === "complete" 
          
          })
          
          this.airtimeReq =  this.requests3.filter((data)=>{
          
            return data.transType === "M2A" && data.state === "complete"
            
            })
          
            this.conversionReq =  this.requests3.filter((data)=>{
          
              return data.transType === "A2M" && data.state === "complete"
              
              })
          
          
              console.log("DATA.....", this.dataReq);
              
              console.log("AIRTIME....", this.airtimeReq);
              
              console.log("CONVERSION....", this.conversionReq);
          
          
              this.dataReq.forEach(data=>{
          
          this.dataCredit = this.dataCredit + data.amount
          
          this.dataCash = this.dataCash + data.mpesaAmount
          
          
          
              })
          
          
              this.airtimeReq.forEach(data=>{
          
                this.airtimeCredit = this.airtimeCredit + data.airtimeAmount
                
                this.airtimeCash = this.airtimeCash + data.mpesaAmount
          
          
                this.airtimeProfit = Math.floor(this.airtimeCredit *0.1)
                this.dataProfit = Math.floor(this.dataCredit *0.1)
                this.totalProfit = this.airtimeProfit + this.dataProfit
                
                
                    })
          
          
              
                    this.conversionReq.forEach(data=>{
          
                    


                      this.conversionAirtime = this.conversionAirtime + data.amount
            
                      this.conversionCash = this.conversionCash + data.mpesaAmount
          
                      if (data.mpesaAmount > 1000) {
          
                        this.conversionCost = this.conversionCost + 8
                        
                      } else if (data.mpesaAmount < 1000 && data.mpesaAmount > 100) {
          
                        this.conversionCost = this.conversionCost + 4
                        
                      } else {
                        
                       // this.conversionCost = this.conversionCost  
                      }
                      
                      
                      
                          })


                          let conv1 =  this.requests3.filter((data)=>{
          
                            return data.transType === "A2M" && data.sessionId !== "Website Request" && data.sessionId !== "App"
                            
                            })

                            let air1 =  this.requests3.filter((data)=>{
          
                              return data.transType === "M2A" && data.sessionId !== "Website Request" && data.sessionId !== "App"
                              
                              })

                              let data1 =  this.requests3.filter((data)=>{
          
                                return data.transType === "Data" && data.sessionId !== "Website Request" && data.sessionId !== "App"
                                
                                })


                  this.conversionCost1 = (conv1.length + air1.length + data1. length ) * 1.5

                          /*  conv1.forEach(data=>{
          
                             this.conversionCost1 = this.conversionCost1 + 1.5 
                              
                                  })*/
          
          
              console.log("data transaction..", this.dataCash, this.dataCredit, "profit...", this.dataProfit);
              console.log("airtime transaction..", this.airtimeCash, this.airtimeCredit, "profit...", this.airtimeProfit);
              console.log("PROFIT....", this.totalProfit);
          
          
              console.log("CONVERSION MPESA TOTAL", this.conversionCash);
              console.log("CONVERSION AIRTIME TOTAL", this.conversionAirtime);
              
              
              console.log("CONVERSION COST..",this.conversionCost);
              
              console.log("CONVERSION COST UNFULL + FULLFILLED..",this.conversionCost1, conv1.length , air1.length , data1. length);
            
              
          
              
          
          
            this.ref.markForCheck();
            
          })
            

}


}


onInput(event) {

  console.log(event);
  const val =  this.searchKey = event.target.value;

  console.log(event.target.value);
  console.log("search value is...",event.target.value);
  
  
 if(event.target.value === ""){


  console.log("nothing to search");


   this.devices = this.devices3

   this.ref.markForCheck();


   
 }else if(val && val.trim() != ''){



    this.requestUssd.findByNameDevices(this.devices3 ,val)
    .then(data => {
      this.devices = data

      this.ref.markForCheck();
        
        
    })
    .catch(error => alert(JSON.stringify(error)));






}



}



onCancel(event) {
  this.devices = this.devices3

  this.ref.markForCheck();
}



}


/*

<ion-row>
  <ion-col>
    {{conversionAirtime}}
  </ion-col>

  <ion-col>
    {{conversionCash}}
  </ion-col>


  <ion-col>
    {{conversionCost}}
  </ion-col>

</ion-row>





<ion-row>
  <ion-col>
    {{conversionAirtime}}
  </ion-col>

  <ion-col>
    {{conversionCash}}
  </ion-col>


  <ion-col>
    {{conversionCost}}
  </ion-col>

</ion-row>

*/