import { Injectable } from '@angular/core';

import { JwtHelperService } from "@auth0/angular-jwt";
 
const helper = new JwtHelperService();

import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor( private storage:Storage ) { }

 async getToken() {

  return await this.storage.get('token')

}

  public isAuthenticated(token) {
    // get the token

  
    return helper.isTokenExpired( token );
  
  
  
    
    // return a boolean reflecting 
    // whether or not the token is expired
   
   

}


}