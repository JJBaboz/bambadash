import { Component, OnInit ,ChangeDetectionStrategy, ChangeDetectorRef,} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GetRequestsService } from '../../get-requests.service';
import { Socket } from 'ngx-socket-io';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';


@Component({
  selector: 'app-sms',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './sms.component.html',
  styleUrls: ['./sms.component.scss'],
})
export class SmsComponent implements OnInit {

  searchKey=""

    device=""

    myDevices=[]

    sms=[]

    title = ""

  constructor(private activatedRoute: ActivatedRoute, 
    private storage:Storage  ,public router: Router,
    
    private requestUssd:GetRequestsService,private socket:Socket, private ref: ChangeDetectorRef) { 



  }

  ngOnInit() {

    this.storage.get('authenticated').then(data=>{

      if(!data){
      
        this.router.navigate(['/login'])
      
      }
      
          })

    this.socket.on('connect', () => {

      console.log('Successfully connected !');
  
   
  
          })


          this.socket.on('connect', () => {

            console.log('Successfully connected !');
        
            this.socket.emit('dashRegister') 
        
                })


                
          this.socket.on('dashSms', (data) => {

        if( data.client === this.searchKey){

          console.log('Received sms!', data);

          if( data.responce === "success"){



            this.sms = data.sms

            
            this.ref.markForCheck();
            
  
          }else{

            this.sms= data.sms
            this.ref.markForCheck();

            this.title = data.responce
          }
           

        }
         
        
                })


             


                this.requestUssd.findDevices({name:null}).subscribe((data) =>{
                        this.myDevices = data.data

                        console.log("myDevices are..",this.myDevices);


                        if (data.msg === "invalid") {
      
                          console.log("token expired");
                    
                          this.router.navigate(['/login'])
                    
                        this.storage.clear()
                    
                    
                          
                        } else {
                          
                          
                        }

                  
                })

                
  }



  sendReq(){

console.log("clicked retrive sms...");

    this.socket.emit("retriveSms",{phone:this.device,client:this.searchKey})
  }



  onCancel(){

    this.searchKey = ""
  }




  

}




