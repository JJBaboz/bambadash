import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { SwUpdate } from '@angular/service-worker';
//import { Router } from '@angular/compiler/src/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PwaupdateService {

  constructor(public alertController: AlertController,private readonly updates: SwUpdate,private _router: Router) { 

    this.updates.available.subscribe(event => {
      this.showAppUpdateAlert();

     this.doAppUpdate();


    });

  }
  async  showAppUpdateAlert() {
    const header = 'App Update available';
    const message = 'Choose Ok to update';
    const action = this.doAppUpdate;
    const caller = this;
    
    // Use MatDialog or ionicframework's AlertController or similar
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: header,
      
      message: message,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
        }, {
          text: 'Ok',
          handler: action,
          cssClass: 'secondary',
        }
      ]
    });

    await alert.present();
  }


  doAppUpdate() {
    this.updates.activateUpdate().then(() => {
    
    //window.location.reload()
    
    this._router.routeReuseStrategy.shouldReuseRoute = () => false;
    this._router.onSameUrlNavigation = 'reload';
    this._router.navigate(['/dash/requests']);
    }
    );
  }

}
