import { TestBed } from '@angular/core/testing';

import { PwaupdateService } from './pwaupdate.service';

describe('PwaupdateService', () => {
  let service: PwaupdateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PwaupdateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
