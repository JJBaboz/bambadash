import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';

import { CommonModule } from '@angular/common';
import { PwaupdateService } from './service/pwaupdate.service';
import { IonicStorageModule } from '@ionic/storage';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';

import { NgxUiLoaderModule, NgxUiLoaderHttpModule,NgxUiLoaderConfig,
  SPINNER,
  POSITION,
  PB_DIRECTION } from 'ngx-ui-loader';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'



  const ngxUiLoaderConfig: NgxUiLoaderConfig = {
    bgsColor: 'blue',
    bgsPosition: POSITION.centerCenter,
    fgsPosition: POSITION.centerCenter,
    bgsSize: 60,
    bgsType: SPINNER.cubeGrid, // background spinner type
    fgsType: SPINNER.cubeGrid, // foreground spinner type
    pbDirection: PB_DIRECTION.leftToRight, // progress bar direction
    pbThickness: 5, // progress bar thickn

  };

const config: SocketIoConfig = { url: environment.url2, options: {
   reconnect: true,
  reconnectionDelay: 1000,
  reconnectionDelayMax: 5000,
  reconnectionAttempts: Infinity,
  autoConnect: true,
  forceNew: true,
  reconnection: true,
  withCredentials:false
} };



import { JwtModule, JWT_OPTIONS, JwtHelperService, } from '@auth0/angular-jwt';

import { Storage } from '@ionic/storage';
/*
export function jwtOptionsFactory(storage) {
  return {
    tokenGetter: () => {

      return storage.get('token');
    },
    allowedDomains: ["https://smarthubgroceries.co.ke"],
    whitelistedDomains: ['localhost:3000'],
    disallowedRoutes: [""],
    throwNoTokenError: true,
    headerName: "amness",
    authScheme: "JWT "
   


  }
}
JwtModule.forRoot({
        jwtOptionsProvider: {
            provide: JWT_OPTIONS,
            useFactory: jwtOptionsFactory,
            deps: [Storage],
        }
    })
*/


/*
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZXN1bHRzIjp7ImRhdGUiOiIyMDIxLTA2LTA5VDExOjQ4OjIyLjQxOFoiLCJfaWQiOiI2MGMwYWE4NzUzOTBiNjJjYzhiODhjOTUiLCJlbWFpbCI6ImJhYnpnZW8yN0BnbWFpbC5jb20iLCJwaG9uZSI6IjI1NDcxMTc4MTUxMiIsInBhc3N3b3JkIjpudWxsLCJiYWxhbmNlIjowLCJQcm92aWRlciI6IlNhZmFyaWNvbSIsInN0YXRlIjoiIiwiX192IjowfSwiaWF0IjoxNjM5MjI0ODQyLCJleHAiOjE2MzkyMjY2NDJ9.fYAva-kkfhyDF1QBHhL6kVr4PB_KFd0VhctIgQZODf0
**/

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireDatabaseModule } from '@angular/fire/database';

export function tokenGetter() {

  return localStorage.getItem("token");

}

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: ['bambaswap.net','www.smarthubgroceries.co.ke','smarthubgroceries.co.ke','localhost:3000'],
        headerName: "amness",
        
      },
    }),
    
    MatFormFieldModule,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule, 
    BrowserModule, 
    IonicStorageModule.forRoot({
      name: '__mydb',
driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    HttpClientModule,
    
    CommonModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
     SocketIoModule.forRoot(config),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }), 
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
    NgxUiLoaderHttpModule.forRoot({ showForeground: false,exclude: [ ] }),
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireDatabaseModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    PwaupdateService,
    JwtHelperService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
