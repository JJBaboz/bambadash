import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { GetRequestsService } from '../get-requests.service';
import { EditAppDataComponent } from '../edit-app-data/edit-app-data.component';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection, DocumentChangeAction, DocumentData } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-app-adds',
  templateUrl: './app-adds.component.html',
  styleUrls: ['./app-adds.component.scss'],
})
export class AppAddsComponent implements OnInit {

  products:any =[]

  constructor(public modalController: ModalController,
    private afFirestore: AngularFirestore) { }

  ngOnInit() {



    this.fetchData()


  }



      
async presentNewModal(data) {

  console.log(data);
  
  const modal = await this.modalController.create({
    component: EditAppDataComponent,
    cssClass: 'my-custom-class',
    showBackdrop:true,
    swipeToClose: true,
    animated:true,
    componentProps: {req:data,mode:"new"}
    
  });

modal.onWillDismiss().then(data =>{
  
  console.log("about to dismiss modal");

 // this.getProducts()

});

  return await modal.present();

  
}


   
async presentEditModal(data) {

  console.log(data);
  
  const modal = await this.modalController.create({
    component: EditAppDataComponent,
    cssClass: 'my-custom-class',
    showBackdrop:true,
    swipeToClose: true,
    animated:true,
    componentProps: {req:data,mode:"edit"}
    
  });

modal.onWillDismiss().then(data =>{
  
  console.log("about to dismiss modal");

 // this.getProducts()

});

  return await modal.present();

  
}


dismiss() {
  // using the injected ModalController this page
  // can "dismiss" itself and optionally pass back data
  this.modalController.dismiss({
    'dismissed': true
  });
}



getDataFromFirestore(): Observable<any[]> {
  return this.afFirestore.collection('appImages').snapshotChanges().pipe(
    map(actions => {
      return actions.map(action => {

        const data:any = action.payload.doc.data();
        const id = action.payload.doc.id;
        return { id, ...data };
      });
    })
  );
}



fetchData(): void {
  this.getDataFromFirestore().subscribe(

    (data: DocumentData[]) => {

  
    this.products = data

        console.log(data);
   
    },
    (error: any) => {
      console.error('Error fetching data:', error);
    }
  );
}


}
