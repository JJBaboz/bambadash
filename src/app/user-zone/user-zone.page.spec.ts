import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UserZonePage } from './user-zone.page';

describe('UserZonePage', () => {
  let component: UserZonePage;
  let fixture: ComponentFixture<UserZonePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserZonePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UserZonePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
