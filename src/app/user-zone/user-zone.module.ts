import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UserZonePageRoutingModule } from './user-zone-routing.module';

import { UserZonePage } from './user-zone.page';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UserZonePageRoutingModule,
  ],
  declarations: [UserZonePage]
})
export class UserZonePageModule {}
