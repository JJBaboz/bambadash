import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserZonePage } from './user-zone.page';

const routes: Routes = [
  {
    path: '',
    component: UserZonePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserZonePageRoutingModule {}
