import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GetRequestsService } from '../get-requests.service';
import { Socket } from 'ngx-socket-io';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-user-zone',
  templateUrl: './user-zone.page.html',
  styleUrls: ['./user-zone.page.scss'],
})
export class UserZonePage implements OnInit {

  Sms:string=""
  clientPhone:any=""
recipient=''
myContacts=[]


  constructor(private activatedRoute: ActivatedRoute, 
    private storage:Storage  ,public router: Router,
    
    private requestUssd:GetRequestsService,private socket:Socket) { }

  ngOnInit() {
    this.storage.get('authenticated').then(data=>{

      if(!data){
      
        this.router.navigate(['/login'])
      
      }
      
          })

                
this.storage.get('token').then(data =>{

  console.log("token data is...", data);

  this.requestUssd.httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json', 'amness':data })
  };

/*  
  this.requestUssd.clientContacts().subscribe((data) =>{

    if (data.msg === "invalid") {

      console.log("token expired");
      this.storage.clear()
    this.router.navigate(['/login'])

   


      
    } else {
      
      this.myContacts = data.map((value,index,array)=>{
        return "+" + value

})
      
    }
    

  

   // this.myContacts = data

//      console.log("mymyContacts are..",this.myContacts);


})*/



}).catch(err=>{

  console.log("error occured ...",err);
  
  this.requestUssd.httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json', 'amness':err })
  };

  this.router.navigate(['/login'])

})






  }



  sendSms(){


if(this.recipient === "Send to All"){


  console.log("Send to All..");

  this.requestUssd.sendSms(this.myContacts,this.Sms,this.recipient).subscribe((data) =>{
   

    console.log("sms send responce..",data);


})

}else if(this.recipient === "Send to Specific"){

  console.log("Send to Specific...");

  this.requestUssd.sendSms(["+" + this.clientPhone],this.Sms,this.recipient).subscribe((data) =>{
  

    console.log("sms send responce..", data);


})
}



  }


  selectionChange(event){

console.log(event.detail.value);
this.recipient = event.detail.value



  }


  smsChange(event){

    console.log(event.detail.value);
    this.Sms = event.detail.value
    
    
    
      }



    phoneChange(event){

        console.log(event.detail.value);
        this.clientPhone = event.detail.value
        
        console.log(this.clientPhone);
        
          }

}
