import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public appPages = [
    {
      title: 'Requests',
      url: '/dash/requests',
      icon: 'paper-plane'
    },
    {
      title: 'Sms',
      url: '/dash/sms',
      icon: 'mail'
    },
    {
      title: 'Finance',
      url: '/dash/finance',
      icon: 'paper-plane'
    },
    
    {
      title: 'clientArea',
      url: '/dash/userzone',
      icon: 'heart'
    },
    {
      title: 'Charts',
      url: '/dash/charts',
      icon: 'heart'
    },
    {
      title: 'App',
      url: '/dash/addData',
      icon: 'heart'
    }
  ];

  /*
 
    {
      title: 'Favorites',
      url: '/folder/Favorites',
      icon: 'heart'
    },
    {
      title: 'Archived',
      url: '/folder/Archived',
      icon: 'archive'
    },
    {
      title: 'Trash',
      url: '/folder/Trash',
      icon: 'trash'
    },
    {
      title: 'Spam',
      url: '/folder/Spam',
      icon: 'warning'
    }

  */
  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];

  constructor(
    private storage:Storage,public router: Router,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    const path = window.location.pathname.split('dash/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
  }

  async logout() {

  

    this.storage.clear()

    

    this.router.navigate(['/login'])
    


  }




}
