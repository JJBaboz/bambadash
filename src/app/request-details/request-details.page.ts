import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { GetRequestsService } from '../get-requests.service';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-request-details',
  templateUrl: './request-details.page.html',
  styleUrls: ['./request-details.page.scss'],
})
export class RequestDetailsPage implements OnInit {

  @Input() req: any;

  profile:any;

  purpose=""

  user = ""
   

  constructor(public alertController: AlertController,private storage:Storage  ,public router: Router,private requestUssd:GetRequestsService,public modalController: ModalController) {

    
    
   }

  ngOnInit() {
  //  console.log(this.req);

  this.purpose = this.req.manual? this.req.manualDescr.reason: ""

  
  this.storage.get('profile').then(data=>{

    if(!data){
    
      this.router.navigate(['/login'])
    
    }else{

      console.log(data);
      
      this.profile = data.profile
      this.user = data.profile.email
     //this.user = "jdfnsif"
    }
    
    
  
    
        })


// && user != 'babzgeo27@gmail.com' || user != 'mutie.michael@gmail.com' || user != 'gmngari1@gmail.com'   


        


    
  }


  
completTrans(req){

  console.log(req._id);
  let id = req._id


  req = {}
  req.id = id
  req.phone = this.profile.phone
  req.email = this.profile.email
  req.purpose = this.purpose

  
  this.requestUssd.complete(req).subscribe((data)=>{
 // this.findAll()
  
    console.log(data);

    this.presentAlert(data.response) 
    
  })

}




sendMoney(Data){

  console.log(Data._id);
  



 let req:any = {}

  req.id = Data._id
  req.phone = this.profile.phone
  req.email = this.profile.email
  req.purpose = this.purpose

  if(req.manual){

    req.email1 = Data.manualDescr? Data.manualDescr.by:this.profile.email
    req.time1 = Data.manualDescr.time 

  }
 


        this.requestUssd.sendMoney(req).subscribe((data)=>{
          // this.findAll()
           
             console.log(data);

             this.presentAlert(data.response) 
             
           })
        


}

sendMoneyApproval(req){

  console.log(req._id);
  let id = req._id


  req = {}
  req.id = id
  req.phone = this.profile.phone
  req.email = this.profile.email
  req.purpose = this.purpose

        this.requestUssd.sendMoneyApproval(req).subscribe((data)=>{
          // this.findAll()
           
             console.log(data);

             this.presentAlert(data.response) 
             
           })


}


sendData(req){

  console.log(req._id);



  let data:any = {}

  data.id = req._id
  data.phone = this.profile.phone
  data.email = this.profile.email
  data.purpose = this.purpose
  data.Provider = req.Provider

  this.requestUssd.sendData(req).subscribe((data)=>{
 // this.findAll()
  
    console.log(data);

    this.presentAlert(data.response) 
    
  })

}



sendCredit(req){

  console.log(req._id);
  let id = req._id


  req = {}
  req.id = id
  req.phone = this.profile.phone
  req.email = this.profile.email
  req.purpose = this.purpose

  
  this.requestUssd.sendCredit(req).subscribe((data)=>{
  //this.findAll()
  
    console.log(data);
    this.presentAlert(data.response) 
    
  })

}


dismiss() {
  // using the injected ModalController this page
  // can "dismiss" itself and optionally pass back data
  this.modalController.dismiss({
    'dismissed': true
  });
}


async presentAlert(message) {
  const alert = await this.alertController.create({
    cssClass: 'my-custom-class',
    header: 'Responce',
    subHeader: '',
    message: message,
    buttons: ['OK']
  });
  await alert.present();

  const { role } = await alert.onDidDismiss();
  console.log('onDidDismiss resolved with role', role);

}

}
