import { Component, OnInit , Input } from '@angular/core';
import { Observable } from 'rxjs';
import { finalize, tap } from 'rxjs/operators';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { ModalController } from '@ionic/angular';


export interface imgFile {
  name: string;
  filepath: string;
  size: number;
}

@Component({
  selector: 'app-edit-app-data',
  templateUrl: './edit-app-data.component.html',
  styleUrls: ['./edit-app-data.component.scss'],
})




export class EditAppDataComponent implements OnInit {

  @Input() req: any;

@Input() mode: any;


  // File upload task 
  fileUploadTask: AngularFireUploadTask;

  // Upload progress
  percentageVal: Observable<number>;
  
  // Track file uploading with snapshot
  trackSnapshot: Observable<any>;
  
  // Uploaded File URL
  UploadedImageURL: Observable<string>;
  
  // Uploaded image collection
  files: Observable<imgFile[]>;
  
  // Image specifications
  imgName: string;
  imgSize: number;
  
  // File uploading status
  isFileUploading: boolean=false;
  isFileUploaded: boolean=false
  myUrl=""
  fileStoragePath = ""
  per= 0
  
  name=""
  price:number=0
  description=""


  private filesCollection: AngularFirestoreCollection<imgFile>;




  constructor(    
    
    public modalController: ModalController,
    private afs: AngularFirestore,
    private afStorage: AngularFireStorage) { }

  ngOnInit() {


    if (this.mode === "edit") {

      this.name = this.req.customName
      this.description = this.req.description
      this.myUrl = this.req.image
    

      this.isFileUploading = false
      
      this.isFileUploaded= true

      
    } else if(this.mode === "new") {

      this.isFileUploading = false
      
      this.isFileUploaded= false

      
    } else {

      console.log( "undefined action request");
      
      
    }

  }




  uploadImage(event: FileList) {
      
    const file = event.item(0)

    console.log(file.name)
    // Image validation
    if (file.type.split('/')[0] !== 'image') { 
      console.log('File type is not supported!')
      return;
    }

    this.isFileUploading = true;
    this.isFileUploaded = false;

    this.imgName = file.name;


    console.log(file.name);



    const storageRef = this.afStorage.ref(`images`);

    const fileExt = file.name.split('.').pop();

  const imageName =  `${file.name}.${fileExt}`; // Replace with your custom name or generate dynamically
 
  const imageRef = storageRef.child(imageName);

  // Upload the image file
  const uploadTask =  imageRef.put(file);


  this.percentageVal =  uploadTask.percentageChanges()


  this.percentageVal.subscribe(resp=>{
        
   

    this.per = Math.floor(resp) /100

    console.log( "PERCENTAGE..", this.per);

    if (resp === 100) {


    imageRef.getDownloadURL().subscribe(resp=>{
      
        console.log( "FILE URL IS...", resp);

        this.myUrl = resp

        this.isFileUploading = false
        this.isFileUploaded = true

    })
      
    } else {
      
    }

})


  // Get the download URL of the uploaded image

  

    // Storage path
   // this.fileStoragePath = `waterApp/${new Date().getTime()}_${file.name}`;

  
  
  /*
  
  // Image reference
    const imageRef = this.afStorage.ref(this.fileStoragePath);

    

    // File upload task
    this.fileUploadTask = this.afStorage.upload(this.fileStoragePath, file);


   // imageRef.put(file);


    // Show uploading progress
    this.percentageVal =  this.fileUploadTask.percentageChanges()

    
    this.UploadedImageURL = imageRef.getDownloadURL();

   
    
    this.percentageVal.subscribe(resp=>{
        
   

      this.per = Math.floor(resp) /100

      console.log( "PERCENTAGE..", this.per);

      if (resp === 100) {


        this.UploadedImageURL.subscribe(resp=>{
        
          console.log( "FILE URL IS...", resp);

          this.myUrl = resp

          this.isFileUploading = false
          this.isFileUploaded = true

      })
        
      } else {
        
      }

  })
*/






  
  
  
  }





  
  async uploadImageToStorage(imageFile: File): Promise<string> {
    const storageRef = this.afStorage.ref('new_app_adds');
    const fileExt = imageFile.name.split('.').pop(); // Get the original file extension
    const imageName = `${this.name}.${fileExt}`; // Generate a unique name with the original file extension
    const imageRef = storageRef.child(imageName);
  
    await imageRef.put(imageFile);
    const imageUrl = await imageRef.getDownloadURL().toPromise();
  
    return imageUrl;
  }


   storeImageData() {
    const collectionRef = this.afs.collection('appImages');
  
     collectionRef.add({
      image: this.myUrl,
      customName: this.name,
      description: this.description
    }).then(data =>{

console.log(data);

this.dismiss()


    }).catch(error=>{


console.log(error);

    })




  }
  




  async handleImageUpload(imageFile: File, customName: string): Promise<void> {
    try {
      const imageUrl = await this.uploadImageToStorage(imageFile);
  //    await this.storeImageData(imageUrl, customName);
      console.log('Image uploaded and data stored successfully.');
    } catch (error) {
      console.error('Error uploading image and storing data:', error);
    }
  }






  delete(){

  
    const originalImageRef = this.afStorage.refFromURL(this.myUrl);

    console.log(originalImageRef);
    


      

    originalImageRef.delete().subscribe(data =>{

        console.log(data);

        this.isFileUploading = this.isFileUploaded = false

        this.myUrl = ""


        
      }, error=>{

        console.log(error);
        
      })
      
    

  }


  deleteProduct(){

    const originalImageRef = this.afStorage.refFromURL(this.myUrl);

    const docRef: AngularFirestoreDocument = this.afs.collection('appImages').doc(this.req.id);


 

      
    docRef.delete().then(d =>{

      console.log("DELETED PRODUCT FROM DATABASE..",d);
    
      this.isFileUploading = this.isFileUploaded = false
    
      this.myUrl = ""
      this.name = ""
      this.description = ""
      this.mode = "new"
    
    
     }
      ).catch(e =>{
    
    
        console.log("failed to delete", e);
    
      })
    console.log(this.req.id);
    


    if(originalImageRef){

    originalImageRef.delete().subscribe(data =>{

      console.log(data);

      this.isFileUploading = this.isFileUploaded = false

      this.myUrl = ""

  
      
    }, error=>{

      console.log(error);



      
    })

  }


      
    

  }





  updateProduct(){


    const docRef: AngularFirestoreDocument = this.afs.collection('appImages').doc(this.req.id);


   let newDoc =  {

customName :this.name,
description: this.description,
image: this.myUrl
    
    }

docRef.update(newDoc).then(d =>{

  console.log("DELETED PRODUCT FROM DATABASE..",d);

  this.isFileUploading = false

  this.isFileUploaded = true

 }
  ).catch(e =>{


    console.log("failed to delete", e);

  })



  /*  this.requestUssd.updateProducts(
      {query:{_id:this.req._id},item:{name:this.name,description:this.description,price:this.price,
        image:this.myUrl,fileStoragePath:this.fileStoragePath}}).subscribe(data=>{

console.log(data);

if (data.state === 'ok') {
  
  this.dismiss()
} else {
  this.dismiss()
}

        }, error=>{

          console.log(error);
          this.dismiss()
          
        })
*/
  }



  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      'dismissed': true
    });
  }



}
